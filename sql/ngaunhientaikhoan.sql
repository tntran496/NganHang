USE [NGANHANG]
GO

/****** Object:  Table [dbo].[NGAUNHIENSTK]    Script Date: 12/16/2020 4:22:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NGAUNHIENSTK](
	[SOTK] [nchar](9) NULL,
	[TRANGTHAI] [bit] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[NGAUNHIENSTK] ADD  CONSTRAINT [DF_NGAUNHIENSTK_TRANGTHAI]  DEFAULT ((0)) FOR [TRANGTHAI]

go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SINH_SO_TK_NGAUNHIEN]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @soluong INT = 999999
	DECLARE @dem INT = 100000
	WHILE @dem <= @soluong
	BEGIN
		INSERT INTO NGANHANG.dbo.NGAUNHIENSTK(SOTK) VALUES ('131'+CONVERT(NCHAR(9),@dem))
		SET @dem = @dem + FLOOR(RAND()*(9-1+1)+1)
	END
	SELECT 'SUCCESS'
END
go
exec [dbo].[SINH_SO_TK_NGAUNHIEN]
GO
/****** Object:  StoredProcedure [dbo].[SP_LAY_SOTK_NGAUNHIEN]    Script Date: 12/17/2020 11:03:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_LAY_SOTK_NGAUNHIEN]
	-- Add the parameters for the stored procedure here
@SOTK NCHAR(9) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 @SOTK = N.SOTK FROM NGANHANG.dbo.NGAUNHIENSTK AS N WHERE N.TRANGTHAI = 0 ORDER BY NEWID() 
	UPDATE NGANHANG.dbo.NGAUNHIENSTK SET TRANGTHAI = 1 WHERE SOTK = @SOTK
	RETURN @SOTK
END
