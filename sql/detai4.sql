USE [NGANHANG]
GO
/****** Object:  User [HTKN]    Script Date: 12/16/2020 4:35:16 PM ******/
CREATE USER [HTKN] FOR LOGIN [HTKN] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [MSmerge_14C8A7329D324FA68FB5DD55766CB123]    Script Date: 12/16/2020 4:35:16 PM ******/
CREATE ROLE [MSmerge_14C8A7329D324FA68FB5DD55766CB123]
GO
/****** Object:  DatabaseRole [MSmerge_B62342CFE9B44B80B81E22EA8AF93023]    Script Date: 12/16/2020 4:35:16 PM ******/
CREATE ROLE [MSmerge_B62342CFE9B44B80B81E22EA8AF93023]
GO
/****** Object:  DatabaseRole [MSmerge_F3A629C49E2845F391AB3EA7797D7BAA]    Script Date: 12/16/2020 4:35:16 PM ******/
CREATE ROLE [MSmerge_F3A629C49E2845F391AB3EA7797D7BAA]
GO
/****** Object:  DatabaseRole [MSmerge_PAL_role]    Script Date: 12/16/2020 4:35:16 PM ******/
CREATE ROLE [MSmerge_PAL_role]
GO
ALTER ROLE [db_owner] ADD MEMBER [HTKN]
GO
ALTER ROLE [MSmerge_PAL_role] ADD MEMBER [MSmerge_14C8A7329D324FA68FB5DD55766CB123]
GO
ALTER ROLE [MSmerge_PAL_role] ADD MEMBER [MSmerge_B62342CFE9B44B80B81E22EA8AF93023]
GO
ALTER ROLE [MSmerge_PAL_role] ADD MEMBER [MSmerge_F3A629C49E2845F391AB3EA7797D7BAA]
GO
/****** Object:  Schema [MSmerge_PAL_role]    Script Date: 12/16/2020 4:35:16 PM ******/
CREATE SCHEMA [MSmerge_PAL_role]
GO
/****** Object:  Table [dbo].[ChiNhanh]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiNhanh](
	[MACN] [nchar](10) NOT NULL,
	[TENCN] [nvarchar](100) NOT NULL,
	[DIACHI] [nvarchar](100) NOT NULL,
	[SoDT] [nvarchar](15) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [MSmerge_df_rowguid_DA75E767A4F048078541D1FA9114B1A8]  DEFAULT (newsequentialid()),
 CONSTRAINT [PK_ChiNhanh] PRIMARY KEY CLUSTERED 
(
	[MACN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GD_CHUYENTIEN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GD_CHUYENTIEN](
	[MAGD] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SOTK_CHUYEN] [nchar](9) NOT NULL,
	[NGAYGD] [datetime] NOT NULL CONSTRAINT [DF_GD_CHUYENTIEN_NGAYGD]  DEFAULT (getdate()),
	[SOTIEN] [money] NOT NULL,
	[SOTK_NHAN] [nchar](9) NOT NULL,
	[MANV] [nchar](10) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [MSmerge_df_rowguid_6D08BFA957DC4106A6E09DCDA1DDB408]  DEFAULT (newsequentialid()),
 CONSTRAINT [PK_GD_CHUYENTIEN] PRIMARY KEY CLUSTERED 
(
	[MAGD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GD_GOIRUT]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GD_GOIRUT](
	[MAGD] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SOTK] [nchar](9) NOT NULL,
	[LOAIGD] [nchar](2) NOT NULL,
	[NGAYGD] [datetime] NOT NULL CONSTRAINT [DF_GD_GOIRUT_NGAYGD]  DEFAULT (getdate()),
	[SOTIEN] [money] NOT NULL CONSTRAINT [DF_GD_GOIRUT_SOTIEN]  DEFAULT ((100000)),
	[MANV] [nchar](10) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [MSmerge_df_rowguid_5191D001941C4E01AD9860D6CB850861]  DEFAULT (newsequentialid()),
 CONSTRAINT [PK_GD_GOIRUT] PRIMARY KEY CLUSTERED 
(
	[MAGD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[CMND] [nchar](10) NOT NULL,
	[HO] [nvarchar](50) NOT NULL,
	[TEN] [nvarchar](10) NOT NULL,
	[DIACHI] [nvarchar](100) NOT NULL,
	[PHAI] [nvarchar](3) NULL,
	[NGAYCAP] [date] NOT NULL CONSTRAINT [DF_KhachHang_NGAYCAP]  DEFAULT (getdate()),
	[SODT] [nvarchar](15) NULL,
	[MACN] [nchar](10) NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [MSmerge_df_rowguid_6C4CBD996AC548819A5EBA7AD478EF16]  DEFAULT (newsequentialid()),
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[CMND] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MANV] [nchar](10) NOT NULL,
	[HO] [nvarchar](40) NOT NULL,
	[TEN] [nvarchar](10) NOT NULL,
	[DIACHI] [nvarchar](100) NOT NULL,
	[PHAI] [nvarchar](3) NULL,
	[SODT] [nvarchar](15) NOT NULL,
	[MACN] [nchar](10) NULL,
	[TrangThaiXoa] [int] NULL CONSTRAINT [DF_NhanVien_TrangThaiXoa]  DEFAULT ((0)),
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [MSmerge_df_rowguid_976B2DE3B7694755A7B36C2C78240585]  DEFAULT (newsequentialid()),
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MANV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[SOTK] [nchar](9) NOT NULL,
	[CMND] [nchar](10) NOT NULL,
	[SODU] [money] NOT NULL CONSTRAINT [DF__TaiKhoan__SODU__1BFD2C07]  DEFAULT ((0)),
	[MACN] [nchar](10) NULL,
	[NGAYMOTK] [datetime] NULL CONSTRAINT [DF_TaiKhoan_NGAYMOTK]  DEFAULT (getdate()),
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [MSmerge_df_rowguid_4333646A803145CBAB105CF28484AB5B]  DEFAULT (newsequentialid()),
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[SOTK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[V_DS_PHANMANH]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_DS_PHANMANH]
AS
SELECT TENCN=PUBS.description, TENSERVER= subscriber_server
   FROM dbo.sysmergepublications PUBS,  dbo.sysmergesubscriptions SUBS
   WHERE PUBS.pubid= SUBS.PUBID  AND PUBS.publisher <> SUBS.subscriber_server

GO
/****** Object:  View [dbo].[V_Lay_DS_TaiKhoan]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[V_Lay_DS_TaiKhoan]
as
select TK.SOTK,TK.CMND,TK.NGAYMOTK from NGANHANG.dbo.TaiKhoan as TK
union
select TK.SOTK,TK.CMND,TK.NGAYMOTK from LINK.NGANHANG.dbo.TaiKhoan as TK
GO
/****** Object:  View [dbo].[V_LAYMANHANVIEN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_LAYMANHANVIEN]
AS
SELECT        NV.MANV
FROM            NhanVien NV WHERE NV.TrangThaiXoa = 0
EXCEPT
SELECT        US.name
FROM            dbo.sysusers US

GO
/****** Object:  View [dbo].[V_MACN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_MACN]
AS
SELECT MACN, TENCN
FROM   dbo.ChiNhanh

GO
INSERT [dbo].[ChiNhanh] ([MACN], [TENCN], [DIACHI], [SoDT], [rowguid]) VALUES (N'BENTHANH  ', N'Chi nhánh Bến Thành', N'211 Lê Lợi, Quận 1, TPHCM', N'..', N'8069b96d-5d2a-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[ChiNhanh] ([MACN], [TENCN], [DIACHI], [SoDT], [rowguid]) VALUES (N'TANDINH   ', N'Chi nhánh Tân Định', N'234 Hai Bà Trưng, phường Đakao, Quận 1, TPHCM', N'...', N'8169b96d-5d2a-eb11-92e4-14abc5d2bbb1')
SET IDENTITY_INSERT [dbo].[GD_CHUYENTIEN] ON 

INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (2002, N'98765    ', CAST(N'2020-11-19 12:40:46.590' AS DateTime), 555555.0000, N'123456   ', N'NV001     ', N'8d69b96d-5d2a-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (4012, N'9038294  ', CAST(N'2020-11-25 23:49:08.313' AS DateTime), 50000.0000, N'345345   ', N'NV001     ', N'fe693122-3e2f-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48011, N'1010101  ', CAST(N'2020-11-27 16:54:36.803' AS DateTime), 55555.0000, N'567890   ', N'NV002     ', N'a222738e-9630-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48012, N'1010101  ', CAST(N'2020-11-27 16:59:55.393' AS DateTime), 44445.0000, N'777777   ', N'NV002     ', N'3702584c-9730-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48016, N'123456   ', CAST(N'2020-11-28 14:24:39.753' AS DateTime), 100000.0000, N'987654   ', N'NV002     ', N'1db933c6-4a31-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48017, N'987654   ', CAST(N'2020-11-29 14:25:14.073' AS DateTime), 50000.0000, N'123456   ', N'NV002     ', N'af8ca8da-4a31-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48018, N'987654   ', CAST(N'2020-11-30 14:27:25.497' AS DateTime), 50000.0000, N'123456   ', N'NV002     ', N'a368fe28-4b31-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48019, N'987654   ', CAST(N'2020-12-01 14:27:53.730' AS DateTime), 50000.0000, N'123456   ', N'NV002     ', N'397ed239-4b31-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48020, N'987654   ', CAST(N'2020-12-01 00:37:30.137' AS DateTime), 200000.0000, N'123456   ', N'NV002     ', N'0451e2b7-3233-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48021, N'123456   ', CAST(N'2020-12-01 16:47:52.203' AS DateTime), 500000.0000, N'987654   ', N'NV002     ', N'1a76f246-ba33-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48022, N'987654   ', CAST(N'2020-12-02 17:45:52.037' AS DateTime), 500000.0000, N'0702332  ', N'NV002     ', N'30ac7e8b-8b34-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48023, N'123456   ', CAST(N'2020-12-03 22:45:42.637' AS DateTime), 55555.0000, N'999999   ', N'NV002     ', N'f8712499-7e35-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48024, N'0702332  ', CAST(N'2020-12-03 22:52:05.297' AS DateTime), 90000.0000, N'123456   ', N'NV002     ', N'4594397d-7f35-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48025, N'0702332  ', CAST(N'2020-12-13 13:58:38.163' AS DateTime), 60000.0000, N'2829389  ', N'NV002     ', N'7b769e9f-103d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48026, N'123456   ', CAST(N'2020-12-13 14:00:27.677' AS DateTime), 50000.0000, N'2829389  ', N'NV002     ', N'bddbe4e0-103d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (48027, N'06542    ', CAST(N'2020-12-14 23:12:42.260' AS DateTime), 100000.0000, N'0702332  ', N'NV002     ', N'b7e70d31-273e-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (70011, N'123456   ', CAST(N'2020-12-02 17:35:24.100' AS DateTime), 50000.0000, N'2829389  ', N'NV001     ', N'0da93715-8a34-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (70012, N'878454   ', CAST(N'2020-12-10 17:43:20.297' AS DateTime), 50000.0000, N'06542    ', N'NV001     ', N'0b6d5a84-d43a-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[GD_CHUYENTIEN] ([MAGD], [SOTK_CHUYEN], [NGAYGD], [SOTIEN], [SOTK_NHAN], [MANV], [rowguid]) VALUES (70013, N'878454   ', CAST(N'2020-12-10 17:53:17.030' AS DateTime), 60000.0000, N'0702332  ', N'NV001     ', N'a88a08e8-d53a-eb11-92e7-14abc5d2bbb1')
SET IDENTITY_INSERT [dbo].[GD_CHUYENTIEN] OFF
SET IDENTITY_INSERT [dbo].[GD_GOIRUT] ON 

INSERT [dbo].[GD_GOIRUT] ([MAGD], [SOTK], [LOAIGD], [NGAYGD], [SOTIEN], [MANV], [rowguid]) VALUES (46006, N'987654   ', N'RT', CAST(N'2020-11-29 14:21:41.313' AS DateTime), 100000.0000, N'NV002     ', N'2ffad75b-4a31-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_GOIRUT] ([MAGD], [SOTK], [LOAIGD], [NGAYGD], [SOTIEN], [MANV], [rowguid]) VALUES (46007, N'987654   ', N'GT', CAST(N'2020-11-30 14:22:06.230' AS DateTime), 300000.0000, N'NV002     ', N'f4b6b16a-4a31-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_GOIRUT] ([MAGD], [SOTK], [LOAIGD], [NGAYGD], [SOTIEN], [MANV], [rowguid]) VALUES (46008, N'987654   ', N'RT', CAST(N'2020-11-28 14:22:38.573' AS DateTime), 300000.0000, N'NV002     ', N'925ef97d-4a31-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[GD_GOIRUT] ([MAGD], [SOTK], [LOAIGD], [NGAYGD], [SOTIEN], [MANV], [rowguid]) VALUES (46009, N'987654   ', N'GT', CAST(N'2020-12-05 14:24:14.730' AS DateTime), 100000.0000, N'NV002     ', N'78ac49b7-4a31-eb11-92e5-14abc5d2bbb1')
SET IDENTITY_INSERT [dbo].[GD_GOIRUT] OFF
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'03748     ', N'nguyen', N'van', N'9678678', N'NỮ', CAST(N'2020-11-11' AS Date), N'07293232', N'BENTHANH  ', N'817f3f41-7d2d-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'045745    ', N'ytsn', N'ihtii', N'99035345', N'NAM', CAST(N'2020-11-03' AS Date), N'0743834', N'BENTHANH  ', N'6cf5c4ab-2f2e-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'057484    ', N'nguyen', N'van', N'87978', N'NAM', CAST(N'2020-11-13' AS Date), N'57657', N'BENTHANH  ', N'97ade8d7-5135-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'0738434   ', N'nguyn', N'uhuihu', N'99', N'NỮ', CAST(N'2020-12-08' AS Date), N'0989983', N'BENTHANH  ', N'e16e55e0-0d3d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'0932984   ', N'luuu', N'thanh', N'08273823', N'NAM', CAST(N'2020-12-31' AS Date), N'0347833', N'TANDINH   ', N'0f7404a5-3635-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'097638722 ', N'nGUYỄN', N'vĂN', N'FDAQS', N'NỮ', CAST(N'2020-12-26' AS Date), N'0987654', N'TANDINH   ', N'68c792dd-5135-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'09876543  ', N'ASDASD', N'ASDASD', N'JHTAS', N'NAM', CAST(N'2020-12-29' AS Date), N'98765', N'TANDINH   ', N'93793b51-4c35-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'123456    ', N'NGuyen', N'Van A', N'adsioh', N'NAM', CAST(N'2020-12-04' AS Date), N'07382732', N'BENTHANH  ', N'8369b96d-5d2a-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'44444444  ', N'Nguyen Van', N'Sửa', N'HEM 3O0', N'NAM', CAST(N'2020-11-19' AS Date), N'0Y5736743', N'TANDINH   ', N'42e7322c-162e-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'908934    ', N'asdasd', N'asdasd', N'asdasd', N'NỮ', CAST(N'2020-12-21' AS Date), N'32434', N'TANDINH   ', N'bc17984e-343d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'95849385  ', N'TRan', N'Thien', N'07657', N'NAM', CAST(N'2020-11-12' AS Date), N'90823984', N'TANDINH   ', N'b0c7c4bf-7c35-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[KhachHang] ([CMND], [HO], [TEN], [DIACHI], [PHAI], [NGAYCAP], [SODT], [MACN], [rowguid]) VALUES (N'988343    ', N'Nguyen', N'Van Moi', N'asdasd', N'NỮ', CAST(N'2020-12-18' AS Date), N'083483787', N'BENTHANH  ', N'4eda221a-053d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'122       ', N'test', N'fdf', N'gdgf', N'NỮ', N'2345', N'TANDINH   ', 1, N'8bea09de-4435-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'fasfasf   ', N'ihsaoihoiasd', N'asdasd', N'fafafs', N'NỮ', N'4354', N'TANDINH   ', 0, N'6e416c22-343d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'NV001     ', N'Tran Duc', N'Thien', N'asd', N'NAM', N'092323', N'BENTHANH  ', 1, N'8669b96d-5d2a-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'NV002     ', N'Tran', N'Van A', N'asdasds', N'NAM', N'0834023', N'TANDINH   ', 0, N'8769b96d-5d2a-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'NV003     ', N'Tran Van ', N'B', N'0706950221', N'NỮ', N'07548754854', N'BENTHANH  ', 1, N'7c5b6db7-8334-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'NV004     ', N'Nguyễn', N'van a', N'asdasd', N'NAM', N'07069402', N'TANDINH   ', 1, N'152c8dd5-023d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'NV006     ', N'Nguyen moi sua', N'Van', N'wehiehfo', N'NỮ', N'wrgwegweg', N'BENTHANH  ', 1, N'c8d38aee-832a-eb11-92e4-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'nv076543  ', N'nguyen van a', N'asd', N'hasidhoiasd', N'NỮ', N'047382', N'TANDINH   ', 0, N'caf66625-323d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'NV091     ', N'nguyen van b', N'a', N'sdfjklsjkld', N'NỮ', N'09374387', N'BENTHANH  ', 1, N'1acec400-043d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[NhanVien] ([MANV], [HO], [TEN], [DIACHI], [PHAI], [SODT], [MACN], [TrangThaiXoa], [rowguid]) VALUES (N'NV702     ', N'nguyen', N'asdas', N'hdiqhwiodq', N'NAM', N'08764', N'TANDINH   ', 0, N'821a5986-2d3d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'0122323  ', N'88888888  ', 444444.0000, N'BENTHANH  ', CAST(N'2020-12-16 00:00:00.000' AS DateTime), N'00386f37-b93e-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'049893   ', N'0976387   ', 9876543.0000, N'BENTHANH  ', CAST(N'2020-12-23 00:00:00.000' AS DateTime), N'c734dcf6-7c35-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'06542    ', N'44444444  ', 555555.0000, N'TANDINH   ', CAST(N'2020-11-20 00:00:00.000' AS DateTime), N'fd85b649-ff32-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'0702332  ', N'09388923  ', 610000.0000, N'TANDINH   ', CAST(N'2020-12-24 00:00:00.000' AS DateTime), N'd97cb473-8b34-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'123456   ', N'44444444  ', 134445.0000, N'TANDINH   ', CAST(N'2020-11-12 00:00:00.000' AS DateTime), N'c4c07620-4931-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'131153508', N'0932984   ', 50000.0000, N'TANDINH   ', CAST(N'2020-12-17 00:00:00.000' AS DateTime), N'7915f843-7b3f-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'131214542', N'045745    ', 100000.0000, N'TANDINH   ', CAST(N'2020-12-18 00:00:00.000' AS DateTime), N'7884c661-7b3f-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'131692248', N'045745    ', 100000.0000, N'TANDINH   ', CAST(N'2020-01-15 00:00:00.000' AS DateTime), N'4502a5a8-7b3f-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'24324    ', N'44444444  ', 400000.0000, N'BENTHANH  ', CAST(N'2020-12-30 00:00:00.000' AS DateTime), N'f36f52ea-c63e-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'2829389  ', N'057484    ', 225432.0000, N'TANDINH   ', CAST(N'2020-11-19 00:00:00.000' AS DateTime), N'193b93c7-0033-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'878454   ', N'0976387   ', 390000.0000, N'TANDINH   ', CAST(N'2020-12-25 00:00:00.000' AS DateTime), N'15384f81-7e35-eb11-92e6-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'9389493  ', N'057484    ', 500000.0000, N'BENTHANH  ', CAST(N'2020-12-03 00:00:00.000' AS DateTime), N'830945f5-0b3d-eb11-92e7-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'987654   ', N'123456    ', 200000.0000, N'BENTHANH  ', CAST(N'2020-12-01 00:00:00.000' AS DateTime), N'd7e77436-4931-eb11-92e5-14abc5d2bbb1')
INSERT [dbo].[TaiKhoan] ([SOTK], [CMND], [SODU], [MACN], [NGAYMOTK], [rowguid]) VALUES (N'999999   ', N'03748     ', 5555555.0000, N'BENTHANH  ', CAST(N'2020-12-25 00:00:00.000' AS DateTime), N'782c77d9-8a34-eb11-92e6-14abc5d2bbb1')
SET ANSI_PADDING ON

GO
/****** Object:  Index [UK_ChiNhanh]    Script Date: 12/16/2020 4:35:16 PM ******/
ALTER TABLE [dbo].[ChiNhanh] ADD  CONSTRAINT [UK_ChiNhanh] UNIQUE NONCLUSTERED 
(
	[TENCN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GD_CHUYENTIEN]  WITH CHECK ADD  CONSTRAINT [FK_GD_CHUYENTIEN_NhanVien] FOREIGN KEY([MANV])
REFERENCES [dbo].[NhanVien] ([MANV])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[GD_CHUYENTIEN] CHECK CONSTRAINT [FK_GD_CHUYENTIEN_NhanVien]
GO
ALTER TABLE [dbo].[GD_GOIRUT]  WITH CHECK ADD  CONSTRAINT [FK_GD_GOIRUT_NhanVien] FOREIGN KEY([MANV])
REFERENCES [dbo].[NhanVien] ([MANV])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[GD_GOIRUT] CHECK CONSTRAINT [FK_GD_GOIRUT_NhanVien]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_ChiNhanh] FOREIGN KEY([MACN])
REFERENCES [dbo].[ChiNhanh] ([MACN])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_ChiNhanh]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_ChiNhanh] FOREIGN KEY([MACN])
REFERENCES [dbo].[ChiNhanh] ([MACN])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_ChiNhanh]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_ChiNhanh] FOREIGN KEY([MACN])
REFERENCES [dbo].[ChiNhanh] ([MACN])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_ChiNhanh]
GO
ALTER TABLE [dbo].[GD_CHUYENTIEN]  WITH CHECK ADD  CONSTRAINT [CK_GD_CHUYENTIEN] CHECK  (([SOTIEN]>(0)))
GO
ALTER TABLE [dbo].[GD_CHUYENTIEN] CHECK CONSTRAINT [CK_GD_CHUYENTIEN]
GO
ALTER TABLE [dbo].[GD_CHUYENTIEN]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_98C9B434_B370_42D0_8EED_5F57B85AF9F5] CHECK NOT FOR REPLICATION (([MAGD]>(2008) AND [MAGD]<=(3008) OR [MAGD]>(3008) AND [MAGD]<=(4008)))
GO
ALTER TABLE [dbo].[GD_CHUYENTIEN] CHECK CONSTRAINT [repl_identity_range_98C9B434_B370_42D0_8EED_5F57B85AF9F5]
GO
ALTER TABLE [dbo].[GD_GOIRUT]  WITH CHECK ADD  CONSTRAINT [CK_LOAIGD] CHECK  (([LOAIGD]='RT' OR [LOAIGD]='GT'))
GO
ALTER TABLE [dbo].[GD_GOIRUT] CHECK CONSTRAINT [CK_LOAIGD]
GO
ALTER TABLE [dbo].[GD_GOIRUT]  WITH CHECK ADD  CONSTRAINT [CK_SOTIEN] CHECK  (([SOTIEN]>=(100000)))
GO
ALTER TABLE [dbo].[GD_GOIRUT] CHECK CONSTRAINT [CK_SOTIEN]
GO
ALTER TABLE [dbo].[GD_GOIRUT]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_9448A1D1_756B_4DE2_9B06_900A7EDEDBDD] CHECK NOT FOR REPLICATION (([MAGD]>=(1) AND [MAGD]<=(1001) OR [MAGD]>(1001) AND [MAGD]<=(2001)))
GO
ALTER TABLE [dbo].[GD_GOIRUT] CHECK CONSTRAINT [repl_identity_range_9448A1D1_756B_4DE2_9B06_900A7EDEDBDD]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [CK_PHAI_KhachHang] CHECK  (([PHAI]='NAM' OR [PHAI]=N'NỮ'))
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [CK_PHAI_KhachHang]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [CK_PHAI_NhanVien] CHECK  (([PHAI]='NAM' OR [PHAI]=N'NỮ'))
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [CK_PHAI_NhanVien]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [CK_SODU_TaiKhoan] CHECK  (([SODU]>=(0)))
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [CK_SODU_TaiKhoan]
GO
/****** Object:  StoredProcedure [dbo].[CHUYEN_CN_NHANVIEN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CHUYEN_CN_NHANVIEN]
@MANV nchar(10), @HO nvarchar(40), @TEN nvarchar(10), @DIACHI nvarchar(100), @PHAI nvarchar(3), @SODT nvarchar(15), @MACN nchar(10) 
AS
BEGIN
UPDATE NGANHANG.dbo.NHANVIEN SET TrangThaiXoa = 1 WHERE MANV = @MANV
IF(NOT EXISTS (SELECT NV.MANV FROM LINK.NGANHANG.dbo.NHANVIEN AS NV WHERE NV.MANV =@MANV))
	BEGIN
		INSERT INTO LINK.NGANHANG.dbo.NHANVIEN (MANV, HO, TEN, DIACHI, PHAI, SODT, MACN, TrangThaiXoa) VALUES (@MANV, @HO, @TEN, @DIACHI, @PHAI, @SODT, @MACN, 0)
	END
ELSE
	BEGIN
		UPDATE LINK.NGANHANG.dbo.NHANVIEN SET TrangThaiXoa = 0 WHERE MANV = @MANV
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_BONHO]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_BONHO]
as
SELECT
    OBJECT_NAME(i.OBJECT_ID) AS TableName,
    i.name AS IndexName,
    i.index_id AS IndexID,
    8 * SUM(a.used_pages) AS 'Indexsize(KB)'
FROM
    sys.indexes AS i
    JOIN sys.partitions AS p ON p.OBJECT_ID = i.OBJECT_ID AND p.index_id = i.index_id
    JOIN sys.allocation_units AS a ON a.container_id = p.partition_id
WHERE
    i.is_primary_key = 0 -- fix for size discrepancy
GROUP BY
    i.OBJECT_ID,
    i.index_id,
    i.name
ORDER BY
    (8 * SUM(a.used_pages)) desc
GO
/****** Object:  StoredProcedure [dbo].[SP_CHECK_CMND_KH]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CHECK_CMND_KH] 
	-- Add the parameters for the stored procedure here
@CMND nchar(10)
AS
BEGIN
	IF(exists(SELECT KH.CMND FROM NGANHANG.dbo.KhachHang AS KH WHERE KH.CMND = @CMND))
		BEGIN
			SELECT 1;
			RETURN;
		END
		
	IF(exists(SELECT KH.CMND FROM LINK.NGANHANG.dbo.KhachHang AS KH WHERE KH.CMND = @CMND))
		BEGIN
			SELECT 1;
			RETURN;
		END
	SELECT 0;
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Check_SoTK_KhachHang]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Check_SoTK_KhachHang]
	-- Add the parameters for the stored procedure here
@SOTK nchar(9)
AS
BEGIN
	IF(exists(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT 1;
			RETURN;
		END
		
	IF(exists(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT 1;
			RETURN;
		END
	SELECT 0;
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Check_TonTai_ChuyenTien_TaiKhoan]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Check_TonTai_ChuyenTien_TaiKhoan]
	-- Add the parameters for the stored procedure here
@SOTK nchar(9)
AS
BEGIN
	IF(EXISTS(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT COUNT(*) FROM NGANHANG.dbo.GD_CHUYENTIEN AS GD WHERE GD.SOTK_CHUYEN = @SOTK
			return
		END
	IF(EXISTS(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT COUNT(*) FROM LINK.NGANHANG.dbo.GD_CHUYENTIEN AS GD WHERE GD.SOTK_CHUYEN = @SOTK
			return
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Check_TonTai_GoiRut_TaiKhoan]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Check_TonTai_GoiRut_TaiKhoan]
	-- Add the parameters for the stored procedure here
@SOTK nchar(9)
AS
BEGIN
	IF(EXISTS(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT COUNT(*) FROM NGANHANG.dbo.GD_GOIRUT AS GD WHERE GD.SOTK = @SOTK
			return
		END
	IF(EXISTS(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT COUNT(*) FROM LINK.NGANHANG.dbo.GD_GOIRUT AS GD WHERE GD.SOTK = @SOTK
			return
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Check_TonTai_NhanTien]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Check_TonTai_NhanTien]
	-- Add the parameters for the stored procedure here
@SOTK nchar(9)
AS
BEGIN
	IF(EXISTS(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT COUNT(*) FROM NGANHANG.dbo.GD_CHUYENTIEN AS GD WHERE GD.SOTK_NHAN = @SOTK
			return
		END
	IF(EXISTS(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT COUNT(*) FROM LINK.NGANHANG.dbo.GD_CHUYENTIEN AS GD WHERE GD.SOTK_NHAN = @SOTK
			return
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Check_TonTai_TaiKhoan_CMND]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Check_TonTai_TaiKhoan_CMND]
	-- Add the parameters for the stored procedure here
@CMND nchar(10)
AS
BEGIN
	IF(EXISTS(SELECT KH.CMND FROM NGANHANG.dbo.KhachHang AS KH WHERE KH.CMND = @CMND))
		BEGIN
			SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.CMND = @CMND
		END
	IF(EXISTS(SELECT KH.CMND FROM LINK.NGANHANG.dbo.KhachHang AS KH WHERE KH.CMND = @CMND))
		BEGIN
			SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.CMND = @CMND
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_CHECKMANV]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[SP_CHECKMANV] 
	-- Add the parameters for the stored procedure here
@MANV nvarchar(10)
AS
BEGIN
	IF(exists(SELECT NV.MANV FROM NGANHANG.dbo.NhanVien AS NV WHERE NV.MANV = @MANV))
		BEGIN
			SELECT 1;
			RETURN;
		END
		
	IF(exists(SELECT NV.MANV FROM LINK.NGANHANG.dbo.NhanVien AS NV WHERE NV.MANV = @MANV))
		BEGIN
			SELECT 1;
			RETURN;
		END
	SELECT 0;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_ChuyenTien_TaiKhoan]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ChuyenTien_TaiKhoan]
	-- Add the parameters for the stored procedure here
@SOTK_CHUYEN NCHAR(9),
@SOTIEN MONEY,
@SOTK_NHAN NCHAR(9),
@MANV NCHAR(10)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN DISTRIBUTED TRANSACTION;
		BEGIN TRY
			DECLARE @ErrorMessage varchar(2000)
			DECLARE @TienTkChuyen money

			IF(EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE NGANHANG.dbo.TaiKhoan  SET SODU = SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
			IF(EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE LINK.NGANHANG.dbo.TaiKhoan SET SODU =SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
			IF(EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE NGANHANG.dbo.TaiKhoan SET SODU =SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
			IF(EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE LINK.NGANHANG.dbo.TaiKhoan SET SODU =SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
		END TRY
		BEGIN CATCH
			SELECT @ErrorMessage = N'LỖI ' +ERROR_MESSAGE()
			RAISERROR(@ErrorMessage, 16,1)
			ROLLBACK
		END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[SP_ChuyenTien_TaiKhoan_VER2]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ChuyenTien_TaiKhoan_VER2]
	-- Add the parameters for the stored procedure here
@SOTK_CHUYEN nchar(9),
@SOTK_NHAN nchar(9),
@SOTIEN MONEY,
@MANV nchar(10)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN DISTRIBUTED TRANSACTION
	DECLARE @cnHienTai nchar(10)
	DECLARE @cnChuyen nchar(10)
	DECLARE @cnNhan nchar(10)
	DECLARE @ErrorMessage nvarchar(2000)
	DECLARE @TienTkChuyen money	
	EXEC NGANHANG.dbo.SP_DinhVi_ChiNhanh_TaiKhoan @SOTK_CHUYEN,@cnChuyen OUTPUT
	EXEC NGANHANG.dbo.SP_DinhVi_ChiNhanh_TaiKhoan @SOTK_NHAN, @cnNhan OUTPUT
	EXEC NGANHANG.dbo.SP_LAY_SODU_TAIKHOAN @SOTK_CHUYEN,@TienTkChuyen OUTPUT
	SELECT @cnHienTai = CN.MACN FROM ChiNhanh AS CN
		IF(@cnHienTai = @cnChuyen AND @cnHienTai = @cnNhan)
			BEGIN
					UPDATE NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
					UPDATE NGANHANG.dbo.TaiKhoan  SET SODU = SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
					INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
			END
		IF(@cnHienTai != @cnChuyen AND @cnHienTai = @cnNhan)
			BEGIN
					UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
					UPDATE NGANHANG.dbo.TaiKhoan  SET SODU = SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
					INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
			END
		IF(@cnHienTai = @cnChuyen AND @cnHienTai != @cnNhan)
			BEGIN
					UPDATE NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
					UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU = SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
					INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
			END
		IF(@cnHienTai != @cnChuyen AND @cnHienTai != @cnNhan)
			BEGIN
					UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
					UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU = SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
					INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
			END
	COMMIT TRANSACTION
END

GO
/****** Object:  StoredProcedure [dbo].[SP_DANGNHAP]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_DANGNHAP]
@TENLOGIN NVARCHAR (50),
@PASSWORD NVARCHAR (50)
AS
DECLARE @TENUSER NVARCHAR(50)
SELECT @TENLOGIN=loginname FROM sys.syslogins where loginname = @TENLOGIN and pwdcompare(@PASSWORD, password) = 1

SELECT @TENUSER=NAME FROM sys.sysusers WHERE sid = SUSER_SID(@TENLOGIN)
 --djkasbdjkbasjk
 SELECT USERNAME = @TENUSER, 
  HOTEN = (SELECT HO+ ' '+ TEN FROM NHANVIEN  WHERE MANV = @TENUSER ),
   TENNHOM= NAME
   FROM sys.sysusers 
   WHERE UID = (SELECT GROUPUID 
                 FROM SYS.SYSMEMBERS 
                   WHERE MEMBERUID= (SELECT UID FROM sys.sysusers 
                                      WHERE NAME=@TENUSER))
GO
/****** Object:  StoredProcedure [dbo].[SP_DinhVi_ChiNhanh_TaiKhoan]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_DinhVi_ChiNhanh_TaiKhoan]
	-- Add the parameters for the stored procedure here
@SOTK nchar(9),
@CN NCHAR(10) OUTPUT
AS
BEGIN
	IF(EXISTS(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT @CN = CN.MACN FROM NGANHANG.dbo.ChiNhanh AS CN 
		END
	IF(EXISTS(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT @CN = CN.MACN FROM LINK.NGANHANG.dbo.ChiNhanh AS CN
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GETLOGNME]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GETLOGNME] 
	-- Add the parameters for the stored procedure here
@MANV nvarchar(10)
AS
DECLARE @SID nvarchar(10)
DECLARE @loginname nvarchar(10)
BEGIN
	SELECT @SID= sid from sys.sysusers where name = @MANV
	SELECT name FROM sys.syslogins WHERE sid = @SID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GOIRUT]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GOIRUT]
@TAIKHOAN NCHAR(9), @SOTIEN money, @LOAIGD NCHAR(2), @MANV nchar(10)

AS
BEGIN

	DECLARE @sd money;
	SET @sd = ( SELECT top 1 SODU FROM TaiKhoan )
	IF(exists(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @TAIKHOAN))		
		BEGIN			 
				IF(@LOAIGD = 'GT')
				BEGIN
					UPDATE NGANHANG.dbo.TaiKhoan SET SODU = SODU + @SOTIEN WHERE SOTK= @TAIKHOAN		  
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV) VALUES (@TAIKHOAN, 'GT', @SOTIEN, @MANV )
					RETURN
				END
			IF(@LOAIGD = 'RT')
				BEGIN
					IF(@sd < @SOTIEN)
						BEGIN
							RAISERROR('So du khong du', 16, 1)
							RETURN
						END				
					UPDATE NGANHANG.dbo.TaiKhoan SET SODU = SODU - @SOTIEN WHERE SOTK= @TAIKHOAN
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV)VALUES (@TAIKHOAN, 'RT', @SOTIEN, @MANV )
					RETURN
				END
		END
	IF( exists(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @TAIKHOAN))
		BEGIN
			IF(@LOAIGD = 'GT')
				BEGIN
					UPDATE LINK.NGANHANG.dbo.TaiKhoan SET SODU = SODU + @SOTIEN WHERE SOTK= @TAIKHOAN		  
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV) VALUES (@TAIKHOAN, 'GT', @SOTIEN, @MANV )
					RETURN
				END
			IF(@LOAIGD = 'RT')
				BEGIN
					IF(@sd < @SOTIEN)
						BEGIN
							RAISERROR('So du khong du', 16, 1)
							RETURN
						END				
					UPDATE LINK.NGANHANG.dbo.TaiKhoan SET SODU = SODU - @SOTIEN WHERE SOTK= @TAIKHOAN
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV)VALUES (@TAIKHOAN, 'RT', @SOTIEN, @MANV )
					RETURN
				END
		END
	RAISERROR ('Tai khoan khong ton tai',16,1)

END
GO
/****** Object:  StoredProcedure [dbo].[SP_LAY_SODU_TAIKHOAN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_LAY_SODU_TAIKHOAN]
	-- Add the parameters for the stored procedure here
@SOTK nchar(9),
@SODU money OUTPUT
AS
BEGIN
	IF(EXISTS(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT @SODU = TK.SODU FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK
		END
	IF(EXISTS(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT @SODU = TK.SODU FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_LAY_SOTK_NGAUNHIEN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_LAY_SOTK_NGAUNHIEN]
	-- Add the parameters for the stored procedure here
@SOTK NCHAR(9) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 @SOTK = N.SOTK FROM NGANHANG.dbo.NGAUNHIENSTK AS N WHERE N.TRANGTHAI = 0 ORDER BY NEWID() 
	UPDATE NGANHANG.dbo.NGAUNHIENSTK SET TRANGTHAI = 1 WHERE SOTK = @SOTK
	RETURN @SOTK
END

GO
/****** Object:  StoredProcedure [dbo].[SP_LK_TaiKhoan_ThoiGian]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_LK_TaiKhoan_ThoiGian]
	-- Add the parameters for the stored procedure here
@TGDAU datetime,
@TGCUOI datetime,
@MACN nchar(10),
@LUACHON int
AS
BEGIN
	
	IF(@LUACHON = 1)
		BEGIN
			SELECT TK.SOTK,TK.CMND,TK.SODU,CN.TENCN,TK.NGAYMOTK FROM NGANHANG.dbo.TaiKhoan AS TK INNER JOIN NGANHANG.dbo.ChiNhanh AS CN ON TK.MACN = CN.MACN WHERE TK.NGAYMOTK>=@TGDAU AND TK.NGAYMOTK<=@TGCUOI
			UNION
			SELECT TK.SOTK,TK.CMND,TK.SODU,CN.TENCN,TK.NGAYMOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK INNER JOIN LINK.NGANHANG.dbo.ChiNhanh AS CN ON TK.MACN = CN.MACN WHERE TK.NGAYMOTK>=@TGDAU AND TK.NGAYMOTK<=@TGCUOI
			ORDER BY TK.NGAYMOTK ASC
		END
	IF(@LUACHON = 0)
		BEGIN
			DECLARE @TENCN NVARCHAR(100) 
			IF(EXISTS(SELECT * FROM NGANHANG.dbo.ChiNhanh AS CN WHERE CN.MACN = @MACN))
				BEGIN
					SELECT @TENCN = CN.TENCN FROM NGANHANG.dbo.ChiNhanh AS CN WHERE CN.MACN = @MACN
					SELECT TK.SOTK,TK.CMND,TK.SODU,TENCN = @TENCN,TK.NGAYMOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.NGAYMOTK>@TGDAU AND TK.NGAYMOTK<@TGCUOI ORDER BY TK.NGAYMOTK ASC
				END
			IF(EXISTS(SELECT * FROM LINK.NGANHANG.dbo.ChiNhanh AS CN WHERE CN.MACN = @MACN))
				BEGIN
					SELECT @TENCN = CN.TENCN FROM LINK.NGANHANG.dbo.ChiNhanh AS CN WHERE CN.MACN = @MACN
					SELECT TK.SOTK,TK.CMND,TK.SODU,TENCN = @TENCN,TK.NGAYMOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.NGAYMOTK>=@TGDAU AND TK.NGAYMOTK<=@TGCUOI ORDER BY TK.NGAYMOTK ASC
				END
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_SaoKe_GiaoDich_TaiKhoan]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_SaoKe_GiaoDich_TaiKhoan] 
	-- Add the parameters for the stored procedure here
@dau datetime,
@cuoi datetime,
@sotk nchar(9)
AS
BEGIN
	SET NOCOUNT ON;  
    IF 1=0 BEGIN  
        SET FMTONLY OFF  
    END  

	DECLARE @tientaidau money
	DECLARE @temp MONEY 
	DECLARE @tiengd MONEY 
	DECLARE @loaigd NCHAR(2) 
	DECLARE @dem int = 1
	DECLARE @count int

	EXEC SP_Tien_TK_Tai_Mot_THOIGIAN @sotk,@dau, @tientaidau output
	IF(EXISTS(SELECT SOTK FROM NGANHANG.dbo.TaiKhoan WHERE SOTK = @sotk))
		BEGIN
			SELECT STT= ROW_NUMBER() OVER (ORDER BY NGAYGD ASC) , SODUDAU = @tientaidau,*,SODUSAU = @tientaidau 
			INTO #TBLTAM 
			FROM (select NGAYGD,SOTIEN,LOAIGD from NGANHANG.DBO.GD_GOIRUT AS GD where SOTK = @SOTK AND GD.NGAYGD >@dau AND GD.NGAYGD < @cuoi
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'CT' from NGANHANG.DBO.GD_CHUYENTIEN AS CT where CT.SOTK_CHUYEN = @SOTK AND CT.NGAYGD >@dau AND CT.NGAYGD < @cuoi
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND  CT.NGAYGD >@dau AND CT.NGAYGD < @cuoi
			UNION
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from LINK.NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND  CT.NGAYGD >@dau AND CT.NGAYGD < @cuoi
			) AS TMP

			SELECT @count = COUNT(*) FROM #TBLTAM
			SET @temp = @tientaidau

			WHILE @dem <= @count
			BEGIN
				SELECT @tiengd = T.SOTIEN,@loaigd = T.LOAIGD FROM #TBLTAM AS T WHERE T.STT = @dem
				UPDATE #TBLTAM SET SODUDAU = @temp WHERE STT = @dem 
				SET @temp = @temp + (
				CASE 
					WHEN @loaigd = N'GT' THEN + @tiengd
					WHEN @loaigd = N'RT' THEN - @tiengd
					WHEN @loaigd = N'CT' THEN - @tiengd
					ELSE  
					+ @tiengd
				END)
				UPDATE #TBLTAM SET SODUSAU = @temp WHERE STT = @dem 
				SET @dem = @dem + 1
			END
			select TBL.SODUDAU,TBL.NGAYGD,TBL.LOAIGD,TBL.SOTIEN,TBL.SODUSAU from #TBLTAM AS TBL
		END
	IF(EXISTS(SELECT SOTK FROM LINK.NGANHANG.dbo.TaiKhoan WHERE SOTK = @sotk))
		BEGIN
			SELECT STT= ROW_NUMBER() OVER (ORDER BY NGAYGD ASC) , SODUDAU = @tientaidau,*,SODUSAU = @tientaidau 
			INTO #TBLTAM1 
			FROM (select NGAYGD,SOTIEN,LOAIGD from LINK.NGANHANG.DBO.GD_GOIRUT AS GD where SOTK = @SOTK AND GD.NGAYGD >@dau AND GD.NGAYGD < @cuoi
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'CT' from LINK.NGANHANG.DBO.GD_CHUYENTIEN AS CT where CT.SOTK_CHUYEN = @SOTK AND CT.NGAYGD >@dau AND CT.NGAYGD < @cuoi
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from LINK.NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND  CT.NGAYGD >@dau AND CT.NGAYGD < @cuoi
			UNION
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND  CT.NGAYGD >@dau AND CT.NGAYGD < @cuoi
			) AS TMP

			SELECT @count = COUNT(*) FROM #TBLTAM1
			SET @temp = @tientaidau

			WHILE @dem <= @count
			BEGIN
				SELECT @tiengd = T.SOTIEN,@loaigd = T.LOAIGD FROM #TBLTAM1 AS T WHERE T.STT = @dem
				UPDATE #TBLTAM1 SET SODUDAU = @temp WHERE STT = @dem 
				SET @temp = @temp + (
				CASE 
					WHEN @loaigd = N'GT' THEN + @tiengd
					WHEN @loaigd = N'RT' THEN - @tiengd
					WHEN @loaigd = N'CT' THEN - @tiengd
					ELSE  
					+ @tiengd
				END)
				UPDATE #TBLTAM1 SET SODUSAU = @temp WHERE STT = @dem 
				SET @dem = @dem + 1
			END
			select TBL.SODUDAU,TBL.NGAYGD,TBL.LOAIGD,TBL.SOTIEN,TBL.SODUSAU from #TBLTAM1 AS TBL
		END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_TAOLOGIN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_TAOLOGIN]
  @LGNAME VARCHAR(50),
  @PASS VARCHAR(50),
  @USERNAME VARCHAR(50),
  @ROLE VARCHAR(50)
AS
  DECLARE @RET INT
  EXEC @RET= SP_ADDLOGIN @LGNAME, @PASS,'NGANHANG'
  IF (@RET =1)  -- LOGIN NAME BI TRUNG
   
  BEGIN
  RAISERROR (N'Login name bị trùng', 16,1)

     RETURN 
	END

  EXEC @RET= SP_GRANTDBACCESS @LGNAME, @USERNAME --tao user
  IF (@RET =1)  -- USER  NAME BI TRUNG
  BEGIN
       EXEC SP_DROPLOGIN @LGNAME
	   RAISERROR (N' name bị trùng', 16,2)
       RETURN 
  END

  EXEC sp_addrolemember @ROLE, @USERNAME
  IF @ROLE= 'NGANHANG' OR @ROLE = 'CHINHANH'
  BEGIN
    EXEC sp_addsrvrolemember @LGNAME, 'securityadmin'
	EXEC sp_addsrvrolemember @LGNAME, 'processadmin'	
	END
	SELECT KQ = 0
RETURN  -- THANH CONG
GO
/****** Object:  StoredProcedure [dbo].[SP_Tien_TK_Tai_Mot_THOIGIAN]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Tien_TK_Tai_Mot_THOIGIAN]
	-- Add the parameters for the stored procedure here
@SOTK nchar(9),
@THOIGIAN datetime,
@SOTIEN money OUTPUT
AS
BEGIN
	EXEC SP_LAY_SODU_TAIKHOAN @SOTK, @SOTIEN OUTPUT

	IF(EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT @SOTIEN = (
						CASE 
							WHEN TMP.LOAIGD = N'GT' THEN  @SOTIEN - TMP.SOTIEN
							WHEN TMP.LOAIGD = N'RT' THEN  @SOTIEN + TMP.SOTIEN
							WHEN TMP.LOAIGD = N'CT' THEN  @SOTIEN + TMP.SOTIEN
							ELSE  @SOTIEN - TMP.SOTIEN
						END)
			FROM (select NGAYGD,SOTIEN,LOAIGD from NGANHANG.DBO.GD_GOIRUT AS GD where SOTK = @SOTK AND GD.NGAYGD > @THOIGIAN
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'CT' from NGANHANG.DBO.GD_CHUYENTIEN AS CT where CT.SOTK_CHUYEN = @SOTK AND CT.NGAYGD > @THOIGIAN
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND CT.NGAYGD > @THOIGIAN
			UNION
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from LINK.NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND CT.NGAYGD > @THOIGIAN
			) AS TMP
		END 
	IF(EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK))
		BEGIN
			SELECT @SOTIEN = (
						CASE 
							WHEN TMP.LOAIGD = N'GT' THEN  @SOTIEN - TMP.SOTIEN
							WHEN TMP.LOAIGD = N'RT' THEN  @SOTIEN + TMP.SOTIEN
							WHEN TMP.LOAIGD = N'CT' THEN  @SOTIEN + TMP.SOTIEN
							ELSE  @SOTIEN - TMP.SOTIEN
						END)
			FROM (select NGAYGD,SOTIEN,LOAIGD from LINK.NGANHANG.DBO.GD_GOIRUT AS GD where SOTK = @SOTK AND GD.NGAYGD > @THOIGIAN
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'CT' from LINK.NGANHANG.DBO.GD_CHUYENTIEN AS CT where CT.SOTK_CHUYEN = @SOTK AND CT.NGAYGD > @THOIGIAN
			UNION 
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from LINK.NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND CT.NGAYGD > @THOIGIAN
			UNION
			select NGAYGD,SOTIEN,LOAIGD=N'NT' from NGANHANG.DBO.GD_CHUYENTIEN AS CT where SOTK_NHAN =  @SOTK AND CT.NGAYGD > @THOIGIAN
			) AS TMP
		END
END

GO
/****** Object:  StoredProcedure [dbo].[THONG_KE_KH]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[THONG_KE_KH]
@MACN nchar(10)

AS
BEGIN 
IF (exists (SELECT CN.MACN FROM NGANHANG.dbo.ChiNhanh AS CN WHERE CN.MACN =@MACN ))
	BEGIN
	SELECT * FROM NGANHANG.dbo.KhachHang AS KH WHERE KH.MACN = @MACN ORDER BY TEN,  HO
	END
ELSE
	BEGIN
	SELECT * FROM LINK.NGANHANG.dbo.KhachHang AS KH WHERE KH.MACN = @MACN ORDER BY HO, TEN
	END

END
GO
/****** Object:  StoredProcedure [dbo].[Xoa_Login]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Xoa_Login]
  @LGNAME VARCHAR(50),
  @USRNAME VARCHAR(50)
  
AS
EXEC SP_DROPUSER @USRNAME
  EXEC SP_DROPLOGIN @LGNAME
GO
/****** Object:  Trigger [dbo].[TG_FK_KH_TK]    Script Date: 12/16/2020 4:35:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TG_FK_KH_TK] ON [NGANHANG].[dbo].[KhachHang]
after UPDATE
AS 
BEGIN
	DECLARE @CMND NCHAR(10)
	SELECT @CMND = CMND FROM deleted
	UPDATE NGANHANG.DBO.TaiKhoan SET CMND = inserted.CMND FROM NGANHANG.DBO.TaiKhoan AS TK JOIN inserted ON TK.CMND = @CMND
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'‘GT’ : gởi tiền vào TK , ‘RT’ : rút tiền khỏi TK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GD_GOIRUT', @level2type=N'COLUMN',@level2name=N'LOAIGD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_LAYMANHANVIEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_LAYMANHANVIEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ChiNhanh"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_MACN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_MACN'
GO
