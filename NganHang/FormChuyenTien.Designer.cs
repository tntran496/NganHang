﻿namespace NganHang
{
    partial class FormChuyenTien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label mAGDLabel;
            System.Windows.Forms.Label sOTK_CHUYENLabel;
            System.Windows.Forms.Label nGAYGDLabel;
            System.Windows.Forms.Label sOTIENLabel;
            System.Windows.Forms.Label sOTK_NHANLabel;
            System.Windows.Forms.Label mANVLabel;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gD_CHUYENTIENGridControl = new DevExpress.XtraGrid.GridControl();
            this.gD_CHUYENTIENBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nGANHANGDataSet = new NganHang.NGANHANGDataSet();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAGD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNGAYGD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOTK_CHUYEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOTK_NHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMANV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItemTHem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLamMOi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLuu = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemThoat = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.groupBoxThongTinGIaoDich = new System.Windows.Forms.GroupBox();
            this.mANVTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sOTIENSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.nGAYGDDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.mAGDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sOTK_CHUYENTextEdit = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sOTK_NHANTextEdit = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxChiNhanh = new System.Windows.Forms.ComboBox();
            this.nGANHANGDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gD_CHUYENTIENTableAdapter = new NganHang.NGANHANGDataSetTableAdapters.GD_CHUYENTIENTableAdapter();
            this.tableAdapterManager = new NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            mAGDLabel = new System.Windows.Forms.Label();
            sOTK_CHUYENLabel = new System.Windows.Forms.Label();
            nGAYGDLabel = new System.Windows.Forms.Label();
            sOTIENLabel = new System.Windows.Forms.Label();
            sOTK_NHANLabel = new System.Windows.Forms.Label();
            mANVLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gD_CHUYENTIENGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gD_CHUYENTIENBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBoxThongTinGIaoDich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mANVTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTIENSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYGDDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYGDDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAGDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTK_CHUYENTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTK_NHANTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSetBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mAGDLabel
            // 
            mAGDLabel.AutoSize = true;
            mAGDLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            mAGDLabel.Location = new System.Drawing.Point(1251, 41);
            mAGDLabel.Name = "mAGDLabel";
            mAGDLabel.Size = new System.Drawing.Size(151, 24);
            mAGDLabel.TabIndex = 0;
            mAGDLabel.Text = "Mã giao dịch :";
            // 
            // sOTK_CHUYENLabel
            // 
            sOTK_CHUYENLabel.AutoSize = true;
            sOTK_CHUYENLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sOTK_CHUYENLabel.Location = new System.Drawing.Point(43, 38);
            sOTK_CHUYENLabel.Name = "sOTK_CHUYENLabel";
            sOTK_CHUYENLabel.Size = new System.Drawing.Size(228, 24);
            sOTK_CHUYENLabel.TabIndex = 2;
            sOTK_CHUYENLabel.Text = "Số tài khoản chuyển :";
            // 
            // nGAYGDLabel
            // 
            nGAYGDLabel.AutoSize = true;
            nGAYGDLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nGAYGDLabel.Location = new System.Drawing.Point(738, 97);
            nGAYGDLabel.Name = "nGAYGDLabel";
            nGAYGDLabel.Size = new System.Drawing.Size(75, 24);
            nGAYGDLabel.TabIndex = 4;
            nGAYGDLabel.Text = "Ngày :";
            // 
            // sOTIENLabel
            // 
            sOTIENLabel.AutoSize = true;
            sOTIENLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sOTIENLabel.Location = new System.Drawing.Point(717, 40);
            sOTIENLabel.Name = "sOTIENLabel";
            sOTIENLabel.Size = new System.Drawing.Size(93, 24);
            sOTIENLabel.TabIndex = 6;
            sOTIENLabel.Text = "Số tiền :";
            // 
            // sOTK_NHANLabel
            // 
            sOTK_NHANLabel.AutoSize = true;
            sOTK_NHANLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sOTK_NHANLabel.Location = new System.Drawing.Point(70, 97);
            sOTK_NHANLabel.Name = "sOTK_NHANLabel";
            sOTK_NHANLabel.Size = new System.Drawing.Size(205, 24);
            sOTK_NHANLabel.TabIndex = 8;
            sOTK_NHANLabel.Text = "Số tài khoản nhận :";
            // 
            // mANVLabel
            // 
            mANVLabel.AutoSize = true;
            mANVLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            mANVLabel.Location = new System.Drawing.Point(1243, 98);
            mANVLabel.Name = "mANVLabel";
            mANVLabel.Size = new System.Drawing.Size(159, 24);
            mANVLabel.TabIndex = 10;
            mANVLabel.Text = "Mã nhân viên :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gD_CHUYENTIENGridControl);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 325);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1796, 368);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách giao dịch";
            // 
            // gD_CHUYENTIENGridControl
            // 
            this.gD_CHUYENTIENGridControl.CausesValidation = false;
            this.gD_CHUYENTIENGridControl.DataSource = this.gD_CHUYENTIENBindingSource;
            this.gD_CHUYENTIENGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gD_CHUYENTIENGridControl.Location = new System.Drawing.Point(3, 32);
            this.gD_CHUYENTIENGridControl.MainView = this.gridView1;
            this.gD_CHUYENTIENGridControl.MenuManager = this.barManager1;
            this.gD_CHUYENTIENGridControl.Name = "gD_CHUYENTIENGridControl";
            this.gD_CHUYENTIENGridControl.Size = new System.Drawing.Size(1790, 333);
            this.gD_CHUYENTIENGridControl.TabIndex = 0;
            this.gD_CHUYENTIENGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gD_CHUYENTIENBindingSource
            // 
            this.gD_CHUYENTIENBindingSource.DataMember = "GD_CHUYENTIEN";
            this.gD_CHUYENTIENBindingSource.DataSource = this.nGANHANGDataSet;
            // 
            // nGANHANGDataSet
            // 
            this.nGANHANGDataSet.DataSetName = "NGANHANGDataSet";
            this.nGANHANGDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAGD,
            this.colNGAYGD,
            this.colSOTK_CHUYEN,
            this.colSOTIEN,
            this.colSOTK_NHAN,
            this.colMANV});
            this.gridView1.GridControl = this.gD_CHUYENTIENGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            // 
            // colMAGD
            // 
            this.colMAGD.AppearanceCell.Options.UseTextOptions = true;
            this.colMAGD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMAGD.FieldName = "MAGD";
            this.colMAGD.MinWidth = 30;
            this.colMAGD.Name = "colMAGD";
            this.colMAGD.Visible = true;
            this.colMAGD.VisibleIndex = 0;
            this.colMAGD.Width = 112;
            // 
            // colNGAYGD
            // 
            this.colNGAYGD.AppearanceCell.Options.UseTextOptions = true;
            this.colNGAYGD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNGAYGD.FieldName = "NGAYGD";
            this.colNGAYGD.MinWidth = 30;
            this.colNGAYGD.Name = "colNGAYGD";
            this.colNGAYGD.Visible = true;
            this.colNGAYGD.VisibleIndex = 1;
            this.colNGAYGD.Width = 112;
            // 
            // colSOTK_CHUYEN
            // 
            this.colSOTK_CHUYEN.AppearanceCell.Options.UseTextOptions = true;
            this.colSOTK_CHUYEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSOTK_CHUYEN.FieldName = "SOTK_CHUYEN";
            this.colSOTK_CHUYEN.MinWidth = 30;
            this.colSOTK_CHUYEN.Name = "colSOTK_CHUYEN";
            this.colSOTK_CHUYEN.Visible = true;
            this.colSOTK_CHUYEN.VisibleIndex = 2;
            this.colSOTK_CHUYEN.Width = 112;
            // 
            // colSOTIEN
            // 
            this.colSOTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.colSOTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSOTIEN.DisplayFormat.FormatString = "{0:#,# VND}";
            this.colSOTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSOTIEN.FieldName = "SOTIEN";
            this.colSOTIEN.MinWidth = 30;
            this.colSOTIEN.Name = "colSOTIEN";
            this.colSOTIEN.Visible = true;
            this.colSOTIEN.VisibleIndex = 3;
            this.colSOTIEN.Width = 112;
            // 
            // colSOTK_NHAN
            // 
            this.colSOTK_NHAN.AppearanceCell.Options.UseTextOptions = true;
            this.colSOTK_NHAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSOTK_NHAN.FieldName = "SOTK_NHAN";
            this.colSOTK_NHAN.MinWidth = 30;
            this.colSOTK_NHAN.Name = "colSOTK_NHAN";
            this.colSOTK_NHAN.Visible = true;
            this.colSOTK_NHAN.VisibleIndex = 4;
            this.colSOTK_NHAN.Width = 112;
            // 
            // colMANV
            // 
            this.colMANV.AppearanceCell.Options.UseTextOptions = true;
            this.colMANV.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMANV.FieldName = "MANV";
            this.colMANV.MinWidth = 30;
            this.colMANV.Name = "colMANV";
            this.colMANV.Visible = true;
            this.colMANV.VisibleIndex = 5;
            this.colMANV.Width = 112;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemTHem,
            this.barButtonItem1,
            this.barButtonItemXoa,
            this.barButtonItem3,
            this.barButtonItemLuu,
            this.barButtonItemThoat,
            this.barSubItem1,
            this.barButtonItemLamMOi});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 8;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemTHem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemXoa, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemLamMOi, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemLuu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemThoat, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItemTHem
            // 
            this.barButtonItemTHem.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemTHem.Caption = "Thêm";
            this.barButtonItemTHem.Id = 0;
            this.barButtonItemTHem.ImageOptions.Image = global::NganHang.Properties.Resources.Button_Add_icon__1_;
            this.barButtonItemTHem.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemTHem.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemTHem.Name = "barButtonItemTHem";
            this.barButtonItemTHem.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemTHem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTHEM_ItemClick);
            // 
            // barButtonItemXoa
            // 
            this.barButtonItemXoa.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemXoa.Caption = "Xóa";
            this.barButtonItemXoa.Id = 2;
            this.barButtonItemXoa.ImageOptions.Image = global::NganHang.Properties.Resources.cancel_16x163;
            this.barButtonItemXoa.ImageOptions.LargeImage = global::NganHang.Properties.Resources.cancel_32x324;
            this.barButtonItemXoa.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemXoa.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemXoa.Name = "barButtonItemXoa";
            this.barButtonItemXoa.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemXOA_ItemClick);
            // 
            // barButtonItemLamMOi
            // 
            this.barButtonItemLamMOi.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemLamMOi.Caption = "Làm Mới";
            this.barButtonItemLamMOi.Id = 7;
            this.barButtonItemLamMOi.ImageOptions.Image = global::NganHang.Properties.Resources.refreshpivottable_16x161;
            this.barButtonItemLamMOi.ImageOptions.LargeImage = global::NganHang.Properties.Resources.refreshpivottable_32x321;
            this.barButtonItemLamMOi.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemLamMOi.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemLamMOi.Name = "barButtonItemLamMOi";
            this.barButtonItemLamMOi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItemLuu
            // 
            this.barButtonItemLuu.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemLuu.Caption = "Lưu";
            this.barButtonItemLuu.Enabled = false;
            this.barButtonItemLuu.Id = 4;
            this.barButtonItemLuu.ImageOptions.Image = global::NganHang.Properties.Resources.saveto_16x16;
            this.barButtonItemLuu.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemLuu.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemLuu.Name = "barButtonItemLuu";
            this.barButtonItemLuu.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemLuu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonLuu_ItemClick);
            // 
            // barButtonItemThoat
            // 
            this.barButtonItemThoat.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemThoat.Caption = "Thoát";
            this.barButtonItemThoat.Enabled = false;
            this.barButtonItemThoat.Id = 5;
            this.barButtonItemThoat.ImageOptions.Image = global::NganHang.Properties.Resources.Log_Out_icon;
            this.barButtonItemThoat.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemThoat.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemThoat.Name = "barButtonItemThoat";
            this.barButtonItemThoat.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemThoat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonThoat_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1796, 45);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 693);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1796, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 45);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 648);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1796, 45);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 648);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Sửa";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Phục Hồi";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "barSubItem1";
            this.barSubItem1.Id = 6;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // groupBoxThongTinGIaoDich
            // 
            this.groupBoxThongTinGIaoDich.Controls.Add(mANVLabel);
            this.groupBoxThongTinGIaoDich.Controls.Add(this.mANVTextEdit);
            this.groupBoxThongTinGIaoDich.Controls.Add(sOTK_NHANLabel);
            this.groupBoxThongTinGIaoDich.Controls.Add(sOTIENLabel);
            this.groupBoxThongTinGIaoDich.Controls.Add(this.sOTIENSpinEdit);
            this.groupBoxThongTinGIaoDich.Controls.Add(nGAYGDLabel);
            this.groupBoxThongTinGIaoDich.Controls.Add(this.nGAYGDDateEdit);
            this.groupBoxThongTinGIaoDich.Controls.Add(sOTK_CHUYENLabel);
            this.groupBoxThongTinGIaoDich.Controls.Add(mAGDLabel);
            this.groupBoxThongTinGIaoDich.Controls.Add(this.mAGDSpinEdit);
            this.groupBoxThongTinGIaoDich.Controls.Add(this.sOTK_CHUYENTextEdit);
            this.groupBoxThongTinGIaoDich.Controls.Add(this.sOTK_NHANTextEdit);
            this.groupBoxThongTinGIaoDich.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxThongTinGIaoDich.Enabled = false;
            this.groupBoxThongTinGIaoDich.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxThongTinGIaoDich.Location = new System.Drawing.Point(0, 45);
            this.groupBoxThongTinGIaoDich.Name = "groupBoxThongTinGIaoDich";
            this.groupBoxThongTinGIaoDich.Size = new System.Drawing.Size(1796, 143);
            this.groupBoxThongTinGIaoDich.TabIndex = 1;
            this.groupBoxThongTinGIaoDich.TabStop = false;
            this.groupBoxThongTinGIaoDich.Text = "Thông tin giao dịch";
            // 
            // mANVTextEdit
            // 
            this.mANVTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_CHUYENTIENBindingSource, "MANV", true));
            this.mANVTextEdit.Enabled = false;
            this.mANVTextEdit.Location = new System.Drawing.Point(1447, 94);
            this.mANVTextEdit.MenuManager = this.barManager1;
            this.mANVTextEdit.Name = "mANVTextEdit";
            this.mANVTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mANVTextEdit.Properties.Appearance.Options.UseFont = true;
            this.mANVTextEdit.Size = new System.Drawing.Size(284, 32);
            this.mANVTextEdit.TabIndex = 11;
            // 
            // sOTIENSpinEdit
            // 
            this.sOTIENSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_CHUYENTIENBindingSource, "SOTIEN", true));
            this.sOTIENSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sOTIENSpinEdit.Location = new System.Drawing.Point(850, 37);
            this.sOTIENSpinEdit.MenuManager = this.barManager1;
            this.sOTIENSpinEdit.Name = "sOTIENSpinEdit";
            this.sOTIENSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sOTIENSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.sOTIENSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sOTIENSpinEdit.Properties.DisplayFormat.FormatString = "{0:#,#}";
            this.sOTIENSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.sOTIENSpinEdit.Size = new System.Drawing.Size(284, 32);
            this.sOTIENSpinEdit.TabIndex = 7;
            // 
            // nGAYGDDateEdit
            // 
            this.nGAYGDDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_CHUYENTIENBindingSource, "NGAYGD", true));
            this.nGAYGDDateEdit.EditValue = null;
            this.nGAYGDDateEdit.Enabled = false;
            this.nGAYGDDateEdit.Location = new System.Drawing.Point(850, 94);
            this.nGAYGDDateEdit.MenuManager = this.barManager1;
            this.nGAYGDDateEdit.Name = "nGAYGDDateEdit";
            this.nGAYGDDateEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nGAYGDDateEdit.Properties.Appearance.Options.UseFont = true;
            this.nGAYGDDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nGAYGDDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nGAYGDDateEdit.Size = new System.Drawing.Size(284, 32);
            this.nGAYGDDateEdit.TabIndex = 5;
            // 
            // mAGDSpinEdit
            // 
            this.mAGDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_CHUYENTIENBindingSource, "MAGD", true));
            this.mAGDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.mAGDSpinEdit.Enabled = false;
            this.mAGDSpinEdit.Location = new System.Drawing.Point(1447, 38);
            this.mAGDSpinEdit.MenuManager = this.barManager1;
            this.mAGDSpinEdit.Name = "mAGDSpinEdit";
            this.mAGDSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mAGDSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.mAGDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mAGDSpinEdit.Size = new System.Drawing.Size(284, 32);
            this.mAGDSpinEdit.TabIndex = 1;
            // 
            // sOTK_CHUYENTextEdit
            // 
            this.sOTK_CHUYENTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_CHUYENTIENBindingSource, "SOTK_CHUYEN", true));
            this.sOTK_CHUYENTextEdit.Location = new System.Drawing.Point(344, 38);
            this.sOTK_CHUYENTextEdit.MenuManager = this.barManager1;
            this.sOTK_CHUYENTextEdit.Name = "sOTK_CHUYENTextEdit";
            this.sOTK_CHUYENTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sOTK_CHUYENTextEdit.Properties.Appearance.Options.UseFont = true;
            this.sOTK_CHUYENTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sOTK_CHUYENTextEdit.Properties.NullText = "";
            this.sOTK_CHUYENTextEdit.Properties.PopupView = this.searchLookUpEdit1View;
            this.sOTK_CHUYENTextEdit.Size = new System.Drawing.Size(284, 32);
            this.sOTK_CHUYENTextEdit.TabIndex = 3;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // sOTK_NHANTextEdit
            // 
            this.sOTK_NHANTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_CHUYENTIENBindingSource, "SOTK_NHAN", true));
            this.sOTK_NHANTextEdit.Location = new System.Drawing.Point(344, 98);
            this.sOTK_NHANTextEdit.MenuManager = this.barManager1;
            this.sOTK_NHANTextEdit.Name = "sOTK_NHANTextEdit";
            this.sOTK_NHANTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sOTK_NHANTextEdit.Properties.Appearance.Options.UseFont = true;
            this.sOTK_NHANTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sOTK_NHANTextEdit.Properties.NullText = "";
            this.sOTK_NHANTextEdit.Properties.PopupView = this.gridView2;
            this.sOTK_NHANTextEdit.Size = new System.Drawing.Size(284, 32);
            this.sOTK_NHANTextEdit.TabIndex = 9;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(582, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(132, 27);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Chi Nhánh :";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // comboBoxChiNhanh
            // 
            this.comboBoxChiNhanh.DisplayMember = "TENSERVER";
            this.comboBoxChiNhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxChiNhanh.Enabled = false;
            this.comboBoxChiNhanh.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxChiNhanh.FormattingEnabled = true;
            this.comboBoxChiNhanh.Location = new System.Drawing.Point(742, 23);
            this.comboBoxChiNhanh.Name = "comboBoxChiNhanh";
            this.comboBoxChiNhanh.Size = new System.Drawing.Size(277, 32);
            this.comboBoxChiNhanh.TabIndex = 12;
            this.comboBoxChiNhanh.ValueMember = "TENSERVER";
            this.comboBoxChiNhanh.SelectedIndexChanged += new System.EventHandler(this.comboBoxChiNhanh_SelectedIndexChanged);
            // 
            // nGANHANGDataSetBindingSource
            // 
            this.nGANHANGDataSetBindingSource.DataSource = this.nGANHANGDataSet;
            this.nGANHANGDataSetBindingSource.Position = 0;
            // 
            // gD_CHUYENTIENTableAdapter
            // 
            this.gD_CHUYENTIENTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ChiNhanhTableAdapter = null;
            this.tableAdapterManager.GD_CHUYENTIENTableAdapter = this.gD_CHUYENTIENTableAdapter;
            this.tableAdapterManager.GD_GOIRUTTableAdapter = null;
            this.tableAdapterManager.KhachHangTableAdapter = null;
            this.tableAdapterManager.NhanVienTableAdapter = null;
            this.tableAdapterManager.TaiKhoanTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxChiNhanh);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 188);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1796, 68);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chọn chi nhánh";
            // 
            // FormChuyenTien
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1796, 693);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxThongTinGIaoDich);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FormChuyenTien";
            this.Text = "Chuyển Tiền";
            this.Load += new System.EventHandler(this.FormChuyenTien_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gD_CHUYENTIENGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gD_CHUYENTIENBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBoxThongTinGIaoDich.ResumeLayout(false);
            this.groupBoxThongTinGIaoDich.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mANVTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTIENSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYGDDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYGDDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAGDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTK_CHUYENTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTK_NHANTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSetBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxThongTinGIaoDich;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTHem;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLuu;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItemThoat;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private NGANHANGDataSet nGANHANGDataSet;
        private DevExpress.XtraEditors.TextEdit mANVTextEdit;
        private DevExpress.XtraEditors.SpinEdit sOTIENSpinEdit;
        private DevExpress.XtraEditors.DateEdit nGAYGDDateEdit;
        private DevExpress.XtraEditors.SpinEdit mAGDSpinEdit;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private System.Windows.Forms.ComboBox comboBoxChiNhanh;
        private System.Windows.Forms.BindingSource nGANHANGDataSetBindingSource;
        private System.Windows.Forms.BindingSource gD_CHUYENTIENBindingSource;
        private NGANHANGDataSetTableAdapters.GD_CHUYENTIENTableAdapter gD_CHUYENTIENTableAdapter;
        private NGANHANGDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraGrid.GridControl gD_CHUYENTIENGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colMAGD;
        private DevExpress.XtraGrid.Columns.GridColumn colNGAYGD;
        private DevExpress.XtraGrid.Columns.GridColumn colSOTK_CHUYEN;
        private DevExpress.XtraGrid.Columns.GridColumn colSOTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn colSOTK_NHAN;
        private DevExpress.XtraGrid.Columns.GridColumn colMANV;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLamMOi;
        private DevExpress.XtraEditors.SearchLookUpEdit sOTK_CHUYENTextEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.SearchLookUpEdit sOTK_NHANTextEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    }
}