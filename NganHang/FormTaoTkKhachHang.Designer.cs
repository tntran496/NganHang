﻿namespace NganHang
{
    partial class FormTaoTkKhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label sOTKLabel;
            System.Windows.Forms.Label cMNDLabel;
            System.Windows.Forms.Label sODULabel;
            System.Windows.Forms.Label mACNLabel;
            System.Windows.Forms.Label nGAYMOTKLabel;
            this.groupBoxTaiKHoanKH = new System.Windows.Forms.GroupBox();
            this.cMNDComboBox = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.taiKhoanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nGANHANGDataSet = new NganHang.NGANHANGDataSet();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItemThem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSua = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPhucHoi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLamMOi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLuu = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemThoat = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.cMNDSearchLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.nGAYMOTKDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.mACNTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sODUSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sOTKTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.taiKhoanGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSOTK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNGAYMOTK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSODU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMACN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxChiNhanh = new System.Windows.Forms.ComboBox();
            this.taiKhoanTableAdapter = new NganHang.NGANHANGDataSetTableAdapters.TaiKhoanTableAdapter();
            this.tableAdapterManager = new NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            sOTKLabel = new System.Windows.Forms.Label();
            cMNDLabel = new System.Windows.Forms.Label();
            sODULabel = new System.Windows.Forms.Label();
            mACNLabel = new System.Windows.Forms.Label();
            nGAYMOTKLabel = new System.Windows.Forms.Label();
            this.groupBoxTaiKHoanKH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMNDComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taiKhoanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMNDSearchLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYMOTKDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYMOTKDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mACNTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sODUSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTKTextEdit.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.taiKhoanGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sOTKLabel
            // 
            sOTKLabel.AutoSize = true;
            sOTKLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sOTKLabel.Location = new System.Drawing.Point(63, 44);
            sOTKLabel.Name = "sOTKLabel";
            sOTKLabel.Size = new System.Drawing.Size(152, 25);
            sOTKLabel.TabIndex = 0;
            sOTKLabel.Text = "Số Tài Khoản:";
            // 
            // cMNDLabel
            // 
            cMNDLabel.AutoSize = true;
            cMNDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cMNDLabel.Location = new System.Drawing.Point(132, 92);
            cMNDLabel.Name = "cMNDLabel";
            cMNDLabel.Size = new System.Drawing.Size(83, 25);
            cMNDLabel.TabIndex = 2;
            cMNDLabel.Text = "CMND:";
            // 
            // sODULabel
            // 
            sODULabel.AutoSize = true;
            sODULabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sODULabel.Location = new System.Drawing.Point(700, 44);
            sODULabel.Name = "sODULabel";
            sODULabel.Size = new System.Drawing.Size(79, 25);
            sODULabel.TabIndex = 4;
            sODULabel.Text = "Số Dư:";
            // 
            // mACNLabel
            // 
            mACNLabel.AutoSize = true;
            mACNLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            mACNLabel.Location = new System.Drawing.Point(624, 92);
            mACNLabel.Name = "mACNLabel";
            mACNLabel.Size = new System.Drawing.Size(155, 25);
            mACNLabel.TabIndex = 6;
            mACNLabel.Text = "Mã chi nhánh :";
            // 
            // nGAYMOTKLabel
            // 
            nGAYMOTKLabel.AutoSize = true;
            nGAYMOTKLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nGAYMOTKLabel.Location = new System.Drawing.Point(1266, 44);
            nGAYMOTKLabel.Name = "nGAYMOTKLabel";
            nGAYMOTKLabel.Size = new System.Drawing.Size(105, 25);
            nGAYMOTKLabel.TabIndex = 8;
            nGAYMOTKLabel.Text = "Ngày Mở:";
            // 
            // groupBoxTaiKHoanKH
            // 
            this.groupBoxTaiKHoanKH.Controls.Add(this.cMNDComboBox);
            this.groupBoxTaiKHoanKH.Controls.Add(nGAYMOTKLabel);
            this.groupBoxTaiKHoanKH.Controls.Add(this.nGAYMOTKDateEdit);
            this.groupBoxTaiKHoanKH.Controls.Add(mACNLabel);
            this.groupBoxTaiKHoanKH.Controls.Add(this.mACNTextEdit);
            this.groupBoxTaiKHoanKH.Controls.Add(sODULabel);
            this.groupBoxTaiKHoanKH.Controls.Add(this.sODUSpinEdit);
            this.groupBoxTaiKHoanKH.Controls.Add(cMNDLabel);
            this.groupBoxTaiKHoanKH.Controls.Add(sOTKLabel);
            this.groupBoxTaiKHoanKH.Controls.Add(this.sOTKTextEdit);
            this.groupBoxTaiKHoanKH.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxTaiKHoanKH.Enabled = false;
            this.groupBoxTaiKHoanKH.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTaiKHoanKH.Location = new System.Drawing.Point(0, 45);
            this.groupBoxTaiKHoanKH.Name = "groupBoxTaiKHoanKH";
            this.groupBoxTaiKHoanKH.Size = new System.Drawing.Size(1796, 143);
            this.groupBoxTaiKHoanKH.TabIndex = 1;
            this.groupBoxTaiKHoanKH.TabStop = false;
            this.groupBoxTaiKHoanKH.Text = "Thông Tin Tài Khoản";
            // 
            // cMNDComboBox
            // 
            this.cMNDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.taiKhoanBindingSource, "CMND", true));
            this.cMNDComboBox.EditValue = ".";
            this.cMNDComboBox.Location = new System.Drawing.Point(254, 88);
            this.cMNDComboBox.MenuManager = this.barManager1;
            this.cMNDComboBox.Name = "cMNDComboBox";
            this.cMNDComboBox.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cMNDComboBox.Properties.Appearance.Options.UseFont = true;
            this.cMNDComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cMNDComboBox.Properties.PopupView = this.cMNDSearchLookUpEditView;
            this.cMNDComboBox.Size = new System.Drawing.Size(341, 32);
            this.cMNDComboBox.TabIndex = 10;
            // 
            // taiKhoanBindingSource
            // 
            this.taiKhoanBindingSource.DataMember = "TaiKhoan";
            this.taiKhoanBindingSource.DataSource = this.nGANHANGDataSet;
            // 
            // nGANHANGDataSet
            // 
            this.nGANHANGDataSet.DataSetName = "NGANHANGDataSet";
            this.nGANHANGDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemThem,
            this.barButtonItemXoa,
            this.barButtonItemSua,
            this.barButtonItemPhucHoi,
            this.barButtonItemLuu,
            this.barButtonItemThoat,
            this.barButtonItemLamMOi});
            this.barManager1.MaxItemId = 7;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemThem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemSua, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemXoa, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemPhucHoi, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemLamMOi, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemLuu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemThoat, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.Text = "Custom 2";
            // 
            // barButtonItemThem
            // 
            this.barButtonItemThem.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemThem.Caption = "Thêm";
            this.barButtonItemThem.Id = 0;
            this.barButtonItemThem.ImageOptions.Image = global::NganHang.Properties.Resources.Button_Add_icon__1_;
            this.barButtonItemThem.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemThem.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemThem.Name = "barButtonItemThem";
            this.barButtonItemThem.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemThem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItemSua
            // 
            this.barButtonItemSua.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemSua.Caption = "Sửa";
            this.barButtonItemSua.Id = 2;
            this.barButtonItemSua.ImageOptions.Image = global::NganHang.Properties.Resources.edit_validated_icon;
            this.barButtonItemSua.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemSua.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemSua.Name = "barButtonItemSua";
            this.barButtonItemSua.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemSua.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSua_ItemClick);
            // 
            // barButtonItemXoa
            // 
            this.barButtonItemXoa.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemXoa.Caption = "Xóa";
            this.barButtonItemXoa.Id = 1;
            this.barButtonItemXoa.ImageOptions.Image = global::NganHang.Properties.Resources.cancel_16x162;
            this.barButtonItemXoa.ImageOptions.LargeImage = global::NganHang.Properties.Resources.cancel_32x323;
            this.barButtonItemXoa.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemXoa.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemXoa.Name = "barButtonItemXoa";
            this.barButtonItemXoa.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemXoa_ItemClick);
            // 
            // barButtonItemPhucHoi
            // 
            this.barButtonItemPhucHoi.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemPhucHoi.Caption = "Phục\r\nHồi\r\n";
            this.barButtonItemPhucHoi.Id = 3;
            this.barButtonItemPhucHoi.ImageOptions.Image = global::NganHang.Properties.Resources.refresh2_16x161;
            this.barButtonItemPhucHoi.ImageOptions.LargeImage = global::NganHang.Properties.Resources.refresh2_32x321;
            this.barButtonItemPhucHoi.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemPhucHoi.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemPhucHoi.Name = "barButtonItemPhucHoi";
            this.barButtonItemPhucHoi.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemPhucHoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPhucHoi_ItemClick);
            // 
            // barButtonItemLamMOi
            // 
            this.barButtonItemLamMOi.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemLamMOi.Caption = "Làm Mới";
            this.barButtonItemLamMOi.Id = 6;
            this.barButtonItemLamMOi.ImageOptions.Image = global::NganHang.Properties.Resources.refreshpivottable_16x162;
            this.barButtonItemLamMOi.ImageOptions.LargeImage = global::NganHang.Properties.Resources.refreshpivottable_32x322;
            this.barButtonItemLamMOi.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemLamMOi.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemLamMOi.Name = "barButtonItemLamMOi";
            this.barButtonItemLamMOi.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemLamMOi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLamMOi_ItemClick);
            // 
            // barButtonItemLuu
            // 
            this.barButtonItemLuu.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemLuu.Caption = "Lưu";
            this.barButtonItemLuu.Enabled = false;
            this.barButtonItemLuu.Id = 4;
            this.barButtonItemLuu.ImageOptions.Image = global::NganHang.Properties.Resources.saveto_16x16;
            this.barButtonItemLuu.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemLuu.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemLuu.Name = "barButtonItemLuu";
            this.barButtonItemLuu.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemLuu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLuu_ItemClick);
            // 
            // barButtonItemThoat
            // 
            this.barButtonItemThoat.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemThoat.Caption = "Thoát";
            this.barButtonItemThoat.Enabled = false;
            this.barButtonItemThoat.Id = 5;
            this.barButtonItemThoat.ImageOptions.Image = global::NganHang.Properties.Resources.Log_Out_icon;
            this.barButtonItemThoat.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemThoat.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemThoat.Name = "barButtonItemThoat";
            this.barButtonItemThoat.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemThoat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemThoat_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Appearance.BackColor = System.Drawing.Color.LightSkyBlue;
            this.barDockControlTop.Appearance.Options.UseBackColor = true;
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1796, 45);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 733);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1796, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 45);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 688);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1796, 45);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 688);
            // 
            // cMNDSearchLookUpEditView
            // 
            this.cMNDSearchLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.cMNDSearchLookUpEditView.Name = "cMNDSearchLookUpEditView";
            this.cMNDSearchLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.cMNDSearchLookUpEditView.OptionsView.ShowGroupPanel = false;
            // 
            // nGAYMOTKDateEdit
            // 
            this.nGAYMOTKDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.taiKhoanBindingSource, "NGAYMOTK", true));
            this.nGAYMOTKDateEdit.EditValue = null;
            this.nGAYMOTKDateEdit.Location = new System.Drawing.Point(1413, 35);
            this.nGAYMOTKDateEdit.MenuManager = this.barManager1;
            this.nGAYMOTKDateEdit.Name = "nGAYMOTKDateEdit";
            this.nGAYMOTKDateEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nGAYMOTKDateEdit.Properties.Appearance.Options.UseFont = true;
            this.nGAYMOTKDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nGAYMOTKDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nGAYMOTKDateEdit.Size = new System.Drawing.Size(341, 34);
            this.nGAYMOTKDateEdit.TabIndex = 9;
            // 
            // mACNTextEdit
            // 
            this.mACNTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.taiKhoanBindingSource, "MACN", true));
            this.mACNTextEdit.Enabled = false;
            this.mACNTextEdit.Location = new System.Drawing.Point(812, 88);
            this.mACNTextEdit.MenuManager = this.barManager1;
            this.mACNTextEdit.Name = "mACNTextEdit";
            this.mACNTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mACNTextEdit.Properties.Appearance.Options.UseFont = true;
            this.mACNTextEdit.Size = new System.Drawing.Size(341, 34);
            this.mACNTextEdit.TabIndex = 7;
            // 
            // sODUSpinEdit
            // 
            this.sODUSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.taiKhoanBindingSource, "SODU", true));
            this.sODUSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sODUSpinEdit.Location = new System.Drawing.Point(812, 35);
            this.sODUSpinEdit.MenuManager = this.barManager1;
            this.sODUSpinEdit.Name = "sODUSpinEdit";
            this.sODUSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sODUSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.sODUSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sODUSpinEdit.Properties.DisplayFormat.FormatString = "{0:#,#}";
            this.sODUSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.sODUSpinEdit.Size = new System.Drawing.Size(341, 34);
            this.sODUSpinEdit.TabIndex = 5;
            // 
            // sOTKTextEdit
            // 
            this.sOTKTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.taiKhoanBindingSource, "SOTK", true));
            this.sOTKTextEdit.Location = new System.Drawing.Point(254, 35);
            this.sOTKTextEdit.MenuManager = this.barManager1;
            this.sOTKTextEdit.Name = "sOTKTextEdit";
            this.sOTKTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sOTKTextEdit.Properties.Appearance.Options.UseFont = true;
            this.sOTKTextEdit.Size = new System.Drawing.Size(341, 34);
            this.sOTKTextEdit.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.taiKhoanGridControl);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 353);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1796, 380);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh Sách Tài Khoản";
            // 
            // taiKhoanGridControl
            // 
            this.taiKhoanGridControl.CausesValidation = false;
            this.taiKhoanGridControl.DataSource = this.taiKhoanBindingSource;
            this.taiKhoanGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taiKhoanGridControl.Location = new System.Drawing.Point(3, 32);
            this.taiKhoanGridControl.MainView = this.gridView1;
            this.taiKhoanGridControl.MenuManager = this.barManager1;
            this.taiKhoanGridControl.Name = "taiKhoanGridControl";
            this.taiKhoanGridControl.Size = new System.Drawing.Size(1790, 345);
            this.taiKhoanGridControl.TabIndex = 0;
            this.taiKhoanGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSOTK,
            this.colNGAYMOTK,
            this.colCMND,
            this.colSODU,
            this.colMACN});
            this.gridView1.GridControl = this.taiKhoanGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            // 
            // colSOTK
            // 
            this.colSOTK.AppearanceCell.Options.UseTextOptions = true;
            this.colSOTK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSOTK.FieldName = "SOTK";
            this.colSOTK.MinWidth = 30;
            this.colSOTK.Name = "colSOTK";
            this.colSOTK.Visible = true;
            this.colSOTK.VisibleIndex = 0;
            this.colSOTK.Width = 112;
            // 
            // colNGAYMOTK
            // 
            this.colNGAYMOTK.AppearanceCell.Options.UseTextOptions = true;
            this.colNGAYMOTK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNGAYMOTK.FieldName = "NGAYMOTK";
            this.colNGAYMOTK.MinWidth = 30;
            this.colNGAYMOTK.Name = "colNGAYMOTK";
            this.colNGAYMOTK.Visible = true;
            this.colNGAYMOTK.VisibleIndex = 1;
            this.colNGAYMOTK.Width = 112;
            // 
            // colCMND
            // 
            this.colCMND.AppearanceCell.Options.UseTextOptions = true;
            this.colCMND.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCMND.FieldName = "CMND";
            this.colCMND.MinWidth = 30;
            this.colCMND.Name = "colCMND";
            this.colCMND.Visible = true;
            this.colCMND.VisibleIndex = 2;
            this.colCMND.Width = 112;
            // 
            // colSODU
            // 
            this.colSODU.AppearanceCell.Options.UseTextOptions = true;
            this.colSODU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSODU.DisplayFormat.FormatString = "{0:#,# VND}";
            this.colSODU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSODU.FieldName = "SODU";
            this.colSODU.MinWidth = 30;
            this.colSODU.Name = "colSODU";
            this.colSODU.Visible = true;
            this.colSODU.VisibleIndex = 3;
            this.colSODU.Width = 112;
            // 
            // colMACN
            // 
            this.colMACN.AppearanceCell.Options.UseTextOptions = true;
            this.colMACN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMACN.FieldName = "MACN";
            this.colMACN.MinWidth = 30;
            this.colMACN.Name = "colMACN";
            this.colMACN.Visible = true;
            this.colMACN.VisibleIndex = 4;
            this.colMACN.Width = 112;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(548, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(117, 24);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Chi Nhánh :";
            // 
            // comboBoxChiNhanh
            // 
            this.comboBoxChiNhanh.DisplayMember = "TENSERVER";
            this.comboBoxChiNhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxChiNhanh.Enabled = false;
            this.comboBoxChiNhanh.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxChiNhanh.FormattingEnabled = true;
            this.comboBoxChiNhanh.Location = new System.Drawing.Point(688, 17);
            this.comboBoxChiNhanh.Name = "comboBoxChiNhanh";
            this.comboBoxChiNhanh.Size = new System.Drawing.Size(304, 32);
            this.comboBoxChiNhanh.TabIndex = 12;
            this.comboBoxChiNhanh.ValueMember = "TENSERVER";
            this.comboBoxChiNhanh.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // taiKhoanTableAdapter
            // 
            this.taiKhoanTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ChiNhanhTableAdapter = null;
            this.tableAdapterManager.GD_CHUYENTIENTableAdapter = null;
            this.tableAdapterManager.GD_GOIRUTTableAdapter = null;
            this.tableAdapterManager.KhachHangTableAdapter = null;
            this.tableAdapterManager.NhanVienTableAdapter = null;
            this.tableAdapterManager.TaiKhoanTableAdapter = this.taiKhoanTableAdapter;
            this.tableAdapterManager.UpdateOrder = NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxChiNhanh);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 188);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1796, 53);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " ";
            // 
            // FormTaoTkKhachHang
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1796, 733);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxTaiKHoanKH);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FormTaoTkKhachHang";
            this.Text = "Tạo Tài Khoản Khách Hàng";
            this.Load += new System.EventHandler(this.FormTaoTkKhachHang_Load);
            this.groupBoxTaiKHoanKH.ResumeLayout(false);
            this.groupBoxTaiKHoanKH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMNDComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taiKhoanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMNDSearchLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYMOTKDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYMOTKDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mACNTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sODUSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTKTextEdit.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.taiKhoanGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBoxTaiKHoanKH;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxChiNhanh;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemThem;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSua;
        private DevExpress.XtraBars.BarButtonItem barButtonItemXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPhucHoi;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLuu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemThoat;
        private DevExpress.XtraEditors.DateEdit nGAYMOTKDateEdit;
        private DevExpress.XtraEditors.TextEdit mACNTextEdit;
        private DevExpress.XtraEditors.SpinEdit sODUSpinEdit;
        private DevExpress.XtraEditors.TextEdit sOTKTextEdit;
        private NGANHANGDataSet nGANHANGDataSet;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private System.Windows.Forms.BindingSource taiKhoanBindingSource;
        private NGANHANGDataSetTableAdapters.TaiKhoanTableAdapter taiKhoanTableAdapter;
        private NGANHANGDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraGrid.GridControl taiKhoanGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSOTK;
        private DevExpress.XtraGrid.Columns.GridColumn colNGAYMOTK;
        private DevExpress.XtraGrid.Columns.GridColumn colCMND;
        private DevExpress.XtraGrid.Columns.GridColumn colSODU;
        private DevExpress.XtraGrid.Columns.GridColumn colMACN;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLamMOi;
        private DevExpress.XtraEditors.SearchLookUpEdit cMNDComboBox;
        private DevExpress.XtraGrid.Views.Grid.GridView cMNDSearchLookUpEditView;
    }
}