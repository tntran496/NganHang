﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace NganHang
{
    public partial class SaoKeTaiKhoanKH : DevExpress.XtraReports.UI.XtraReport
    {
        public SaoKeTaiKhoanKH(DateTime tuNgay, DateTime denNgay, String sotk)
        {
            InitializeComponent();
            xrLabelTaiKhoan.Text = sotk;
            xrLabelTuNgay.Text = tuNgay.ToString("dd/MM/yyyy");
            xrLabelDenNGay.Text = denNgay.ToString("dd/MM/yyyy");
            xrLabelNgayThangNam.Text = Program.tencn +","+DateTime.UtcNow.Date.ToString("dd/MM/yyyy");
            xrLabelHOTenNguoiLap.Text = Program.mHoten;
            this.sqlDataSource1.Connection.ConnectionString = Program.connstr;
            this.sqlDataSource1.Queries[0].Parameters[0].Value = tuNgay;
            this.sqlDataSource1.Queries[0].Parameters[1].Value = denNgay;
            this.sqlDataSource1.Queries[0].Parameters[2].Value = sotk;
        }

    }
}
