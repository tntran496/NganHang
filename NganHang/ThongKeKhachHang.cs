﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace NganHang
{
    public partial class ThongKeKhachHang : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongKeKhachHang(String macn)
        {
            InitializeComponent();
            xrLabelMaCn.Text = macn;
            xrLabelNgayThangNam.Text = Program.tencn + "," + DateTime.UtcNow.Date.ToString("dd/MM/yyyy");
            xrLabelHOTenNguoiLap.Text = Program.mHoten;
            this.sqlDataSource1.Connection.ConnectionString = Program.connstr;
            this.sqlDataSource1.Queries[0].Parameters[0].Value = macn;
        }

    }
}
