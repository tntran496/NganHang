﻿namespace NganHang
{
    partial class KhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItemThemKH = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSua = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDelete = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPhucHoi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemThoatKh = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonLamMoi = new DevExpress.XtraBars.BarButtonItem();
            this.skinBarSubItem1 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem2 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem3 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinDropDownButtonItem1 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.skinBarSubItem4 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinDropDownButtonItem2 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.skinBarSubItem5 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem6 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem7 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem8 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem9 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem10 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem11 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem12 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem13 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem14 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem15 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem16 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinDropDownButtonItem3 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.skinBarSubItem17 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem18 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem19 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem20 = new DevExpress.XtraBars.SkinBarSubItem();
            this.groupBoxThongTinNv = new System.Windows.Forms.GroupBox();
            this.comboBoxPhai = new System.Windows.Forms.ComboBox();
            this.khachHangBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nGANHANGDataSet = new NganHang.NGANHANGDataSet();
            this.textEditCN = new DevExpress.XtraEditors.TextEdit();
            this.dateeEditNgayCap = new DevExpress.XtraEditors.DateEdit();
            this.textEditSDT = new DevExpress.XtraEditors.TextEdit();
            this.textEditHo = new DevExpress.XtraEditors.TextEdit();
            this.textEditTen = new DevExpress.XtraEditors.TextEdit();
            this.textEditDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.textEditCMND = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxChiNhanh = new System.Windows.Forms.ComboBox();
            this.nGANHANGDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.khachHangGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNGAYCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSODT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMACN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.khachHangTableAdapter = new NganHang.NGANHANGDataSetTableAdapters.KhachHangTableAdapter();
            this.tableAdapterManager1 = new NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBoxThongTinNv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.khachHangBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateeEditNgayCap.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateeEditNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.khachHangGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemThemKH,
            this.barButtonItemSave,
            this.barButtonItemDelete,
            this.barButtonItemPhucHoi,
            this.barButtonLamMoi,
            this.barButtonItemThoatKh,
            this.barButtonItemSua,
            this.skinBarSubItem1,
            this.skinBarSubItem2,
            this.skinBarSubItem3,
            this.skinDropDownButtonItem1,
            this.skinBarSubItem4,
            this.skinDropDownButtonItem2,
            this.skinBarSubItem5,
            this.skinBarSubItem6,
            this.skinBarSubItem7,
            this.skinBarSubItem8,
            this.skinBarSubItem9,
            this.skinBarSubItem10,
            this.skinBarSubItem11,
            this.skinBarSubItem12,
            this.skinBarSubItem13,
            this.skinBarSubItem14,
            this.skinBarSubItem15,
            this.skinBarSubItem16,
            this.skinDropDownButtonItem3,
            this.skinBarSubItem17,
            this.skinBarSubItem18,
            this.skinBarSubItem19,
            this.skinBarSubItem20});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 30;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(179, 205);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemThemKH, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemSua, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemPhucHoi, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemThoatKh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItemThemKH
            // 
            this.barButtonItemThemKH.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemThemKH.Caption = "Thêm";
            this.barButtonItemThemKH.Id = 0;
            this.barButtonItemThemKH.ImageOptions.Image = global::NganHang.Properties.Resources.Button_Add_icon__1_;
            this.barButtonItemThemKH.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemThemKH.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemThemKH.Name = "barButtonItemThemKH";
            this.barButtonItemThemKH.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemThemKH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonThem_ItemClick);
            // 
            // barButtonItemSua
            // 
            this.barButtonItemSua.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemSua.Caption = "Sửa";
            this.barButtonItemSua.Id = 6;
            this.barButtonItemSua.ImageOptions.Image = global::NganHang.Properties.Resources.edit_validated_icon;
            this.barButtonItemSua.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemSua.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemSua.Name = "barButtonItemSua";
            this.barButtonItemSua.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemSua.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonSua_ItemClick_1);
            // 
            // barButtonItemDelete
            // 
            this.barButtonItemDelete.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemDelete.Caption = "Xóa";
            this.barButtonItemDelete.Id = 2;
            this.barButtonItemDelete.ImageOptions.Image = global::NganHang.Properties.Resources.cancel_16x16;
            this.barButtonItemDelete.ImageOptions.LargeImage = global::NganHang.Properties.Resources.cancel_32x321;
            this.barButtonItemDelete.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemDelete.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemDelete.Name = "barButtonItemDelete";
            this.barButtonItemDelete.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDelete_ItemClick);
            // 
            // barButtonItemPhucHoi
            // 
            this.barButtonItemPhucHoi.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemPhucHoi.Caption = "Phục\r\nHồi";
            this.barButtonItemPhucHoi.Id = 3;
            this.barButtonItemPhucHoi.ImageOptions.Image = global::NganHang.Properties.Resources.refresh2_16x16;
            this.barButtonItemPhucHoi.ImageOptions.LargeImage = global::NganHang.Properties.Resources.refresh2_32x32;
            this.barButtonItemPhucHoi.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemPhucHoi.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemPhucHoi.Name = "barButtonItemPhucHoi";
            this.barButtonItemPhucHoi.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemPhucHoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPhucHoi_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemSave.Caption = "Lưu";
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Id = 1;
            this.barButtonItemSave.ImageOptions.Image = global::NganHang.Properties.Resources.saveto_16x16;
            this.barButtonItemSave.ImageOptions.LargeImage = global::NganHang.Properties.Resources.saveto_32x32;
            this.barButtonItemSave.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemSave.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Black;
            this.barButtonItemSave.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemSave.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonLuu_ItemClick);
            // 
            // barButtonItemThoatKh
            // 
            this.barButtonItemThoatKh.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barButtonItemThoatKh.Caption = "Thoát";
            this.barButtonItemThoatKh.Enabled = false;
            this.barButtonItemThoatKh.Id = 5;
            this.barButtonItemThoatKh.ImageOptions.Image = global::NganHang.Properties.Resources.Log_Out_icon;
            this.barButtonItemThoatKh.ImageOptions.LargeImage = global::NganHang.Properties.Resources.changeview_32x32;
            this.barButtonItemThoatKh.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemThoatKh.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Black;
            this.barButtonItemThoatKh.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItemThoatKh.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barButtonItemThoatKh.ItemInMenuAppearance.Normal.BackColor = System.Drawing.Color.LightSkyBlue;
            this.barButtonItemThoatKh.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItemThoatKh.ItemInMenuAppearance.Normal.Options.UseBackColor = true;
            this.barButtonItemThoatKh.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barButtonItemThoatKh.Name = "barButtonItemThoatKh";
            this.barButtonItemThoatKh.Size = new System.Drawing.Size(85, 30);
            this.barButtonItemThoatKh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemThoatKh_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1802, 45);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 733);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1802, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 45);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 688);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1802, 45);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 688);
            // 
            // barButtonLamMoi
            // 
            this.barButtonLamMoi.Caption = "Làm Mới";
            this.barButtonLamMoi.Id = 4;
            this.barButtonLamMoi.ImageOptions.Image = global::NganHang.Properties.Resources.refresh_16x16;
            this.barButtonLamMoi.ImageOptions.LargeImage = global::NganHang.Properties.Resources.refresh_32x32;
            this.barButtonLamMoi.Name = "barButtonLamMoi";
            // 
            // skinBarSubItem1
            // 
            this.skinBarSubItem1.Caption = "skinBarSubItem1";
            this.skinBarSubItem1.Id = 7;
            this.skinBarSubItem1.Name = "skinBarSubItem1";
            // 
            // skinBarSubItem2
            // 
            this.skinBarSubItem2.Id = 8;
            this.skinBarSubItem2.Name = "skinBarSubItem2";
            // 
            // skinBarSubItem3
            // 
            this.skinBarSubItem3.Caption = "skinBarSubItem3";
            this.skinBarSubItem3.Id = 9;
            this.skinBarSubItem3.Name = "skinBarSubItem3";
            // 
            // skinDropDownButtonItem1
            // 
            this.skinDropDownButtonItem1.Id = 10;
            this.skinDropDownButtonItem1.Name = "skinDropDownButtonItem1";
            // 
            // skinBarSubItem4
            // 
            this.skinBarSubItem4.Caption = "skinBarSubItem4";
            this.skinBarSubItem4.Id = 11;
            this.skinBarSubItem4.Name = "skinBarSubItem4";
            // 
            // skinDropDownButtonItem2
            // 
            this.skinDropDownButtonItem2.Id = 12;
            this.skinDropDownButtonItem2.Name = "skinDropDownButtonItem2";
            // 
            // skinBarSubItem5
            // 
            this.skinBarSubItem5.Caption = "skinBarSubItem5";
            this.skinBarSubItem5.Id = 13;
            this.skinBarSubItem5.Name = "skinBarSubItem5";
            // 
            // skinBarSubItem6
            // 
            this.skinBarSubItem6.Caption = "skinBarSubItem6";
            this.skinBarSubItem6.Id = 14;
            this.skinBarSubItem6.Name = "skinBarSubItem6";
            // 
            // skinBarSubItem7
            // 
            this.skinBarSubItem7.Caption = "skinBarSubItem7";
            this.skinBarSubItem7.Id = 15;
            this.skinBarSubItem7.Name = "skinBarSubItem7";
            // 
            // skinBarSubItem8
            // 
            this.skinBarSubItem8.Caption = "skinBarSubItem8";
            this.skinBarSubItem8.Id = 16;
            this.skinBarSubItem8.Name = "skinBarSubItem8";
            // 
            // skinBarSubItem9
            // 
            this.skinBarSubItem9.Caption = "skinBarSubItem9";
            this.skinBarSubItem9.Id = 17;
            this.skinBarSubItem9.Name = "skinBarSubItem9";
            // 
            // skinBarSubItem10
            // 
            this.skinBarSubItem10.Caption = "skinBarSubItem10";
            this.skinBarSubItem10.Id = 18;
            this.skinBarSubItem10.Name = "skinBarSubItem10";
            // 
            // skinBarSubItem11
            // 
            this.skinBarSubItem11.Caption = "\r\n";
            this.skinBarSubItem11.Enabled = false;
            this.skinBarSubItem11.Id = 19;
            this.skinBarSubItem11.ItemAppearance.Normal.BackColor = System.Drawing.Color.PaleTurquoise;
            this.skinBarSubItem11.ItemAppearance.Normal.Options.UseBackColor = true;
            this.skinBarSubItem11.Name = "skinBarSubItem11";
            this.skinBarSubItem11.Size = new System.Drawing.Size(664, 0);
            // 
            // skinBarSubItem12
            // 
            this.skinBarSubItem12.Id = 20;
            this.skinBarSubItem12.Name = "skinBarSubItem12";
            // 
            // skinBarSubItem13
            // 
            this.skinBarSubItem13.Caption = "skinBarSubItem13";
            this.skinBarSubItem13.Id = 21;
            this.skinBarSubItem13.Name = "skinBarSubItem13";
            // 
            // skinBarSubItem14
            // 
            this.skinBarSubItem14.Caption = "skinBarSubItem14";
            this.skinBarSubItem14.Id = 22;
            this.skinBarSubItem14.Name = "skinBarSubItem14";
            // 
            // skinBarSubItem15
            // 
            this.skinBarSubItem15.Caption = "skinBarSubItem15";
            this.skinBarSubItem15.Id = 23;
            this.skinBarSubItem15.Name = "skinBarSubItem15";
            // 
            // skinBarSubItem16
            // 
            this.skinBarSubItem16.Caption = "skinBarSubItem16";
            this.skinBarSubItem16.Id = 24;
            this.skinBarSubItem16.Name = "skinBarSubItem16";
            // 
            // skinDropDownButtonItem3
            // 
            this.skinDropDownButtonItem3.Id = 25;
            this.skinDropDownButtonItem3.Name = "skinDropDownButtonItem3";
            // 
            // skinBarSubItem17
            // 
            this.skinBarSubItem17.Id = 26;
            this.skinBarSubItem17.Name = "skinBarSubItem17";
            // 
            // skinBarSubItem18
            // 
            this.skinBarSubItem18.Id = 27;
            this.skinBarSubItem18.Name = "skinBarSubItem18";
            // 
            // skinBarSubItem19
            // 
            this.skinBarSubItem19.Id = 28;
            this.skinBarSubItem19.Name = "skinBarSubItem19";
            // 
            // skinBarSubItem20
            // 
            this.skinBarSubItem20.Id = 29;
            this.skinBarSubItem20.Name = "skinBarSubItem20";
            // 
            // groupBoxThongTinNv
            // 
            this.groupBoxThongTinNv.Controls.Add(this.comboBoxPhai);
            this.groupBoxThongTinNv.Controls.Add(this.textEditCN);
            this.groupBoxThongTinNv.Controls.Add(this.dateeEditNgayCap);
            this.groupBoxThongTinNv.Controls.Add(this.textEditSDT);
            this.groupBoxThongTinNv.Controls.Add(this.textEditHo);
            this.groupBoxThongTinNv.Controls.Add(this.textEditTen);
            this.groupBoxThongTinNv.Controls.Add(this.textEditDiaChi);
            this.groupBoxThongTinNv.Controls.Add(this.textEditCMND);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl8);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl7);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl6);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl5);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl4);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl3);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl2);
            this.groupBoxThongTinNv.Controls.Add(this.labelControl1);
            this.groupBoxThongTinNv.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBoxThongTinNv.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxThongTinNv.Enabled = false;
            this.groupBoxThongTinNv.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxThongTinNv.Location = new System.Drawing.Point(0, 45);
            this.groupBoxThongTinNv.Name = "groupBoxThongTinNv";
            this.groupBoxThongTinNv.Size = new System.Drawing.Size(1802, 161);
            this.groupBoxThongTinNv.TabIndex = 6;
            this.groupBoxThongTinNv.TabStop = false;
            this.groupBoxThongTinNv.Text = "Thông Tin Khách Hàng";
            this.groupBoxThongTinNv.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // comboBoxPhai
            // 
            this.comboBoxPhai.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.khachHangBindingSource, "PHAI", true));
            this.comboBoxPhai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPhai.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPhai.FormattingEnabled = true;
            this.comboBoxPhai.Items.AddRange(new object[] {
            "NỮ",
            "NAM"});
            this.comboBoxPhai.Location = new System.Drawing.Point(124, 105);
            this.comboBoxPhai.Name = "comboBoxPhai";
            this.comboBoxPhai.Size = new System.Drawing.Size(121, 32);
            this.comboBoxPhai.TabIndex = 18;
            // 
            // khachHangBindingSource
            // 
            this.khachHangBindingSource.DataMember = "KhachHang";
            this.khachHangBindingSource.DataSource = this.nGANHANGDataSet;
            // 
            // nGANHANGDataSet
            // 
            this.nGANHANGDataSet.DataSetName = "NGANHANGDataSet";
            this.nGANHANGDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textEditCN
            // 
            this.textEditCN.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.khachHangBindingSource, "MACN", true));
            this.textEditCN.Enabled = false;
            this.textEditCN.Location = new System.Drawing.Point(1492, 108);
            this.textEditCN.MenuManager = this.barManager1;
            this.textEditCN.Name = "textEditCN";
            this.textEditCN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditCN.Properties.Appearance.Options.UseFont = true;
            this.textEditCN.Size = new System.Drawing.Size(270, 32);
            this.textEditCN.TabIndex = 17;
            // 
            // dateeEditNgayCap
            // 
            this.dateeEditNgayCap.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.khachHangBindingSource, "NGAYCAP", true));
            this.dateeEditNgayCap.EditValue = null;
            this.dateeEditNgayCap.Location = new System.Drawing.Point(1014, 104);
            this.dateeEditNgayCap.MenuManager = this.barManager1;
            this.dateeEditNgayCap.Name = "dateeEditNgayCap";
            this.dateeEditNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateeEditNgayCap.Properties.Appearance.Options.UseFont = true;
            this.dateeEditNgayCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateeEditNgayCap.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateeEditNgayCap.Size = new System.Drawing.Size(270, 32);
            this.dateeEditNgayCap.TabIndex = 16;
            // 
            // textEditSDT
            // 
            this.textEditSDT.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.khachHangBindingSource, "SODT", true));
            this.textEditSDT.Location = new System.Drawing.Point(549, 104);
            this.textEditSDT.MenuManager = this.barManager1;
            this.textEditSDT.Name = "textEditSDT";
            this.textEditSDT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditSDT.Properties.Appearance.Options.UseFont = true;
            this.textEditSDT.Size = new System.Drawing.Size(270, 32);
            this.textEditSDT.TabIndex = 13;
            // 
            // textEditHo
            // 
            this.textEditHo.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.khachHangBindingSource, "HO", true));
            this.textEditHo.Location = new System.Drawing.Point(549, 35);
            this.textEditHo.MenuManager = this.barManager1;
            this.textEditHo.Name = "textEditHo";
            this.textEditHo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditHo.Properties.Appearance.Options.UseFont = true;
            this.textEditHo.Size = new System.Drawing.Size(270, 32);
            this.textEditHo.TabIndex = 12;
            // 
            // textEditTen
            // 
            this.textEditTen.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.khachHangBindingSource, "TEN", true));
            this.textEditTen.Location = new System.Drawing.Point(1014, 35);
            this.textEditTen.MenuManager = this.barManager1;
            this.textEditTen.Name = "textEditTen";
            this.textEditTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditTen.Properties.Appearance.Options.UseFont = true;
            this.textEditTen.Size = new System.Drawing.Size(270, 32);
            this.textEditTen.TabIndex = 10;
            // 
            // textEditDiaChi
            // 
            this.textEditDiaChi.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.khachHangBindingSource, "DIACHI", true));
            this.textEditDiaChi.Location = new System.Drawing.Point(1492, 35);
            this.textEditDiaChi.MenuManager = this.barManager1;
            this.textEditDiaChi.Name = "textEditDiaChi";
            this.textEditDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditDiaChi.Properties.Appearance.Options.UseFont = true;
            this.textEditDiaChi.Size = new System.Drawing.Size(270, 32);
            this.textEditDiaChi.TabIndex = 9;
            // 
            // textEditCMND
            // 
            this.textEditCMND.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.khachHangBindingSource, "CMND", true));
            this.textEditCMND.Location = new System.Drawing.Point(124, 35);
            this.textEditCMND.MenuManager = this.barManager1;
            this.textEditCMND.Name = "textEditCMND";
            this.textEditCMND.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditCMND.Properties.Appearance.Options.UseFont = true;
            this.textEditCMND.Size = new System.Drawing.Size(270, 32);
            this.textEditCMND.TabIndex = 8;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(1320, 112);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(153, 24);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Mã Chi Nhánh :";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(47, 108);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(57, 24);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "Phái :";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(1389, 43);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(84, 24);
            this.labelControl6.TabIndex = 5;
            this.labelControl6.Text = "Địa Chỉ :";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(871, 108);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(109, 24);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Ngày Cấp :";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(376, 109);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(151, 24);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Số Điện Thoại :";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(930, 39);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(50, 24);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Tên :";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(482, 43);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 24);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Họ :";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(30, 43);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 24);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "CMND :";
            // 
            // comboBoxChiNhanh
            // 
            this.comboBoxChiNhanh.DisplayMember = "TENSERVER";
            this.comboBoxChiNhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxChiNhanh.Enabled = false;
            this.comboBoxChiNhanh.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxChiNhanh.FormattingEnabled = true;
            this.comboBoxChiNhanh.Location = new System.Drawing.Point(725, 16);
            this.comboBoxChiNhanh.Name = "comboBoxChiNhanh";
            this.comboBoxChiNhanh.Size = new System.Drawing.Size(325, 27);
            this.comboBoxChiNhanh.TabIndex = 15;
            this.comboBoxChiNhanh.ValueMember = "TENSERVER";
            this.comboBoxChiNhanh.SelectedIndexChanged += new System.EventHandler(this.comboBoxChiNhanh_SelectedIndexChanged);
            // 
            // nGANHANGDataSetBindingSource
            // 
            this.nGANHANGDataSetBindingSource.DataSource = this.nGANHANGDataSet;
            this.nGANHANGDataSetBindingSource.Position = 0;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(590, 18);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(119, 25);
            this.labelControl9.TabIndex = 13;
            this.labelControl9.Text = "Chi nhánh :";
            this.labelControl9.Click += new System.EventHandler(this.labelControl9_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.khachHangGridControl);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 354);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1802, 379);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh Sách Khách Hàng";
            // 
            // khachHangGridControl
            // 
            this.khachHangGridControl.CausesValidation = false;
            this.khachHangGridControl.DataSource = this.khachHangBindingSource;
            this.khachHangGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.khachHangGridControl.Location = new System.Drawing.Point(3, 32);
            this.khachHangGridControl.MainView = this.gridView1;
            this.khachHangGridControl.MenuManager = this.barManager1;
            this.khachHangGridControl.Name = "khachHangGridControl";
            this.khachHangGridControl.Size = new System.Drawing.Size(1796, 344);
            this.khachHangGridControl.TabIndex = 0;
            this.khachHangGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCMND,
            this.colHO,
            this.colTEN,
            this.colNGAYCAP,
            this.colDIACHI,
            this.colSODT,
            this.colPHAI,
            this.colMACN});
            this.gridView1.GridControl = this.khachHangGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            // 
            // colCMND
            // 
            this.colCMND.AppearanceCell.Options.UseTextOptions = true;
            this.colCMND.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCMND.FieldName = "CMND";
            this.colCMND.MinWidth = 30;
            this.colCMND.Name = "colCMND";
            this.colCMND.Visible = true;
            this.colCMND.VisibleIndex = 0;
            this.colCMND.Width = 112;
            // 
            // colHO
            // 
            this.colHO.AppearanceCell.Options.UseTextOptions = true;
            this.colHO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHO.FieldName = "HO";
            this.colHO.MinWidth = 30;
            this.colHO.Name = "colHO";
            this.colHO.Visible = true;
            this.colHO.VisibleIndex = 1;
            this.colHO.Width = 112;
            // 
            // colTEN
            // 
            this.colTEN.AppearanceCell.Options.UseTextOptions = true;
            this.colTEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTEN.FieldName = "TEN";
            this.colTEN.MinWidth = 30;
            this.colTEN.Name = "colTEN";
            this.colTEN.Visible = true;
            this.colTEN.VisibleIndex = 2;
            this.colTEN.Width = 112;
            // 
            // colNGAYCAP
            // 
            this.colNGAYCAP.AppearanceCell.Options.UseTextOptions = true;
            this.colNGAYCAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNGAYCAP.FieldName = "NGAYCAP";
            this.colNGAYCAP.MinWidth = 30;
            this.colNGAYCAP.Name = "colNGAYCAP";
            this.colNGAYCAP.Visible = true;
            this.colNGAYCAP.VisibleIndex = 3;
            this.colNGAYCAP.Width = 112;
            // 
            // colDIACHI
            // 
            this.colDIACHI.AppearanceCell.Options.UseTextOptions = true;
            this.colDIACHI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDIACHI.FieldName = "DIACHI";
            this.colDIACHI.MinWidth = 30;
            this.colDIACHI.Name = "colDIACHI";
            this.colDIACHI.Visible = true;
            this.colDIACHI.VisibleIndex = 4;
            this.colDIACHI.Width = 112;
            // 
            // colSODT
            // 
            this.colSODT.AppearanceCell.Options.UseTextOptions = true;
            this.colSODT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSODT.FieldName = "SODT";
            this.colSODT.MinWidth = 30;
            this.colSODT.Name = "colSODT";
            this.colSODT.Visible = true;
            this.colSODT.VisibleIndex = 5;
            this.colSODT.Width = 112;
            // 
            // colPHAI
            // 
            this.colPHAI.AppearanceCell.Options.UseTextOptions = true;
            this.colPHAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPHAI.FieldName = "PHAI";
            this.colPHAI.MinWidth = 30;
            this.colPHAI.Name = "colPHAI";
            this.colPHAI.Visible = true;
            this.colPHAI.VisibleIndex = 6;
            this.colPHAI.Width = 112;
            // 
            // colMACN
            // 
            this.colMACN.AppearanceCell.Options.UseTextOptions = true;
            this.colMACN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMACN.FieldName = "MACN";
            this.colMACN.MinWidth = 30;
            this.colMACN.Name = "colMACN";
            this.colMACN.Visible = true;
            this.colMACN.VisibleIndex = 7;
            this.colMACN.Width = 112;
            // 
            // khachHangTableAdapter
            // 
            this.khachHangTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.ChiNhanhTableAdapter = null;
            this.tableAdapterManager1.GD_CHUYENTIENTableAdapter = null;
            this.tableAdapterManager1.GD_GOIRUTTableAdapter = null;
            this.tableAdapterManager1.KhachHangTableAdapter = this.khachHangTableAdapter;
            this.tableAdapterManager1.NhanVienTableAdapter = null;
            this.tableAdapterManager1.TaiKhoanTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxChiNhanh);
            this.groupBox1.Controls.Add(this.labelControl9);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 206);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1802, 49);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chọn Chi Nhánh";
            // 
            // KhachHang
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1802, 733);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxThongTinNv);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.None;
            this.Name = "KhachHang";
            this.Text = "Khách Hàng";
            this.Load += new System.EventHandler(this.KhachHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBoxThongTinNv.ResumeLayout(false);
            this.groupBoxThongTinNv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.khachHangBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateeEditNgayCap.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateeEditNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.khachHangGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonLamMoi;
        private System.Windows.Forms.GroupBox groupBoxThongTinNv;
        private DevExpress.XtraEditors.TextEdit textEditSDT;
        private DevExpress.XtraEditors.TextEdit textEditHo;
        private DevExpress.XtraEditors.TextEdit textEditTen;
        private DevExpress.XtraEditors.TextEdit textEditDiaChi;
        private DevExpress.XtraEditors.TextEdit textEditCMND;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.ComboBox comboBoxChiNhanh;
        private DevExpress.XtraEditors.DateEdit dateeEditNgayCap;
        private System.Windows.Forms.BindingSource nGANHANGDataSetBindingSource;
        private NGANHANGDataSet nGANHANGDataSet;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEditCN;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemThoatKh;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem1;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem2;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem3;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem1;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem4;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem2;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem5;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem6;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem7;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem8;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem9;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem10;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem11;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem12;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem13;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem14;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem15;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem16;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem3;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem17;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem18;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem19;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem20;
        private System.Windows.Forms.BindingSource khachHangBindingSource;
        private NGANHANGDataSetTableAdapters.KhachHangTableAdapter khachHangTableAdapter;
        private NGANHANGDataSetTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraGrid.GridControl khachHangGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCMND;
        private DevExpress.XtraGrid.Columns.GridColumn colHO;
        private DevExpress.XtraGrid.Columns.GridColumn colTEN;
        private DevExpress.XtraGrid.Columns.GridColumn colNGAYCAP;
        private DevExpress.XtraGrid.Columns.GridColumn colDIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn colSODT;
        private DevExpress.XtraGrid.Columns.GridColumn colPHAI;
        private DevExpress.XtraGrid.Columns.GridColumn colMACN;
        public DevExpress.XtraBars.BarButtonItem barButtonItemThemKH;
        public DevExpress.XtraBars.BarButtonItem barButtonItemDelete;
        public DevExpress.XtraBars.BarButtonItem barButtonItemPhucHoi;
        public DevExpress.XtraBars.BarButtonItem barButtonItemSua;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxPhai;
    }
}