﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace NganHang
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
           
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nGANHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            try
            {
                string chuoiketnoi = "Data Source=DESKTOP-V1KFST6; Initial Catalog=" + Program.database + ";Integrated Security=True";
                //string chuoiketnoi = "Data Source=MIMILE; Initial Catalog=" + Program.database + ";Integrated Security=True";
                Program.conn.ConnectionString = chuoiketnoi;
                DataTable dt = new DataTable();
                dt = Program.ExecSqlDataTable("SELECT * FROM V_DS_PHANMANH");
                Program.bds_dspm.DataSource = dt;
                Program.BindingDataToComBo(cbxDsPhanManh, dt);
            }
            catch (Exception)
            {

            }

            DataTable mcn = new DataTable();
            mcn = Program.ExecSqlDataTable("SELECT * FROM V_MACN");
            Program.view_macn.DataSource = mcn;

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cbxDsPhanManh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Program.servername = cbxDsPhanManh.SelectedValue.ToString();
                Program.tencn = cbxDsPhanManh.Text;
            }
            catch (Exception) { }
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (txtTaiKhoan.Text.Trim() == "" || txtPassword.Text.Trim() == "")
            {
                XtraMessageBox.Show("Tài khoản đăng nhập không được trống", "Lỗi đăng nhập", MessageBoxButtons.OK);
                txtTaiKhoan.Focus();
                return;
            }
            try
            {
                Program.mChinhanh = cbxDsPhanManh.SelectedIndex;
                if (Program.mChinhanh == 2)
                {
                    Program.mlogin = txtTaiKhoan.Text;
                    Program.password = txtPassword.Text;
                    if (Program.KetNoi() == 0)
                    {
                        return;
                    }
                    hienThiServer3();
                    Form1.login.Close();
                }
                else
                {
                    Program.mlogin = txtTaiKhoan.Text;
                    Program.password = txtPassword.Text;
                    if (Program.KetNoi() == 0)
                    {
                        return;
                    }
                    
                    Program.mloginDN = Program.mlogin;
                    Program.passwordDN = Program.password;

                    List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@TENLOGIN", txtTaiKhoan.Text), new SqlParameter("@PASSWORD", txtPassword.Text) };
                    Program.myReader = Program.CallSpDataReader("SP_DANGNHAP", list);
                    String connec = Program.connstr;

                    if (Program.myReader.Read())
                    {
                        Program.username = Program.myReader.GetString(0);
                        if (Convert.IsDBNull(Program.username))
                        {
                            XtraMessageBox.Show("Login bạn nhập không có quyền truy cập dữ liệu\nBạn xem lại username, password", "", MessageBoxButtons.OK);
                            return;
                        }
                        Program.mHoten = Program.myReader.GetString(1);
                        Program.mGroup = Program.myReader.GetString(2);
                        Program.cnnv = Program.servername;
                        turnOnOrOffComponentByRole(true);
                        //server chi hien thi khách hàng

                        Form1.login.Close();
                    }
                    else
                    {
                        return;
                    }
                    Program.myReader.Close();
                    Program.conn.Close();
                }
            }
            catch (Exception ex) { MessageBox.Show("Lỗi : "+ex.Message); }
           
        }
        private void turnOnOrOffComponentByRole(Boolean b)
        {
            if (b)
            {
                Program.nganHang.barButtonDangNhap.Enabled = false;
            }
            else
            {
                Program.nganHang.barButtonDangNhap.Enabled = true;
            }  
            Program.nganHang.barButtonDangXuat.Enabled = b;
            Program.nganHang.barButtonGoiRutTien.Enabled = b;
            Program.nganHang.barButtonChuyenTien.Enabled = b;
            Program.nganHang.barButtonTaiKhoanKH.Enabled = b;
            Program.nganHang.barButtonThongTinKh.Enabled = b;
            Program.nganHang.barButtonTaiKhoanNV.Enabled = b;
            Program.nganHang.barButtonThongTinNv.Enabled = b;
            Program.nganHang.barButtonCapNhatTkNv.Enabled = b;
            Program.nganHang.barButtonSaoKeTaiKhoan.Enabled = b;
            Program.nganHang.barButtonItemThongKeKhachHang.Enabled = b;
            if(Program.mGroup == "NGANHANG")
            {
                Program.nganHang.barButtonItemThongKeGiaoDich.Enabled = true;
            }
            Program.nganHang.barStaticItemMaNV.Caption = Program.username;
            Program.nganHang.barStaticItemTenNV.Caption = Program.mHoten;
            Program.nganHang.barStaticItemNhomNv.Caption = Program.mGroup;

            Program.nganHang.barStaticItemChiNhanh.Caption = Program.tencn;
        }

        public void hienThiServer3()
        {
            Form1.formKh = new KhachHang();
            Form1.formKh.MdiParent = Program.nganHang ;
            Form1.formKh.TopLevel = false;
            //panelControlDangNhap.Controls.Add(formKh);
            Form1.formKh.Dock = DockStyle.Fill;
            Form1.formKh.Show();

            Form1.formKh.barButtonItemThemKH.Enabled = false;
            Form1.formKh.barButtonItemDelete.Enabled = false;
            Form1.formKh.barButtonItemSua.Enabled = false;
            Form1.formKh.barButtonItemPhucHoi.Enabled = false;
            Program.nganHang.barButtonDangNhap.Enabled = false;
            Program.nganHang.barButtonDangXuat.Enabled = true;
        }

    }
}