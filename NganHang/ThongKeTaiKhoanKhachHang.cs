﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace NganHang
{
    public partial class ThongKeTaiKhoanKhachHang : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongKeTaiKhoanKhachHang(String macn, DateTime tuNgay,DateTime denNgay,int luachon,String tenCN)
        {
            InitializeComponent();
            xrLabelMaCn.Text = tenCN;
            xrLabelDenNGay.Text = denNgay.ToString("yyyy-MM-dd");
            xrLabelTuNgay.Text = tuNgay.ToString("yyyy-MM-dd");
            xrLabelNgayThangNam.Text = Program.tencn + "," + DateTime.UtcNow.Date.ToString("dd/MM/yyyy");
            xrLabelHOTenNguoiLap.Text = Program.mHoten;
            this.sqlDataSource1.Connection.ConnectionString = Program.connstr;
            this.sqlDataSource1.Queries[0].Parameters[0].Value = tuNgay;
            this.sqlDataSource1.Queries[0].Parameters[1].Value = denNgay;
            this.sqlDataSource1.Queries[0].Parameters[2].Value = macn;
            this.sqlDataSource1.Queries[0].Parameters[3].Value = luachon;

        }

    }
}
