﻿namespace NganHang
{
    partial class XfrmThongKeTaiKhoanKH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XfrmThongKeTaiKhoanKH));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxChiNhanh = new System.Windows.Forms.ComboBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonInThongKe = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditDenNgay = new DevExpress.XtraEditors.DateEdit();
            this.dateEditTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.checkBox1ChiNhanh = new System.Windows.Forms.CheckBox();
            this.checkBoxTatCaChiNhanh = new System.Windows.Forms.CheckBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupBoxFormThongTIn = new System.Windows.Forms.GroupBox();
            this.simpleButtonTHoat = new DevExpress.XtraEditors.SimpleButton();
            this.groupBoxLuaChon = new System.Windows.Forms.GroupBox();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDenNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTuNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTuNgay.Properties)).BeginInit();
            this.groupBoxFormThongTIn.SuspendLayout();
            this.groupBoxLuaChon.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(33, 26);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(136, 42);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Chi Nhánh :";
            // 
            // comboBoxChiNhanh
            // 
            this.comboBoxChiNhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxChiNhanh.Enabled = false;
            this.comboBoxChiNhanh.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxChiNhanh.FormattingEnabled = true;
            this.comboBoxChiNhanh.Location = new System.Drawing.Point(257, 31);
            this.comboBoxChiNhanh.Name = "comboBoxChiNhanh";
            this.comboBoxChiNhanh.Size = new System.Drawing.Size(317, 37);
            this.comboBoxChiNhanh.TabIndex = 1;
            this.comboBoxChiNhanh.ValueMember = "MACN";
            this.comboBoxChiNhanh.SelectedIndexChanged += new System.EventHandler(this.comboBoxChiNhanh_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(33, 142);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(128, 34);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Từ ngày :";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(33, 250);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(128, 51);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Đến ngày :";
            // 
            // simpleButtonInThongKe
            // 
            this.simpleButtonInThongKe.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.simpleButtonInThongKe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonInThongKe.Appearance.Options.UseBackColor = true;
            this.simpleButtonInThongKe.Appearance.Options.UseFont = true;
            this.simpleButtonInThongKe.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonInThongKe.ImageOptions.Image")));
            this.simpleButtonInThongKe.Location = new System.Drawing.Point(128, 333);
            this.simpleButtonInThongKe.Name = "simpleButtonInThongKe";
            this.simpleButtonInThongKe.Size = new System.Drawing.Size(188, 78);
            this.simpleButtonInThongKe.TabIndex = 6;
            this.simpleButtonInThongKe.Text = "In Báo Cáo";
            this.simpleButtonInThongKe.Click += new System.EventHandler(this.simpleButtonInThongKe_Click);
            // 
            // dateEditDenNgay
            // 
            this.dateEditDenNgay.EditValue = null;
            this.dateEditDenNgay.Location = new System.Drawing.Point(257, 257);
            this.dateEditDenNgay.Name = "dateEditDenNgay";
            this.dateEditDenNgay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditDenNgay.Properties.Appearance.Options.UseFont = true;
            this.dateEditDenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDenNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDenNgay.Size = new System.Drawing.Size(317, 38);
            this.dateEditDenNgay.TabIndex = 3;
            // 
            // dateEditTuNgay
            // 
            this.dateEditTuNgay.EditValue = null;
            this.dateEditTuNgay.Location = new System.Drawing.Point(257, 141);
            this.dateEditTuNgay.Name = "dateEditTuNgay";
            this.dateEditTuNgay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditTuNgay.Properties.Appearance.Options.UseFont = true;
            this.dateEditTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTuNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTuNgay.Size = new System.Drawing.Size(317, 38);
            this.dateEditTuNgay.TabIndex = 2;
            // 
            // checkBox1ChiNhanh
            // 
            this.checkBox1ChiNhanh.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1ChiNhanh.Location = new System.Drawing.Point(227, 27);
            this.checkBox1ChiNhanh.Name = "checkBox1ChiNhanh";
            this.checkBox1ChiNhanh.Size = new System.Drawing.Size(264, 42);
            this.checkBox1ChiNhanh.TabIndex = 7;
            this.checkBox1ChiNhanh.Text = "1 Chi Nhánh";
            this.checkBox1ChiNhanh.UseVisualStyleBackColor = true;
            this.checkBox1ChiNhanh.CheckedChanged += new System.EventHandler(this.checkBox1ChiNhanh_CheckedChanged);
            // 
            // checkBoxTatCaChiNhanh
            // 
            this.checkBoxTatCaChiNhanh.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTatCaChiNhanh.Location = new System.Drawing.Point(497, 27);
            this.checkBoxTatCaChiNhanh.Name = "checkBoxTatCaChiNhanh";
            this.checkBoxTatCaChiNhanh.Size = new System.Drawing.Size(264, 42);
            this.checkBoxTatCaChiNhanh.TabIndex = 8;
            this.checkBoxTatCaChiNhanh.Text = "Tất Cả Chi Nhánh";
            this.checkBoxTatCaChiNhanh.UseVisualStyleBackColor = true;
            this.checkBoxTatCaChiNhanh.CheckedChanged += new System.EventHandler(this.checkBoxTatCaChiNhanh_CheckedChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(40, 27);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(136, 42);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Lựa chọn :";
            // 
            // groupBoxFormThongTIn
            // 
            this.groupBoxFormThongTIn.Controls.Add(this.simpleButtonTHoat);
            this.groupBoxFormThongTIn.Controls.Add(this.labelControl1);
            this.groupBoxFormThongTIn.Controls.Add(this.comboBoxChiNhanh);
            this.groupBoxFormThongTIn.Controls.Add(this.dateEditTuNgay);
            this.groupBoxFormThongTIn.Controls.Add(this.dateEditDenNgay);
            this.groupBoxFormThongTIn.Controls.Add(this.simpleButtonInThongKe);
            this.groupBoxFormThongTIn.Controls.Add(this.labelControl2);
            this.groupBoxFormThongTIn.Controls.Add(this.labelControl3);
            this.groupBoxFormThongTIn.Enabled = false;
            this.groupBoxFormThongTIn.Location = new System.Drawing.Point(542, 206);
            this.groupBoxFormThongTIn.Name = "groupBoxFormThongTIn";
            this.groupBoxFormThongTIn.Size = new System.Drawing.Size(675, 433);
            this.groupBoxFormThongTIn.TabIndex = 10;
            this.groupBoxFormThongTIn.TabStop = false;
            // 
            // simpleButtonTHoat
            // 
            this.simpleButtonTHoat.Appearance.BackColor = System.Drawing.Color.OrangeRed;
            this.simpleButtonTHoat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonTHoat.Appearance.Options.UseBackColor = true;
            this.simpleButtonTHoat.Appearance.Options.UseFont = true;
            this.simpleButtonTHoat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonTHoat.ImageOptions.Image")));
            this.simpleButtonTHoat.Location = new System.Drawing.Point(348, 333);
            this.simpleButtonTHoat.Name = "simpleButtonTHoat";
            this.simpleButtonTHoat.Size = new System.Drawing.Size(188, 78);
            this.simpleButtonTHoat.TabIndex = 7;
            this.simpleButtonTHoat.Text = "Thoát";
            this.simpleButtonTHoat.Click += new System.EventHandler(this.simpleButtonTHoat_Click);
            // 
            // groupBoxLuaChon
            // 
            this.groupBoxLuaChon.Controls.Add(this.labelControl4);
            this.groupBoxLuaChon.Controls.Add(this.checkBox1ChiNhanh);
            this.groupBoxLuaChon.Controls.Add(this.checkBoxTatCaChiNhanh);
            this.groupBoxLuaChon.Location = new System.Drawing.Point(474, 125);
            this.groupBoxLuaChon.Name = "groupBoxLuaChon";
            this.groupBoxLuaChon.Size = new System.Drawing.Size(786, 75);
            this.groupBoxLuaChon.TabIndex = 11;
            this.groupBoxLuaChon.TabStop = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(625, 62);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(531, 42);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Thống Kê Tài Khoản Của Khách Hàng";
            // 
            // XfrmThongKeTaiKhoanKH
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1796, 685);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.groupBoxLuaChon);
            this.Controls.Add(this.groupBoxFormThongTIn);
            this.Name = "XfrmThongKeTaiKhoanKH";
            this.Text = "Thống Kê Tài Khoản Của Khách Hàng";
            this.Load += new System.EventHandler(this.XfrmThongKeTaiKhoanKH_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDenNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTuNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTuNgay.Properties)).EndInit();
            this.groupBoxFormThongTIn.ResumeLayout(false);
            this.groupBoxLuaChon.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.ComboBox comboBoxChiNhanh;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonInThongKe;
        private DevExpress.XtraEditors.DateEdit dateEditDenNgay;
        private DevExpress.XtraEditors.DateEdit dateEditTuNgay;
        private System.Windows.Forms.CheckBox checkBox1ChiNhanh;
        private System.Windows.Forms.CheckBox checkBoxTatCaChiNhanh;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.GroupBox groupBoxFormThongTIn;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTHoat;
        private System.Windows.Forms.GroupBox groupBoxLuaChon;
        private DevExpress.XtraEditors.LabelControl labelControl5;
    }
}