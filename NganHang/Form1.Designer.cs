﻿namespace NganHang
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonDangNhap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonThongTinNv = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonTaiKhoanKH = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonGoiRutTien = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRutTien = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonChuyenTien = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonTaiKhoanNV = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonThongTinKh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSaoKeTaiKhoan = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barLinkContainerItem2 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barHeaderItem1 = new DevExpress.XtraBars.BarHeaderItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.barButtonCapNhatTkNv = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemThongKeKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemThongKeGiaoDich = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemMaNV = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChiNhanh = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemTenNV = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem24 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemNhomNv = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage6 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage5 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonControl1.SearchEditItem,
            this.barButtonItem1,
            this.barButtonDangNhap,
            this.barButtonDangXuat,
            this.barButtonThongTinNv,
            this.barButtonItem5,
            this.barButtonTaiKhoanKH,
            this.barButtonGoiRutTien,
            this.barButtonRutTien,
            this.barButtonChuyenTien,
            this.barButtonTaiKhoanNV,
            this.barButtonThongTinKh,
            this.barButtonSaoKeTaiKhoan,
            this.barButtonItem13,
            this.barSubItem1,
            this.barEditItem1,
            this.barLinkContainerItem1,
            this.barLinkContainerItem2,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barStaticItem1,
            this.barHeaderItem1,
            this.barButtonGroup1,
            this.barButtonCapNhatTkNv,
            this.barButtonItemThongKeKhachHang,
            this.barButtonItemThongKeGiaoDich,
            this.barEditItem2,
            this.barStaticItem2,
            this.barStaticItemMaNV,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barStaticItemChiNhanh,
            this.barStaticItemTenNV,
            this.barStaticItem24,
            this.barStaticItemNhomNv});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Margin = new System.Windows.Forms.Padding(4);
            this.ribbonControl1.MaxItemId = 51;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage6,
            this.ribbonPage3,
            this.ribbonPage4});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemTextEdit1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2019;
            this.ribbonControl1.Size = new System.Drawing.Size(1798, 260);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Click += new System.EventHandler(this.ribbonControl1_Click);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonDangNhap
            // 
            this.barButtonDangNhap.Caption = "Đăng Nhập";
            this.barButtonDangNhap.Id = 2;
            this.barButtonDangNhap.ImageOptions.Image = global::NganHang.Properties.Resources.editrangepermission_16x16;
            this.barButtonDangNhap.ImageOptions.LargeImage = global::NganHang.Properties.Resources.editrangepermission_32x321;
            this.barButtonDangNhap.ItemAppearance.Disabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.barButtonDangNhap.ItemAppearance.Disabled.Options.UseBackColor = true;
            this.barButtonDangNhap.LargeWidth = 70;
            this.barButtonDangNhap.Name = "barButtonDangNhap";
            this.barButtonDangNhap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonDangXuat
            // 
            this.barButtonDangXuat.Caption = "Đăng Xuất";
            this.barButtonDangXuat.Enabled = false;
            this.barButtonDangXuat.Id = 3;
            this.barButtonDangXuat.ImageOptions.Image = global::NganHang.Properties.Resources.outbox_16x16;
            this.barButtonDangXuat.ImageOptions.LargeImage = global::NganHang.Properties.Resources.outbox_32x32;
            this.barButtonDangXuat.LargeWidth = 70;
            this.barButtonDangXuat.Name = "barButtonDangXuat";
            this.barButtonDangXuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonDangXuat_ItemClick);
            // 
            // barButtonThongTinNv
            // 
            this.barButtonThongTinNv.Caption = "Thông Tin";
            this.barButtonThongTinNv.Enabled = false;
            this.barButtonThongTinNv.Id = 4;
            this.barButtonThongTinNv.ImageOptions.Image = global::NganHang.Properties.Resources.bocustomer_16x16;
            this.barButtonThongTinNv.ImageOptions.LargeImage = global::NganHang.Properties.Resources.bocustomer_32x32;
            this.barButtonThongTinNv.LargeWidth = 70;
            this.barButtonThongTinNv.Name = "barButtonThongTinNv";
            this.barButtonThongTinNv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonThongTinNv_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Khách Hàng";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.ImageOptions.Image = global::NganHang.Properties.Resources.customer_16x161;
            this.barButtonItem5.ImageOptions.LargeImage = global::NganHang.Properties.Resources.customer_32x321;
            this.barButtonItem5.LargeWidth = 70;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonTaiKhoanKH
            // 
            this.barButtonTaiKhoanKH.Caption = "Tài Khoản";
            this.barButtonTaiKhoanKH.Enabled = false;
            this.barButtonTaiKhoanKH.Id = 6;
            this.barButtonTaiKhoanKH.ImageOptions.Image = global::NganHang.Properties.Resources.user_add_icon1;
            this.barButtonTaiKhoanKH.ImageOptions.LargeImage = global::NganHang.Properties.Resources.user_add_icon1;
            this.barButtonTaiKhoanKH.LargeWidth = 70;
            this.barButtonTaiKhoanKH.Name = "barButtonTaiKhoanKH";
            this.barButtonTaiKhoanKH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonGoiRutTien
            // 
            this.barButtonGoiRutTien.Caption = "Gởi Rút";
            this.barButtonGoiRutTien.Enabled = false;
            this.barButtonGoiRutTien.Id = 7;
            this.barButtonGoiRutTien.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.barButtonGoiRutTien.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonGoiRutTien.ImageOptions.Image")));
            this.barButtonGoiRutTien.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonGoiRutTien.ImageOptions.LargeImage")));
            this.barButtonGoiRutTien.LargeWidth = 70;
            this.barButtonGoiRutTien.Name = "barButtonGoiRutTien";
            this.barButtonGoiRutTien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonRutTien
            // 
            this.barButtonRutTien.Caption = "Rút Tiền";
            this.barButtonRutTien.Enabled = false;
            this.barButtonRutTien.Id = 8;
            this.barButtonRutTien.ImageOptions.Image = global::NganHang.Properties.Resources.bosale_16x16;
            this.barButtonRutTien.ImageOptions.LargeImage = global::NganHang.Properties.Resources.bosale_32x32;
            this.barButtonRutTien.LargeWidth = 70;
            this.barButtonRutTien.Name = "barButtonRutTien";
            // 
            // barButtonChuyenTien
            // 
            this.barButtonChuyenTien.Caption = "Chuyển Tiền";
            this.barButtonChuyenTien.Enabled = false;
            this.barButtonChuyenTien.Id = 9;
            this.barButtonChuyenTien.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonChuyenTien.ImageOptions.Image")));
            this.barButtonChuyenTien.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonChuyenTien.ImageOptions.LargeImage")));
            this.barButtonChuyenTien.LargeWidth = 70;
            this.barButtonChuyenTien.Name = "barButtonChuyenTien";
            this.barButtonChuyenTien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonChuyenTien_ItemClick);
            // 
            // barButtonTaiKhoanNV
            // 
            this.barButtonTaiKhoanNV.Caption = "Tài Khoản";
            this.barButtonTaiKhoanNV.Enabled = false;
            this.barButtonTaiKhoanNV.Id = 13;
            this.barButtonTaiKhoanNV.ImageOptions.Image = global::NganHang.Properties.Resources.Actions_contact_new_icon1;
            this.barButtonTaiKhoanNV.ImageOptions.LargeImage = global::NganHang.Properties.Resources.Actions_contact_new_icon1;
            this.barButtonTaiKhoanNV.LargeWidth = 70;
            this.barButtonTaiKhoanNV.Name = "barButtonTaiKhoanNV";
            this.barButtonTaiKhoanNV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonTaiKhoanNV_ItemClick);
            // 
            // barButtonThongTinKh
            // 
            this.barButtonThongTinKh.Caption = "Thông tin";
            this.barButtonThongTinKh.Enabled = false;
            this.barButtonThongTinKh.Id = 16;
            this.barButtonThongTinKh.ImageOptions.Image = global::NganHang.Properties.Resources.usergroup_16x16;
            this.barButtonThongTinKh.ImageOptions.LargeImage = global::NganHang.Properties.Resources.usergroup_32x32;
            this.barButtonThongTinKh.LargeWidth = 70;
            this.barButtonThongTinKh.Name = "barButtonThongTinKh";
            this.barButtonThongTinKh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonThongTinKh_ItemClick);
            // 
            // barButtonSaoKeTaiKhoan
            // 
            this.barButtonSaoKeTaiKhoan.Caption = "Tài khoản";
            this.barButtonSaoKeTaiKhoan.Enabled = false;
            this.barButtonSaoKeTaiKhoan.Id = 17;
            this.barButtonSaoKeTaiKhoan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonSaoKeTaiKhoan.ImageOptions.Image")));
            this.barButtonSaoKeTaiKhoan.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonSaoKeTaiKhoan.ImageOptions.LargeImage")));
            this.barButtonSaoKeTaiKhoan.LargeWidth = 70;
            this.barButtonSaoKeTaiKhoan.Name = "barButtonSaoKeTaiKhoan";
            this.barButtonSaoKeTaiKhoan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonSaoKeTaiKhoan_ItemClick);
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "Lịch Sử";
            this.barButtonItem13.Id = 18;
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 19;
            this.barSubItem1.ImageOptions.Image = global::NganHang.Properties.Resources.bank_icon;
            this.barSubItem1.ImageOptions.LargeImage = global::NganHang.Properties.Resources.bank_icon;
            this.barSubItem1.LargeWidth = 70;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Chi Nhánh";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 20;
            this.barEditItem1.ImageOptions.Image = global::NganHang.Properties.Resources.bank_icon;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "barLinkContainerItem1";
            this.barLinkContainerItem1.Id = 22;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // barLinkContainerItem2
            // 
            this.barLinkContainerItem2.Caption = "barLinkContainerItem2";
            this.barLinkContainerItem2.Id = 23;
            this.barLinkContainerItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.barLinkContainerItem2.Name = "barLinkContainerItem2";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 24;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Chi Nhánh";
            this.barStaticItem1.Id = 27;
            this.barStaticItem1.ImageOptions.Image = global::NganHang.Properties.Resources.bank_icon;
            this.barStaticItem1.ImageOptions.LargeImage = global::NganHang.Properties.Resources.bank_icon;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem1_ItemClick);
            // 
            // barHeaderItem1
            // 
            this.barHeaderItem1.Caption = "barHeaderItem1";
            this.barHeaderItem1.Id = 28;
            this.barHeaderItem1.Name = "barHeaderItem1";
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Caption = "barButtonGroup1";
            this.barButtonGroup1.Id = 29;
            this.barButtonGroup1.Name = "barButtonGroup1";
            // 
            // barButtonCapNhatTkNv
            // 
            this.barButtonCapNhatTkNv.Caption = "Cập nhật";
            this.barButtonCapNhatTkNv.Enabled = false;
            this.barButtonCapNhatTkNv.Id = 32;
            this.barButtonCapNhatTkNv.ImageOptions.LargeImage = global::NganHang.Properties.Resources.Edit_Users_icon;
            this.barButtonCapNhatTkNv.LargeWidth = 70;
            this.barButtonCapNhatTkNv.Name = "barButtonCapNhatTkNv";
            // 
            // barButtonItemThongKeKhachHang
            // 
            this.barButtonItemThongKeKhachHang.Caption = "Khách Hàng";
            this.barButtonItemThongKeKhachHang.Enabled = false;
            this.barButtonItemThongKeKhachHang.Id = 33;
            this.barButtonItemThongKeKhachHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemThongKeKhachHang.ImageOptions.Image")));
            this.barButtonItemThongKeKhachHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemThongKeKhachHang.ImageOptions.LargeImage")));
            this.barButtonItemThongKeKhachHang.LargeWidth = 70;
            this.barButtonItemThongKeKhachHang.Name = "barButtonItemThongKeKhachHang";
            this.barButtonItemThongKeKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemThongKeKhachHang_ItemClick);
            // 
            // barButtonItemThongKeGiaoDich
            // 
            this.barButtonItemThongKeGiaoDich.Caption = "Giao Dịch Tài Khoản";
            this.barButtonItemThongKeGiaoDich.Enabled = false;
            this.barButtonItemThongKeGiaoDich.Id = 35;
            this.barButtonItemThongKeGiaoDich.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemThongKeGiaoDich.ImageOptions.Image")));
            this.barButtonItemThongKeGiaoDich.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemThongKeGiaoDich.ImageOptions.LargeImage")));
            this.barButtonItemThongKeGiaoDich.LargeWidth = 70;
            this.barButtonItemThongKeGiaoDich.Name = "barButtonItemThongKeGiaoDich";
            this.barButtonItemThongKeGiaoDich.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemThongKeGiaoDich_ItemClick);
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemTextEdit1;
            this.barEditItem2.Enabled = false;
            this.barEditItem2.Id = 42;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Mã nhân viên :";
            this.barStaticItem2.Id = 43;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem2_ItemClick);
            // 
            // barStaticItemMaNV
            // 
            this.barStaticItemMaNV.Id = 44;
            this.barStaticItemMaNV.Name = "barStaticItemMaNV";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Tên :";
            this.barStaticItem4.Id = 45;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Chi Nhánh :";
            this.barStaticItem5.Id = 46;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barStaticItemChiNhanh
            // 
            this.barStaticItemChiNhanh.Id = 47;
            this.barStaticItemChiNhanh.Name = "barStaticItemChiNhanh";
            // 
            // barStaticItemTenNV
            // 
            this.barStaticItemTenNV.Id = 48;
            this.barStaticItemTenNV.Name = "barStaticItemTenNV";
            // 
            // barStaticItem24
            // 
            this.barStaticItem24.Caption = "Nhóm :";
            this.barStaticItem24.Id = 49;
            this.barStaticItem24.Name = "barStaticItem24";
            // 
            // barStaticItemNhomNv
            // 
            this.barStaticItemNhomNv.Id = 50;
            this.barStaticItemNhomNv.Name = "barStaticItemNhomNv";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage1.Appearance.Options.UseFont = true;
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage1.ImageOptions.Image")));
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Trang chủ";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ImageOptions.ImageUri.Uri = "AlignVerticalCenter";
            this.ribbonPageGroup1.ImageOptions.SvgImage = global::NganHang.Properties.Resources.security_key1;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonDangNhap);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonDangXuat);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Hệ thống";
            // 
            // ribbonPage6
            // 
            this.ribbonPage6.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2});
            this.ribbonPage6.ImageOptions.Image = global::NganHang.Properties.Resources._62975_balance_scale_icon;
            this.ribbonPage6.Name = "ribbonPage6";
            this.ribbonPage6.Text = "Tác Vụ";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonGoiRutTien);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonChuyenTien);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Giao Dịch";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage3.Appearance.Options.UseFont = true;
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3,
            this.ribbonPageGroup11});
            this.ribbonPage3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage3.ImageOptions.Image")));
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Quản lý";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonThongTinNv);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonTaiKhoanNV);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Nhân Viên";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.barButtonThongTinKh);
            this.ribbonPageGroup11.ItemLinks.Add(this.barButtonTaiKhoanKH);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.Text = "Khách Hàng";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6,
            this.ribbonPageGroup4});
            this.ribbonPage4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage4.ImageOptions.Image")));
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "Báo cáo";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonSaoKeTaiKhoan);
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItemThongKeKhachHang);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Thống kê";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItemThongKeGiaoDich);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Sao Kê";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem2);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemMaNV);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem4);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemTenNV);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem5);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemChiNhanh);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem24);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemNhomNv);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 943);
            this.ribbonStatusBar1.Margin = new System.Windows.Forms.Padding(4);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1798, 36);
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ribbonPage2";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "ribbonPageGroup10";
            // 
            // ribbonPage5
            // 
            this.ribbonPage5.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup10});
            this.ribbonPage5.Name = "ribbonPage5";
            this.ribbonPage5.Text = "ribbonPage5";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.xtraTabbedMdiManager1.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // Form1
            // 
            this.AllowMdiBar = true;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1798, 979);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl1);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Form1";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "Ngân Hàng";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarHeaderItem barHeaderItem1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup1;
        public DevExpress.XtraBars.BarButtonItem barButtonDangNhap;
        public DevExpress.XtraBars.BarButtonItem barButtonDangXuat;
        public DevExpress.XtraBars.BarButtonItem barButtonThongTinNv;
        public DevExpress.XtraBars.BarButtonItem barButtonTaiKhoanKH;
        public DevExpress.XtraBars.BarButtonItem barButtonGoiRutTien;
        public DevExpress.XtraBars.BarButtonItem barButtonRutTien;
        public DevExpress.XtraBars.BarButtonItem barButtonChuyenTien;
        public DevExpress.XtraBars.BarButtonItem barButtonTaiKhoanNV;
        public DevExpress.XtraBars.BarButtonItem barButtonThongTinKh;
        public DevExpress.XtraBars.BarButtonItem barButtonSaoKeTaiKhoan;
        public DevExpress.XtraBars.BarButtonItem barButtonCapNhatTkNv;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        public DevExpress.XtraBars.BarButtonItem barButtonItemThongKeGiaoDich;
        public DevExpress.XtraBars.BarButtonItem barButtonItemThongKeKhachHang;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage6;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        public DevExpress.XtraBars.BarStaticItem barStaticItemMaNV;
        public DevExpress.XtraBars.BarStaticItem barStaticItemChiNhanh;
        public DevExpress.XtraBars.BarStaticItem barStaticItemTenNV;
        private DevExpress.XtraBars.BarStaticItem barStaticItem24;
        public DevExpress.XtraBars.BarStaticItem barStaticItemNhomNv;
    }
}

