﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;


namespace NganHang
{
    public partial class XfrmSaoKeTaiKhoan : DevExpress.XtraEditors.XtraForm
    {
        public XfrmSaoKeTaiKhoan()
        {
            InitializeComponent();
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void dateEdit2_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void XfrmSaoKeTaiKhoan_Load(object sender, EventArgs e)
        {
            try
            {
              
                Program.servername = Program.cnnv;
                Program.KetNoi();
                DataTable dt = Program.ExecSqlDataTable("SELECT * FROM V_Lay_DS_TaiKhoan");
                Program.conn.Close();

                comboBoxTaiKhoan.Properties.DataSource = dt;
                comboBoxTaiKhoan.Properties.DisplayMember = "SOTK";
                comboBoxTaiKhoan.Properties.ValueMember = "SOTK";

            }
            catch (Exception)
            {

            }

        }

        private void comboboxChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            try
            {
                SaoKeTaiKhoanKH sktk = new SaoKeTaiKhoanKH(DateTime.Parse(dateEditTuNgay.Text), DateTime.Parse(dateEditDenNgay.Text), comboBoxTaiKhoan.Text);
                ReportPrintTool print = new ReportPrintTool(sktk);
                print.ShowPreviewDialog();
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}