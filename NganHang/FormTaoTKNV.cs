﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace NganHang
{
    public partial class FormTaoTKNV : DevExpress.XtraEditors.XtraForm
    {
        private object txtPassword;

        public FormTaoTKNV()
        {
            InitializeComponent();
        }

        private void FormTaoTKNV_Load(object sender, EventArgs e)
        {
            Program.KetNoi();
            this.comboBoxMNV.Properties.DataSource = Program.ExecSqlDataTable("SELECT * FROM V_LAYMANHANVIEN");
            this.comboBoxMNV.Properties.DisplayMember = "MANV";
            this.comboBoxMNV.Properties.ValueMember = "MANV"; //Field in the datatable which you want to be the 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@USERNAME", comboBoxMNV.Text), new SqlParameter("@LGNAME", txtLoginName.Text), new SqlParameter("@PASS", txtMatKhau.Text), new SqlParameter("@ROLE", Program.mGroup) };
                SqlDataReader rd = Program.CallSpDataReader("SP_TAOLOGIN", list);
                String connec = Program.connstr;
                int check;
                if (rd != null)
                {
                    if (rd.Read())
                    {
                        check = rd.GetInt32(0);
                        if (check == 0)
                        {
                            MessageBox.Show("TẠO THÀNH CÔNG");
                            Program.KetNoi();
                            this.comboBoxMNV.Properties.DataSource = Program.ExecSqlDataTable("SELECT * FROM V_LAYMANHANVIEN");
                            this.comboBoxMNV.Properties.DisplayMember = "MANV";
                            this.comboBoxMNV.Properties.ValueMember = "MANV";
                        }


                    }
                    else
                    {
                        MessageBox.Show("Tạo không thành công!");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
                
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxMNV_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1.taoTk.Close();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}