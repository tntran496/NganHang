﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace NganHang
{
    public partial class FormChuyenTien : DevExpress.XtraEditors.XtraForm
    {
        

        public FormChuyenTien()
        {
            InitializeComponent();
        }

        private void FormChuyenTien_Load(object sender, EventArgs e)
        {
            try {
                Program.KetNoi();
                DataTable dt = Program.ExecSqlDataTable("SELECT * FROM V_Lay_DS_TaiKhoan");
                BindingSource bind = new BindingSource();
                bind.DataSource = dt;
                fillComboTaiKhoan(sOTK_CHUYENTextEdit, bind);
                fillComboTaiKhoan(sOTK_NHANTextEdit, bind);

                // TODO: This line of code loads data into the 'nGANHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
                Program.bds_dspm.Filter = "TENCN <> 'Khách Hàng'";
                Program.BindingDataToComBo(comboBoxChiNhanh, Program.bds_dspm);

                // TODO: This line of code loads data into the 'nGANHANGDataSet.GD_CHUYENTIEN' table. You can move, or remove it, as needed.
                this.gD_CHUYENTIENTableAdapter.Connection.ConnectionString = Program.connstr;
                this.gD_CHUYENTIENTableAdapter.Fill(this.nGANHANGDataSet.GD_CHUYENTIEN);


                if (Program.mGroup == "NGANHANG")
                {
                    comboBoxChiNhanh.Enabled = true;
                }

            } catch (Exception) { }
            
        }

        private void barButtonItemTHEM_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.cnnv != Program.servername)
            {
                MessageBox.Show(" Bạn chỉ được thực thiện giao dịch trên chi nhánh của mình ! ", "", MessageBoxButtons.OK);
                return;
            }
            gD_CHUYENTIENBindingSource.AddNew();
            mANVTextEdit.Text = Program.username;

            gD_CHUYENTIENGridControl.Enabled = false;
            groupBoxThongTinGIaoDich.Enabled = true;

            barButtonItemTHem.Enabled = barButtonItemXoa.Enabled = barButtonItemLamMOi.Enabled =false;
            barButtonItemLuu.Enabled = barButtonItemThoat.Enabled = true;
            comboBoxChiNhanh.Enabled = false;
        }

        private void barButtonItemXOA_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String magd = "";
            if (MessageBox.Show("Bạn có chắc muốn xóa ?", "Xác Nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    magd = ((DataRowView)gD_CHUYENTIENBindingSource[gD_CHUYENTIENBindingSource.Position])["MAGD"].ToString();
                    gD_CHUYENTIENBindingSource.RemoveCurrent();
                    this.gD_CHUYENTIENTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.gD_CHUYENTIENTableAdapter.Update(this.nGANHANGDataSet.GD_CHUYENTIEN);


                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi Xóa Chuyển Tiền " + ex.Message, "", MessageBoxButtons.OK);
                    reload();
                }
                finally
                {
                    reload();
                    gD_CHUYENTIENBindingSource.Position = gD_CHUYENTIENBindingSource.Find("MAGD", magd);
                }
                return;
            }
            else
            {
                return;
            }
        }

        private void barButtonLuu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (sOTK_CHUYENTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Tài khoản chuyển không được trống", "", MessageBoxButtons.OK);
                sOTK_CHUYENTextEdit.Focus();
                return;
            }
            if(sOTK_NHANTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Tài khoản nhận không được trống","",MessageBoxButtons.OK);
                sOTK_NHANTextEdit.Focus();
                return;
            }
            if(sOTIENSpinEdit.Text.Trim() == "")
            {
                MessageBox.Show("Số tiền không được trống", "", MessageBoxButtons.OK);
                sOTIENSpinEdit.Focus();
                return;
            }
            if(mANVTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Mã nhân viên không được trống", "", MessageBoxButtons.OK);
                mANVTextEdit.Focus();
                return;
            }
            if (sOTK_CHUYENTextEdit.Text.Trim() == sOTK_NHANTextEdit.Text.Trim())
            {
                MessageBox.Show("Số tài khoản chuyển và số tài khoản nhận không được trùng nhau", "", MessageBoxButtons.OK);
                sOTK_CHUYENTextEdit.Focus();
                return;
            }
            if (checkSoTkKhachHang(sOTK_CHUYENTextEdit.Text.Trim()) == 0)
            {
                MessageBox.Show("Số tài khoản chuyển không tồn tại");
                sOTK_CHUYENTextEdit.Focus();
                return;
            }
            if (checkSoTkKhachHang(sOTK_NHANTextEdit.Text.Trim()) == 0)
            {
                MessageBox.Show("Số tài khoản nhận không tồn tại");
                sOTK_NHANTextEdit.Focus();
                return;
            }
            if (getSoDuTaiKhoan(sOTK_CHUYENTextEdit.Text.Trim()) < sOTIENSpinEdit.Value)
            {
                MessageBox.Show("Số dư trong tài khoản không đủ");
                sOTIENSpinEdit.Focus();
                return;
            }
            try
            {
                Program.KetNoi();
                if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
                SqlCommand sqlcmd = Program.conn.CreateCommand();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_ChuyenTien_TaiKhoan_VER2";
                sqlcmd.Parameters.AddWithValue("@SOTK_CHUYEN", sOTK_CHUYENTextEdit.Text.Trim());
                sqlcmd.Parameters.AddWithValue("@SOTK_NHAN", sOTK_NHANTextEdit.Text.Trim());
                sqlcmd.Parameters.AddWithValue("@SOTIEN", sOTIENSpinEdit.Value);
                sqlcmd.Parameters.AddWithValue("@MANV", mANVTextEdit.Text.Trim());
                int row = sqlcmd.ExecuteNonQuery();
                if (row != 0)
                {
                    MessageBox.Show("Chuyển tiền thành công ", "", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Chuyển tiền thất bại ", "", MessageBoxButtons.OK);
                }
                Program.conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Lỗi "+ex.Message,"",MessageBoxButtons.OK);
                reload();
            }
            finally
            {
                gD_CHUYENTIENGridControl.Enabled = true;
                groupBoxThongTinGIaoDich.Enabled = false;
                barButtonItemTHem.Enabled = barButtonItemXoa.Enabled = true;
                barButtonItemLuu.Enabled = barButtonItemThoat.Enabled = false;

                gD_CHUYENTIENBindingSource.CancelEdit();
                reload();
                barButtonItemLamMOi.Enabled = true;
                if (Program.mGroup == "NGANHANG")
                {
                    comboBoxChiNhanh.Enabled = true;
                }
            }
        }

        private void barButtonThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try {
                gD_CHUYENTIENBindingSource.CancelEdit();

                gD_CHUYENTIENGridControl.Enabled = true;
                groupBoxThongTinGIaoDich.Enabled = false;
                barButtonItemTHem.Enabled = barButtonItemXoa.Enabled = true;
                barButtonItemLuu.Enabled = barButtonItemThoat.Enabled = false;
                barButtonItemLamMOi.Enabled = true;
                if (Program.mGroup.Trim() == "NGANHANG")
                {
                    this.comboBoxChiNhanh.Enabled = true;
                }
                reload();
            } catch (Exception) { }
        }

        private void comboBoxChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Program.ComboChuyenChiNhanh(comboBoxChiNhanh);
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
                }
                else
                {
                    reload();
                }
            }
            catch (Exception) { }
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }
        private int checkSoTkKhachHang(String sotk)
        {
            int result = 0;
            try {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@SOTK", sotk) };
                Program.myReader = Program.CallSpDataReader("SP_Check_SoTK_KhachHang", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            } catch(Exception ) { }
            return result;
        }
        private Decimal getSoDuTaiKhoan(String sotk)
        {
            Decimal result = 0;
            try {
                Program.KetNoi();
                SqlCommand cmd = new SqlCommand("SP_LAY_SODU_TAIKHOAN", Program.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SOTK", sOTK_CHUYENTextEdit.Text.Trim());
                cmd.Parameters.Add("@SODU", SqlDbType.Money);
                cmd.Parameters["@SODU"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();
                result = Convert.ToDecimal(cmd.Parameters["@SODU"].Value);
                Program.conn.Close();
            } catch (Exception ex) {
                XtraMessageBox.Show(ex.Message);
            }
            return result;
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            reload();
        }
        private void fillComboTaiKhoan(SearchLookUpEdit combo, BindingSource bind)
        {
            try
            {
                // combo.Properties.PopupView.OptionsBehavior.AutoPopulateColumns = false;
                // The data source for the dropdown rows
                combo.Properties.DataSource = bind;

                // The field for the editor's display text.
                combo.Properties.DisplayMember = "SOTK";
                // The field matching the edit value.
                combo.Properties.ValueMember = "SOTK";


            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
        private void reload()
        {
            try
            {
                Program.KetNoi();
                DataTable dt = Program.ExecSqlDataTable("SELECT * FROM V_Lay_DS_TaiKhoan");
                BindingSource bind = new BindingSource();
                bind.DataSource = dt;
                fillComboTaiKhoan(sOTK_CHUYENTextEdit, bind);
                fillComboTaiKhoan(sOTK_NHANTextEdit, bind);

                // TODO: This line of code loads data into the 'nGANHANGDataSet.GD_CHUYENTIEN' table. You can move, or remove it, as needed.
                this.gD_CHUYENTIENTableAdapter.Connection.ConnectionString = Program.connstr;
                this.gD_CHUYENTIENTableAdapter.Fill(this.nGANHANGDataSet.GD_CHUYENTIEN);
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
    }
}