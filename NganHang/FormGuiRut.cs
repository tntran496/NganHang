﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
namespace NganHang
{
    public partial class FormGuiRut : DevExpress.XtraEditors.XtraForm
    {
        
        public FormGuiRut()
        {
            InitializeComponent();
        }

        private void panelNhanVien_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FormGuiRut_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nGANHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            Program.bds_dspm.Filter = "TENCN <> 'Khách Hàng'";
            Program.BindingDataToComBo(comboBoxCN, Program.bds_dspm);

            Program.KetNoi();
            DataTable dt = Program.ExecSqlDataTable("SELECT * FROM V_Lay_DS_TaiKhoan");
            BindingSource bind = new BindingSource();
            bind.DataSource = dt;
            fillComboTaiKhoan(textBoxSTK,bind);

            // TODO: This line of code loads data into the 'nGANHANGDataSet.GD_GOIRUT' table. You can move, or remove it, as needed.
            this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
            this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);
            if (Program.mGroup == "NGANHANG")
            {
                comboBoxCN.Enabled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
 
        private void spinEditTien_EditValueChanged(object sender, EventArgs e)
        {

        }

       
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Program.ComboChuyenChiNhanh(comboBoxCN);
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
                }
                else
                {
                    this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);

                }
            }
            catch (Exception) { }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.cnnv != Program.servername)
            {
                MessageBox.Show(" Bạn chỉ được thực thiện giao dịch trên chi nhánh của mình ! ", "", MessageBoxButtons.OK);
                return;
            }
            gD_GOIRUTBindingSource.AddNew();
            textBoxMNV.Text = Program.username;
            comboBoxLOAIGD.SelectedItem = "RT";
            buttonXoa.Enabled = buttonThem.Enabled = gridControlGoiRut.Enabled = comboBoxCN.Enabled = buttonReload.Enabled = false;
            panelGuiRut.Enabled = buttonThoat.Enabled = buttonGD.Enabled = true;
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String magd = "";
            if (MessageBox.Show("Bạn có chắc muốn xóa ?", "Xác Nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    magd = ((DataRowView)gD_GOIRUTBindingSource[gD_GOIRUTBindingSource.Position])["MAGD"].ToString();//LAY GIA TRI MGD DANG CHON
                    gD_GOIRUTBindingSource.RemoveCurrent();
                    this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.gD_GOIRUTTableAdapter.Update(this.nGANHANGDataSet.GD_GOIRUT);

                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Loi xoa giao dich!" + ex.Message);
                }
                finally
                {
                    this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);
                    gD_GOIRUTBindingSource.Position = gD_GOIRUTBindingSource.Find("MAGD", magd);
                }
                return;
            }
            else
            {
                return;
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (textBoxSTK.Text.Trim() == "")
            {
                MessageBox.Show("Số tài khoản không được trống", "", MessageBoxButtons.OK);
                textBoxSTK.Focus();
                return;
            }

            if (spinEditTien.Text.Trim() == "")
            {
                MessageBox.Show("Số tiền không được trống", "", MessageBoxButtons.OK);
                spinEditTien.Focus();
                return;
            }
            if(spinEditTien.Value < 100000)
            {
                MessageBox.Show("Số tiền giao dịch phải lớn hơn 100000!", "", MessageBoxButtons.OK);
                spinEditTien.Focus();
                return;
            }
            if(getSoDuTaiKhoan(textBoxSTK.Text.Trim()) < spinEditTien.Value && comboBoxLOAIGD.Text == "RT")
            {
                MessageBox.Show("Số dư không đủ", "", MessageBoxButtons.OK);
                spinEditTien.Focus();
                return;
            }
            try
            {

                Program.KetNoi();
                if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
                SqlCommand sqlcmd = Program.conn.CreateCommand();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_GOIRUT";
                sqlcmd.Parameters.AddWithValue("@TAIKHOAN", textBoxSTK.Text.Trim());
                sqlcmd.Parameters.AddWithValue("@SOTIEN", spinEditTien.Value);
                sqlcmd.Parameters.AddWithValue("@LOAIGD", comboBoxLOAIGD.SelectedItem);

                sqlcmd.Parameters.AddWithValue("@MANV", textBoxMNV.Text);
                int row = sqlcmd.ExecuteNonQuery();
                if (row != 0)
                {
                    MessageBox.Show("Giao dịch thành công ", "", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Giao dịch thất bại ", "", MessageBoxButtons.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi " + ex.Message, "", MessageBoxButtons.OK);
            }
            finally
            {
                gridControlGoiRut.Enabled = true;
                panelGuiRut.Enabled = false;
                buttonThem.Enabled = buttonXoa.Enabled = buttonReload.Enabled = true;
                buttonGD.Enabled = buttonThoat.Enabled = false;
                gD_GOIRUTBindingSource.CancelEdit();
                this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
                this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);

                if (Program.mGroup == "NGANHANG")
                {
                    comboBoxCN.Enabled = true;
                }


            }
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gD_GOIRUTBindingSource.CancelEdit();
            buttonGD.Enabled = false;
            buttonThoat.Enabled = false;
            buttonThem.Enabled = buttonXoa.Enabled = buttonReload.Enabled = true;
            panelGuiRut.Enabled = false;
            gridControlGoiRut.Enabled = true;
            if (Program.mGroup.Trim() == "NGANHANG")
            {
                this.comboBoxCN.Enabled = true;
            }
            this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
            this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);
        }

        private Decimal getSoDuTaiKhoan(String sotk)
        {
            Decimal result = 0;
            try
            {
                Program.KetNoi();
                SqlCommand cmd = new SqlCommand("SP_LAY_SODU_TAIKHOAN", Program.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SOTK", textBoxSTK.Text.Trim());
                cmd.Parameters.Add("@SODU", SqlDbType.Money);
                cmd.Parameters["@SODU"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();
                result = Convert.ToDecimal(cmd.Parameters["@SODU"].Value);
                Program.conn.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            return result;
        }
       
        private void gridControlGoiRut_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem1_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                Program.KetNoi();
                DataTable dt = Program.ExecSqlDataTable("SELECT * FROM V_Lay_DS_TaiKhoan");
                BindingSource bind = new BindingSource();
                bind.DataSource = dt;
                fillComboTaiKhoan(textBoxSTK, bind);
                // TODO: This line of code loads data into the 'nGANHANGDataSet.GD_CHUYENTIEN' table. You can move, or remove it, as needed.
                this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
                this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);

            }
            catch (Exception) { }
        }
        private void fillComboTaiKhoan(SearchLookUpEdit combo, BindingSource bind)
        {
            try
            {
                // combo.Properties.PopupView.OptionsBehavior.AutoPopulateColumns = false;
                // The data source for the dropdown rows
                combo.Properties.DataSource = bind;

                // The field for the editor's display text.
                combo.Properties.DisplayMember = "SOTK";
                // The field matching the edit value.
                combo.Properties.ValueMember = "SOTK";


            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
    }
}


    