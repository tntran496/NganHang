﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NganHang
{
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public static LoginForm login;
        public static FormTaoTKNV taoTk;
        public static KhachHang formKh; 
        public static RegisterNvForm nhanVien;
        public static FormTaoTkKhachHang taoTkKh;
        public static FormGuiRut guiRut;
        public static FormChuyenTien formChuyenTien;
        public static XfrmThongKeTaiKhoanKH xformThongKeTaiKhoan;
        public static XfrmThongKeKhachHang XfrmThongKeKhachHang;

        public Form1()
        {
            InitializeComponent();
        }
        private Form CheckExists(Type ftype)
        {
            foreach (Form f in this.MdiChildren)
                if (f.GetType() == ftype)
                    return f;
            return null;
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //login= new LoginForm();
            //login.TopLevel = false;
            //panelControlDangNhap.Controls.Add(login);

            //login.Dock = DockStyle.Fill;
            //login.Show();

            Form frm = this.CheckExists(typeof(LoginForm));
            if (frm != null) frm.Activate();
            else
            {
                login = new LoginForm();
                login.MdiParent = this;
                login.TopLevel = false;
                login.Dock = DockStyle.Fill;
                login.Show();
            }
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(FormGuiRut));
            if (frm != null) frm.Activate();
            else
            {
                guiRut = new FormGuiRut();
                guiRut.MdiParent = this;
                guiRut.TopLevel = false;
                //panelControlDangNhap.Controls.Add(taoTk);
                guiRut.Dock = DockStyle.Fill;
                guiRut.Show();
            }
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(FormTaoTkKhachHang));
            if (frm != null) frm.Activate();
            else
            {
                taoTkKh = new FormTaoTkKhachHang();
                taoTkKh.MdiParent = this;
                taoTkKh.TopLevel = false;
                //panelControlDangNhap.Controls.Add(taoTk);
                taoTkKh.Dock = DockStyle.Fill;
                taoTkKh.Show();
            }
        }

        private void barStaticItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void panelControlDangNhap_Paint(object sender, PaintEventArgs e)
        {

        }

        private void barButtonTaiKhoanNV_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //taoTk = new FormTaoTKNV();
            //taoTk.TopLevel = false;
            //panelControlDangNhap.Controls.Add(taoTk);

            //taoTk.Dock = DockStyle.Fill;
            //taoTk.Show();

            Form frm = this.CheckExists(typeof(FormTaoTKNV));
            if (frm != null) frm.Activate();
            else
            {
                taoTk = new FormTaoTKNV();
                taoTk.MdiParent = this;
                taoTk.TopLevel = false;
                //panelControlDangNhap.Controls.Add(taoTk);
                taoTk.Dock = DockStyle.Fill;
                taoTk.Show();
            }

        }

        private void barButtonThongTinKh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(KhachHang));
            if (frm != null) frm.Activate();
            else
            {
                formKh = new KhachHang();
                formKh.MdiParent = this;
                formKh.TopLevel = false;
                //panelControlDangNhap.Controls.Add(formKh);
                formKh.Dock = DockStyle.Fill;
                formKh.Show();
            }
        }

        private void barButtonThongTinNv_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(RegisterNvForm));
            if (frm != null) frm.Activate();
            else
            {
                nhanVien = new RegisterNvForm();
                nhanVien.MdiParent = this;
                nhanVien.TopLevel = false;
                //panelControlDangNhap.Controls.Add(formKh);
                nhanVien.Dock = DockStyle.Fill;
                nhanVien.Show();
            }
        }

        private void barButtonChuyenTien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(FormChuyenTien));
            if (frm != null) frm.Activate();
            else
            {
                formChuyenTien = new FormChuyenTien();
                formChuyenTien.MdiParent = this;
                formChuyenTien.TopLevel = false;
                //panelControlDangNhap.Controls.Add(formKh);
                formChuyenTien.Dock = DockStyle.Fill;
                formChuyenTien.Show();
            }
        }

        private void barButtonDangXuat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.conn != null && Program.conn.State == ConnectionState.Open) Program.conn.Close();

            Program.connstr = "";
            Program.servername = "";
            Program.username = "";
            Program.mlogin = "";
            Program.password = "";
            Program.mGroup = "";
            Program.cnnv = "";
            Program.mloginDN = "";
            Program.passwordDN = "";
            Program.mChinhanh = 0;
            Program.bds_dspm = new BindingSource();
            Program.nganHang.barButtonDangXuat.Enabled = false;
            Program.nganHang.barButtonGoiRutTien.Enabled = false;
            Program.nganHang.barButtonRutTien.Enabled = false;
            Program.nganHang.barButtonChuyenTien.Enabled = false;
            Program.nganHang.barButtonSaoKeTaiKhoan.Enabled = false;
            Program.nganHang.barButtonTaiKhoanKH.Enabled = false;
            Program.nganHang.barButtonThongTinKh.Enabled = false;
            Program.nganHang.barButtonTaiKhoanNV.Enabled = false;
            Program.nganHang.barButtonThongTinNv.Enabled = false;
            Program.nganHang.barButtonCapNhatTkNv.Enabled = false;
            Program.nganHang.barButtonSaoKeTaiKhoan.Enabled = false;
            Program.nganHang.barButtonItemThongKeGiaoDich.Enabled = false;
            Program.nganHang.barButtonItemThongKeKhachHang.Enabled = false;
            Program.nganHang.barButtonDangNhap.Enabled = true;

            Program.nganHang.barStaticItemMaNV.Caption = "";
            Program.nganHang.barStaticItemTenNV.Caption = "";
            Program.nganHang.barStaticItemChiNhanh.Caption = "";
            Program.nganHang.barStaticItemNhomNv.Caption = "";

            foreach (Form c in this.MdiChildren)
            {
                c.Close();
            }

        }

        private void barButtonSaoKeTaiKhoan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(XfrmThongKeTaiKhoanKH));
            if (frm != null) frm.Activate();
            else
            {
                xformThongKeTaiKhoan = new XfrmThongKeTaiKhoanKH();
                xformThongKeTaiKhoan.MdiParent = this;
                xformThongKeTaiKhoan.TopLevel = false;
                //panelControlDangNhap.Controls.Add(formKh);
                xformThongKeTaiKhoan.Dock = DockStyle.Fill;
                xformThongKeTaiKhoan.Show();
            }
        }

        private void barButtonItemThongKeKhachHang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(XfrmThongKeKhachHang));
            if (frm != null) frm.Activate();
            else
            {
                XfrmThongKeKhachHang = new XfrmThongKeKhachHang();
                XfrmThongKeKhachHang.MdiParent = this;
                XfrmThongKeKhachHang.TopLevel = false;
                //panelControlDangNhap.Controls.Add(formKh);
                XfrmThongKeKhachHang.Dock = DockStyle.Fill;
                XfrmThongKeKhachHang.Show();
            }
        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItemThongKeGiaoDich_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(XfrmSaoKeTaiKhoan));
            if (frm != null) frm.Activate();
            else
            {
                XfrmSaoKeTaiKhoan saoke = new XfrmSaoKeTaiKhoan();
                saoke.MdiParent = this;
                saoke.TopLevel = false;
                //panelControlDangNhap.Controls.Add(formKh);
                saoke.Dock = DockStyle.Fill;
                saoke.Show();
            }
        }

        private void barStaticItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
