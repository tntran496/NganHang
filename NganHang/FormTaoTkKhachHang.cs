﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace NganHang
{
    public partial class FormTaoTkKhachHang : DevExpress.XtraEditors.XtraForm
    {
        Stack<SqlCommand> sqlsCache;

 
        String cn = "";
        String sukien = "";
       

        public FormTaoTkKhachHang()
        {
            InitializeComponent();
        }
        private void FormTaoTkKhachHang_Load(object sender, EventArgs e)
        {
            try
            {
                // TODO: This line of code loads data into the 'nGANHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
                Program.bds_dspm.Filter = "TENCN <> 'Khách Hàng'";
                Program.BindingDataToComBo(comboBoxChiNhanh, Program.bds_dspm);

                fillComboKhachHang(cMNDComboBox);

                // TODO: This line of code loads data into the 'nGANHANGDataSet.TaiKhoan' table. You can move, or remove it, as needed.
                this.taiKhoanTableAdapter.Connection.ConnectionString = Program.connstr;
                this.taiKhoanTableAdapter.Fill(this.nGANHANGDataSet.TaiKhoan);

                if (Program.mGroup.Trim() == "NGANHANG")
                {
                    this.comboBoxChiNhanh.Enabled = true;
                }
                cn = Program.getMaChiNhanhHienTai();

                sqlsCache = new Stack<SqlCommand>();
            }
            catch(Exception) { }
        }

        private void taiKhoanGridControl_Click(object sender, EventArgs e)
        {

        }
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sukien = "THEM";
            taiKhoanBindingSource.AddNew();
            mACNTextEdit.Text = cn;

            // random
            

            taiKhoanGridControl.Enabled = false;
            groupBoxTaiKHoanKH.Enabled = true;
            barButtonItemXoa.Enabled = barButtonItemSua.Enabled = barButtonItemPhucHoi.Enabled = barButtonItemThem.Enabled = barButtonItemLamMOi.Enabled = false;
            barButtonItemLuu.Enabled = barButtonItemThoat.Enabled = true;
            this.comboBoxChiNhanh.Enabled = false;
        }

        private void taiKhoanGridControl_Click_1(object sender, EventArgs e)
        {

        }

        private void barButtonItemSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sukien = "SUA";
            sOTKTextEdit.Enabled = false;
            taiKhoanGridControl.Enabled = false;
            groupBoxTaiKHoanKH.Enabled = true;
            barButtonItemXoa.Enabled = barButtonItemThem.Enabled = barButtonItemPhucHoi.Enabled = barButtonItemSua.Enabled = false;
            barButtonItemLuu.Enabled = barButtonItemThoat.Enabled = true;
            this.comboBoxChiNhanh.Enabled = false;
            barButtonItemLamMOi.Enabled = false;
        }

        private void barButtonItemXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String sotk = "";
            String cmnd = "";
            String sodu = "";
            String ngaymo = "";
            
            if (MessageBox.Show("Bạn có chắc muốn xóa ?", "Xác Nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    sotk = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["SOTK"].ToString();
                    if (checkTonTaiGoiRutTaiKH(sotk)>0)
                    {
                        XtraMessageBox.Show("Tài khoản này đã tồn tại giao dịch, không được xóa ","",MessageBoxButtons.OK);
                        return;
                    }
                    if (checkTonTaiChuyenTienTaiKH(sotk) > 0)
                    {
                        XtraMessageBox.Show("Tài khoản này đã tồn tại giao dịch, không được xóa ", "", MessageBoxButtons.OK);
                        return;
                    }
                    if (checkTonTaiNhanTienTaiKH(sotk) > 0)
                    {
                        XtraMessageBox.Show("Tài khoản này đã tồn tại giao dịch, không được xóa ", "", MessageBoxButtons.OK);
                        return;
                    }
                    //cache
                    cmnd = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["CMND"].ToString();
                    sodu = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["SODU"].ToString();
                    ngaymo = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["NGAYMOTK"].ToString();


                    taiKhoanBindingSource.RemoveCurrent();
                    this.taiKhoanTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.taiKhoanTableAdapter.Update(this.nGANHANGDataSet.TaiKhoan);

                    //cache
                    SqlCommand sql = new SqlCommand("INSERT INTO TaiKhoan(SOTK,CMND,SODU,MACN,NGAYMOTK) VALUES (@SOTK,@CMND,@SODU,@MACN,@NGAYMOTK)");
                    sql.Parameters.AddWithValue("@SOTK", sotk);
                    sql.Parameters.AddWithValue("@CMND", cmnd);
                    sql.Parameters.AddWithValue("@SODU", Decimal.Parse(sodu));
                    sql.Parameters.AddWithValue("@MACN", cn);
                    sql.Parameters.AddWithValue("@NGAYMOTK", DateTime.Parse(ngaymo));
                    sqlsCache.Push(sql);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi Xóa Tài Khoản " + ex.Message, "", MessageBoxButtons.OK);
                    reload();
                    return;
                }
                finally
                {
                    // random
                   
                    taiKhoanBindingSource.Position = taiKhoanBindingSource.Find("SOTK", sotk);
                }
                return;
            }
            else
            {
                return;
            }
        }

        private void barButtonItemPhucHoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (sqlsCache.Count > 0)
            {
                if (MessageBox.Show("Bạn có chắc muốn phục hồi ?", "Xác Nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    SqlCommand sql = sqlsCache.Pop();
                    try
                    {
                        Program.KetNoi();
                        sql.Connection = Program.conn;
                        sql.ExecuteNonQuery();
                        
                        MessageBox.Show("Phục Hồi Thành Công !", "", MessageBoxButtons.OK);
                        Program.conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi phục Hồi !" + ex.Message);
                        reload();
                    }
                    finally
                    {
                        reload();
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void barButtonItemLuu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            String sotk = "";
            String newsotk = "";
            String cmnd = "";
            String sodu = "";
            String ngaymo = "";

            if (sOTKTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Số tài khoản Không Được Trống");
                sOTKTextEdit.Focus();
                return;
            }
            if (cMNDComboBox.Text.Trim() == "")
            {
                MessageBox.Show("CMND Không Được Trống");
                cMNDComboBox.Focus();
                return;
            }
            if (sODUSpinEdit.Text.Trim() == "")
            {
                MessageBox.Show("số dư Không Được Trống");
                sODUSpinEdit.Focus();
                return;
            }
            if (nGAYMOTKDateEdit.Text.Trim() == "")
            {
                MessageBox.Show("Ngày mở Không Được Trống");
                nGAYMOTKDateEdit.Focus();
                return;
            }
            if (mACNTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Mã chi Nhánh Không Được Trống");
                nGAYMOTKDateEdit.Focus();
                return;
            }
            if (sukien == "THEM")
            {
                if (checkSoTkKhachHang(sOTKTextEdit.Text.Trim()) == 1)
                {
                    MessageBox.Show("Số tài khoản của khách hàng này đã tồn tại");
                    sOTKTextEdit.Focus();
                    //sOTKTextEdit.Text = laySoTaiKhoanNgauNhien();
                    return;
                }
            }
            if (checkCmndKhachHang(cMNDComboBox.Text.Trim()) == 0)
            {
                MessageBox.Show("CMND của khách hàng này không tồn tại");
                cMNDComboBox.Focus();
                return;
            }
            try
            {
                if(sukien == "SUA")
                {
                    sotk = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["SOTK"].ToString();
                    cmnd = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["CMND"].ToString();
                    sodu = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["SODU"].ToString();
                    ngaymo = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["NGAYMOTK"].ToString();
                }


                taiKhoanBindingSource.EndEdit();
                taiKhoanBindingSource.ResetCurrentItem();
                this.taiKhoanTableAdapter.Connection.ConnectionString = Program.connstr;
                this.taiKhoanTableAdapter.Update(this.nGANHANGDataSet.TaiKhoan);

                newsotk = ((DataRowView)taiKhoanBindingSource[taiKhoanBindingSource.Position])["SOTK"].ToString();


                //cache
                if (sukien == "SUA")
                {
                    SqlCommand sql = new SqlCommand("UPDATE TaiKhoan SET CMND = @CMND,SODU= @SODU,MACN = @MACN,NGAYMOTK = @NGAYMOTK  WHERE SOTK = @SOTK");
                    sql.Parameters.AddWithValue("@CMND", cmnd);
                    sql.Parameters.AddWithValue("@SODU", Decimal.Parse(sodu));
                    sql.Parameters.AddWithValue("@MACN", cn);
                    sql.Parameters.AddWithValue("@NGAYMOTK", DateTime.Parse(ngaymo));
                    sql.Parameters.AddWithValue("@SOTK", sotk);
                    sqlsCache.Push(sql);
                }
                if (sukien == "THEM")
                {
                    String str = "if(not exists(SELECT TOP 1 TK.SOTK FROM NGANHANG.dbo.GD_GOIRUT AS TK WHERE TK.SOTK = @SOTK) and " +
                                            "not exists(SELECT TOP 1 TK.SOTK FROM LINK.NGANHANG.dbo.GD_GOIRUT AS TK WHERE TK.SOTK = @SOTK) and " +
                                            "not exists(SELECT TOP 1 TK.SOTK_CHUYEN FROM NGANHANG.dbo.GD_CHUYENTIEN AS TK WHERE TK.SOTK_CHUYEN = @SOTK) and " +
                                            "not exists(SELECT TOP 1 TK.SOTK_CHUYEN FROM LINK.NGANHANG.dbo.GD_CHUYENTIEN AS TK WHERE TK.SOTK_CHUYEN = @SOTK) and "+
                                            "not exists(SELECT TOP 1 TK.SOTK_NHAN FROM NGANHANG.dbo.GD_CHUYENTIEN AS TK WHERE TK.SOTK_NHAN = @SOTK) and "+
                                            "not exists(SELECT TOP 1 TK.SOTK_NHAN FROM LINK.NGANHANG.dbo.GD_CHUYENTIEN AS TK WHERE TK.SOTK_NHAN = @SOTK) ) " +
                                        "BEGIN DELETE FROM TaiKhoan WHERE SOTK = @SOTK END "+
                                        "ELSE BEGIN RAISERROR('TÀI KHOẢN TỒN TẠI GIAO DỊCH KHÔNG ĐƯỢC XÓA', 16, 1) END";
                    //SqlCommand sql = new SqlCommand("DELETE FROM TaiKhoan WHERE SOTK = @SOTK ; UPDATE NGANHANG.dbo.NGAUNHIENSTK SET TRANGTHAI = 0 WHERE SOTK = @SOTK");
                    SqlCommand sql = new SqlCommand(str);
                    sql.Parameters.AddWithValue("@SOTK", newsotk);
                    sqlsCache.Push(sql);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi ghi Tài Khoản " + ex.Message, "", MessageBoxButtons.OK);
                reload();
            }
            finally
            {
                taiKhoanGridControl.Enabled = true;
                groupBoxTaiKHoanKH.Enabled = false;
                barButtonItemXoa.Enabled = barButtonItemSua.Enabled = barButtonItemThem.Enabled = barButtonItemPhucHoi.Enabled = true;
                barButtonItemLuu.Enabled = barButtonItemThoat.Enabled = false;
                barButtonItemLamMOi.Enabled = true;
                if (Program.mGroup.Trim() == "NGANHANG")
                {
                    this.comboBoxChiNhanh.Enabled = true;
                }
                sOTKTextEdit.Enabled = true;
            }
            return;

        }

        private void barButtonItemThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //random
            //if(sukien == "THEM")
            //{
            //    rollBackSoTaiKhoanNgauNhien(sOTKTextEdit.Text);
            //}

            taiKhoanBindingSource.CancelEdit();

            taiKhoanGridControl.Enabled = true;
            groupBoxTaiKHoanKH.Enabled = false;
            barButtonItemXoa.Enabled = barButtonItemSua.Enabled = barButtonItemThem.Enabled = barButtonItemPhucHoi.Enabled = true;
            barButtonItemLuu.Enabled = barButtonItemThoat.Enabled = false;
            barButtonItemLamMOi.Enabled = true;
            sOTKTextEdit.Enabled = true;
            if (Program.mGroup.Trim() == "NGANHANG")
            {
                this.comboBoxChiNhanh.Enabled = true;
            }
           
            reload();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
                try
                {
                    Program.ComboChuyenChiNhanh(comboBoxChiNhanh);
                    if (Program.KetNoi() == 0)
                    {
                        XtraMessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
                    }
                    else
                    {
                        reload();
                        cn = Program.getMaChiNhanhHienTai();
                        sqlsCache = new Stack<SqlCommand>();
                    }
                   
            }
                catch (Exception) { }
        }
        private int checkCmndKhachHang(String cmnd)
        {
            int result = 0;
            try {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@CMND", cmnd) };
                Program.myReader = Program.CallSpDataReader("SP_CHECK_CMND_KH", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch (Exception) { }
            return result;
        }
        private int checkSoTkKhachHang(String sotk)
        {
            int result = 0;
            try
            {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@SOTK", sotk) };
                Program.myReader = Program.CallSpDataReader("SP_Check_SoTK_KhachHang", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch (Exception) { }
            return result;
        }
        private Int32 checkTonTaiGoiRutTaiKH(String sotk)
        {
            Int32 result = 0;
            try
            {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@SOTK", sotk) };
                Program.myReader = Program.CallSpDataReader("SP_Check_TonTai_GoiRut_TaiKhoan", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            return result;
        }
        private Int32 checkTonTaiChuyenTienTaiKH(String sotk)
        {
            Int32 result = 0;
            try
            {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@SOTK", sotk) };
                Program.myReader = Program.CallSpDataReader("SP_Check_TonTai_ChuyenTien_TaiKhoan", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch (Exception ex) {
                XtraMessageBox.Show(ex.Message);
            }
            return result;
        }
        private Int32 checkTonTaiNhanTienTaiKH(String sotk)
        {
            Int32 result = 0;
            try
            {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@SOTK", sotk) };
                Program.myReader = Program.CallSpDataReader("SP_Check_TonTai_NhanTien", list);
                if (Program.myReader.Read())
                {
                    result =Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            return result;
        }

        private void barButtonItemLamMOi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            reload();
        }
        private void fillComboKhachHang(SearchLookUpEdit combo)
        {
            try
            {
                
                Program.KetNoi();
                DataTable dt = Program.ExecSqlDataTable("SELECT KH.CMND,HOTEN = KH.HO+KH.TEN,KH.SODT FROM LINK3.NGANHANG.dbo.KhachHang AS KH");
                //DataViewManager dvm = new DataViewManager(dt.DataSet);
                //DataView dv = dvm.CreateDataView(dt.DataSet.Tables["KH"]);
                BindingSource bind = new BindingSource();
                bind.DataSource = dt;
                
                // combo.Properties.PopupView.OptionsBehavior.AutoPopulateColumns = false;
                // The data source for the dropdown rows
                combo.Properties.DataSource = bind;
                
                // The field for the editor's display text.
                combo.Properties.DisplayMember = "CMND";
                // The field matching the edit value.
                combo.Properties.ValueMember = "CMND";
                

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
        private String laySoTaiKhoanNgauNhien()
        {
            String sotk = "";
            try
            {
                Program.KetNoi();
                SqlCommand cmd = new SqlCommand("SP_LAY_SOTK_NGAUNHIEN", Program.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SOTK", SqlDbType.NChar,9);
                cmd.Parameters["@SOTK"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();
                sotk = Convert.ToString(cmd.Parameters["@SOTK"].Value);
                Program.conn.Close();
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            return sotk;
        }
        private void rollBackSoTaiKhoanNgauNhien(String sotk)
        {
            try
            {
                Program.KetNoi();
                Program.ExecSqlNonQuery("UPDATE NGANHANG.dbo.NGAUNHIENSTK SET TRANGTHAI = 0 WHERE SOTK = "+sotk);
                Program.conn.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
        private void reload()
        {
            try
            {
                fillComboKhachHang(cMNDComboBox);

                // TODO: This line of code loads data into the 'nGANHANGDataSet.TaiKhoan' table. You can move, or remove it, as needed.
                this.taiKhoanTableAdapter.Connection.ConnectionString = Program.connstr;
                this.taiKhoanTableAdapter.Fill(this.nGANHANGDataSet.TaiKhoan);
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
    }
   
}