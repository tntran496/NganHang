﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using System.Data;
using System.Data.SqlClient;
namespace NganHang
{
    static class Program
    {
        public static Form1 nganHang = new Form1();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static SqlConnection conn = new SqlConnection();
        public static String connstr;
        public static SqlDataReader myReader;
        public static String servername = "";
        public static String username = "";
        public static String mlogin = "";
        public static String password = "";
       
        public static String database = "NGANHANG";
        public static String remotelogin = "HTKN";
        public static String remotepassword = "123456";
        public static String mloginDN = "";
        public static String passwordDN = "";
        public static String mGroup = "";
        public static String mHoten = "";
        public static String cnnv = "";
        public static String tencn = "";
        public static int mChinhanh = 0;

        public static BindingSource  bds_dspm = new BindingSource ();  // giữ bdsPM khi đăng nhập
        public static BindingSource view_macn = new BindingSource();
        public static Form1 frmChinh;
       
        public static int KetNoi()
        {
            if (Program.conn != null && Program.conn.State == ConnectionState.Open)
                Program.conn.Close();
            try
            {
                Program.connstr = "Data Source=" + Program.servername + ";Initial Catalog=" + 
                      Program.database + ";User ID=" +
                      Program.mlogin + ";password=" + Program.password;
                Program.conn.ConnectionString = Program.connstr;
                Program.conn.Open();
                return 1;
            }

            catch (Exception e)
            {
                MessageBox.Show("Lỗi kết nối cơ sở dữ liệu.\nBạn xem lại user name và password.\n " + e.Message, "", MessageBoxButtons.OK);
                return 0;
            }
        }
        public static SqlDataReader ExecSqlDataReader(String strLenh)
        {
            SqlDataReader myreader;
            SqlCommand sqlcmd = new SqlCommand(strLenh, Program.conn);
            sqlcmd.CommandType = CommandType.Text ;
            if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
            try
            {
                myreader = sqlcmd.ExecuteReader(); return myreader;
                
            }
            catch (SqlException ex)
            {
                Program.conn.Close();
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public static DataTable ExecSqlDataTable(String cmd)
        {   
            DataTable dt = new DataTable();
            if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd, conn);
                da.Fill(dt);
                
                conn.Close();
            }catch(Exception ex){
                MessageBox.Show(ex.Message);
            }
            return dt;
        }
     
        public static int ExecSqlNonQuery(String strlenh)
        {
            SqlCommand Sqlcmd = new SqlCommand(strlenh, conn);
            Sqlcmd.CommandType = CommandType.Text;
            Sqlcmd.CommandTimeout = 600;// 10 phut 
            if (conn.State == ConnectionState.Closed) conn.Open();
            try
            {
                Sqlcmd.ExecuteNonQuery(); conn.Close();
                return 0;
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Error converting data type varchar to int"))
                    MessageBox.Show("Bạn format Cell lại cột \"Ngày Thi\" qua kiểu Number hoặc mở File Excel.");
                else MessageBox.Show(ex.Message);
                conn.Close();
                return ex.State; // trang thai lỗi gởi từ RAISERROR trong SQL Server qua
            }
        }
        public static SqlDataReader CallSpDataReader(String spName,List<SqlParameter> sqlParams)
        {
            SqlDataReader myreader = null;
            if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
            SqlCommand sqlcmd = Program.conn.CreateCommand();
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.CommandText = spName;
            try
            {
                if (sqlParams != null)
                {
                    foreach (SqlParameter param in sqlParams)
                    {
                        sqlcmd.Parameters.Add(param);
                    }
                }
                myreader = sqlcmd.ExecuteReader();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);

                Program.conn.Close();
            }
            return myreader;
        }
        public static void ComboChuyenChiNhanh(ComboBox cmb)
        {
            // bắt lỗi khi giá trị của selectedvalue = "sysem.data.datarowview"  ==> lỗi hay gặp của combobox winform
            if (cmb.SelectedValue.ToString() == "System.Data.DataRowView")
                return;

            // gán server đã chọn vào biến toàn cục.
            Program.servername = cmb.SelectedValue.ToString();
            Program.tencn = cmb.Text;
            nganHang.barStaticItemChiNhanh.Caption = Program.tencn;
            Console.WriteLine("Servername : " + Program.servername);

            // đoạn code hỗ trợ chuyển chi nhánh
            // ở chi nhánh A qua B thì dùng RemoteLogin,
            if (cmb.SelectedIndex != Program.mChinhanh)
            {
                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            { // ở B về lại A dùng login ban đầu
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }
        }
        public static void BindingDataToComBo(ComboBox combo, Object data)
        {
            combo.DataSource = data;
            combo.DisplayMember = "TENCN";
            combo.ValueMember = "TENSERVER";

           
            if(Program.mChinhanh != 2)
            {
                // lệnh này quan trọng... phải bỏ vào. ==> để cho combo box chạy đúng.
                combo.SelectedIndex = 1;

                // nếu login vào là chi nhánh ben thanh, thì combox sẽ hiện ben thanh
                // nếu login vào là chi nhanh tan dinh thì combox sẽ hiện tan dinh 
                combo.SelectedIndex = Program.mChinhanh;
            }
            
        }
        public static String getMaChiNhanhHienTai()
        {
            String cn = "";
            Program.KetNoi();
            Program.myReader = Program.ExecSqlDataReader("SELECT MACN FROM ChiNhanh");
            if (Program.myReader.Read())
            {
                cn = Program.myReader.GetString(0).ToString();
            }
            else
            {
                MessageBox.Show("Không Tìm thấy chi nhánh");
            }
            Program.myReader.Close();
            Program.conn.Close();
            return cn;
        }
        [STAThread]
        static void Main()
        { 
            Application.EnableVisualStyles();
            Application.Run(nganHang);
        }
    }
}
