﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
namespace NganHang
{
    public partial class XfrmThongKeKhachHang : DevExpress.XtraEditors.XtraForm
    {
        public XfrmThongKeKhachHang()
        {
            InitializeComponent();
        }

        private void comboBoxCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void XfrmThongKeKhachHang_Load(object sender, EventArgs e)
        {
            try
            {
                comboBoxCN.DataSource = Program.view_macn;
                comboBoxCN.DisplayMember = "TENCN";
                comboBoxCN.ValueMember = "MACN";
                comboBoxCN.SelectedValue = "BENTHANH";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loi chon chi nhanh cho nhan vien" + ex.Message, "", MessageBoxButtons.OK);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ThongKeKhachHang rpt = new ThongKeKhachHang(comboBoxCN.SelectedValue.ToString());
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreviewDialog();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            comboBoxCN.Enabled = false;

        }
    }
}