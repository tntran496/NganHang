﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using System.Data.SqlClient;
namespace NganHang
{
    public partial class XfrmThongKeTaiKhoanKH : DevExpress.XtraEditors.XtraForm
    {
        int luachon = 0;
        String tencn = "";
        public XfrmThongKeTaiKhoanKH()
        {
            InitializeComponent();
        }

        private void XfrmThongKeTaiKhoanKH_Load(object sender, EventArgs e)
        {

            comboBoxChiNhanh.DataSource = Program.view_macn;
            comboBoxChiNhanh.DisplayMember = "TENCN";
            comboBoxChiNhanh.ValueMember = "MACN";

            Program.servername = Program.cnnv;
            Program.KetNoi();
            SqlDataReader rd = Program.ExecSqlDataReader("SELECT MACN FROM ChiNhanh");
            if (rd.Read())
            {
                comboBoxChiNhanh.SelectedValue = rd.GetString(0).ToString();
            }
            else
            {
                MessageBox.Show("Không Tìm thấy chi nhánh");
            }
        }

        private void simpleButtonInThongKe_Click(object sender, EventArgs e)
        {
            try
            {
                if (luachon == 1)
                {
                    for (int i = 0; i < Program.view_macn.Count; i++)
                    {
                        tencn += ((DataRowView)Program.view_macn[i])["TENCN"].ToString() + " ";

                    }
                }
                else
                {
                    tencn = comboBoxChiNhanh.Text;
                }
                ThongKeTaiKhoanKhachHang rpt = new ThongKeTaiKhoanKhachHang(comboBoxChiNhanh.SelectedValue.ToString(), DateTime.Parse(dateEditTuNgay.Text), DateTime.Parse(dateEditDenNgay.Text), luachon, tencn);
                ReportPrintTool print = new ReportPrintTool(rpt);
                print.ShowPreviewDialog();
                tencn = "";
            }
            catch(Exception)
            {

            }
        }

        private void checkBox1ChiNhanh_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1ChiNhanh.Checked)
            {
                groupBoxLuaChon.Enabled = false;
                groupBoxFormThongTIn.Enabled = true;
                if(Program.mGroup == "NGANHANG")
                {
                    comboBoxChiNhanh.Enabled = true;
                }
                luachon = 0;
            }
        }

        private void simpleButtonTHoat_Click(object sender, EventArgs e)
        {
            groupBoxLuaChon.Enabled = true;
            groupBoxFormThongTIn.Enabled = false;
            comboBoxChiNhanh.Enabled = false;
            luachon = 0;
            tencn = "";
            checkBox1ChiNhanh.Checked = false;
            checkBoxTatCaChiNhanh.Checked = false;
        }

        private void checkBoxTatCaChiNhanh_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTatCaChiNhanh.Checked)
            {
                groupBoxLuaChon.Enabled = false;
                groupBoxFormThongTIn.Enabled = true;
                luachon = 1;
            }

               
        }

        private void comboBoxChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}