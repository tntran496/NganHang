﻿namespace NganHang
{
    partial class FormGuiRut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGuiRut));
            this.panelGuiRut = new System.Windows.Forms.Panel();
            this.gD_GOIRUTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nGANHANGDataSet = new NganHang.NGANHANGDataSet();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.buttonThem = new DevExpress.XtraBars.BarButtonItem();
            this.buttonXoa = new DevExpress.XtraBars.BarButtonItem();
            this.buttonGD = new DevExpress.XtraBars.BarButtonItem();
            this.buttonThoat = new DevExpress.XtraBars.BarButtonItem();
            this.buttonReload = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditTien = new DevExpress.XtraEditors.SpinEdit();
            this.dateEditNgayGD = new DevExpress.XtraEditors.DateEdit();
            this.textBoxMNV = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxLOAIGD = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxMGD = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboBoxCN = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gridControlGoiRut = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAGD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNGAYGD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLOAIGD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOTK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMANV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gD_GOIRUTTableAdapter = new NganHang.NGANHANGDataSetTableAdapters.GD_GOIRUTTableAdapter();
            this.tableAdapterManager = new NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.sOTKSearchLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textBoxSTK = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.panelGuiRut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gD_GOIRUTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNgayGD.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNgayGD.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoiRut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTKSearchLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSTK.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelGuiRut
            // 
            this.panelGuiRut.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panelGuiRut.Controls.Add(this.textBoxSTK);
            this.panelGuiRut.Controls.Add(this.labelControl1);
            this.panelGuiRut.Controls.Add(this.spinEditTien);
            this.panelGuiRut.Controls.Add(this.dateEditNgayGD);
            this.panelGuiRut.Controls.Add(this.textBoxMNV);
            this.panelGuiRut.Controls.Add(this.label3);
            this.panelGuiRut.Controls.Add(this.label2);
            this.panelGuiRut.Controls.Add(this.comboBoxLOAIGD);
            this.panelGuiRut.Controls.Add(this.label6);
            this.panelGuiRut.Controls.Add(this.textBoxMGD);
            this.panelGuiRut.Controls.Add(this.label5);
            this.panelGuiRut.Controls.Add(this.label9);
            this.panelGuiRut.Controls.Add(this.label4);
            this.panelGuiRut.Controls.Add(this.label1);
            this.panelGuiRut.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelGuiRut.Enabled = false;
            this.panelGuiRut.Location = new System.Drawing.Point(0, 45);
            this.panelGuiRut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelGuiRut.Name = "panelGuiRut";
            this.panelGuiRut.Size = new System.Drawing.Size(1668, 289);
            this.panelGuiRut.TabIndex = 3;
            // 
            // gD_GOIRUTBindingSource
            // 
            this.gD_GOIRUTBindingSource.DataMember = "GD_GOIRUT";
            this.gD_GOIRUTBindingSource.DataSource = this.nGANHANGDataSet;
            // 
            // nGANHANGDataSet
            // 
            this.nGANHANGDataSet.DataSetName = "NGANHANGDataSet";
            this.nGANHANGDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.buttonThem,
            this.buttonXoa,
            this.buttonGD,
            this.buttonThoat,
            this.buttonReload});
            this.barManager1.MainMenu = this.bar3;
            this.barManager1.MaxItemId = 5;
            // 
            // bar3
            // 
            this.bar3.BarName = "Main menu";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.buttonThem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.buttonXoa, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.buttonGD, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.buttonThoat, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.buttonReload, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar3.OptionsBar.MultiLine = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // buttonThem
            // 
            this.buttonThem.Caption = "Thêm";
            this.buttonThem.Id = 0;
            this.buttonThem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonThem.ImageOptions.Image")));
            this.buttonThem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("buttonThem.ImageOptions.LargeImage")));
            this.buttonThem.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThem.ItemAppearance.Normal.Options.UseFont = true;
            this.buttonThem.Name = "buttonThem";
            this.buttonThem.Size = new System.Drawing.Size(85, 30);
            this.buttonThem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // buttonXoa
            // 
            this.buttonXoa.Caption = "Xóa";
            this.buttonXoa.Id = 1;
            this.buttonXoa.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonXoa.ImageOptions.Image")));
            this.buttonXoa.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("buttonXoa.ImageOptions.LargeImage")));
            this.buttonXoa.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonXoa.ItemAppearance.Normal.Options.UseFont = true;
            this.buttonXoa.Name = "buttonXoa";
            this.buttonXoa.Size = new System.Drawing.Size(85, 30);
            this.buttonXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // buttonGD
            // 
            this.buttonGD.Caption = "Giao dịch";
            this.buttonGD.Enabled = false;
            this.buttonGD.Id = 2;
            this.buttonGD.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonGD.ImageOptions.Image")));
            this.buttonGD.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("buttonGD.ImageOptions.LargeImage")));
            this.buttonGD.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGD.ItemAppearance.Normal.Options.UseFont = true;
            this.buttonGD.Name = "buttonGD";
            this.buttonGD.Size = new System.Drawing.Size(105, 30);
            this.buttonGD.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // buttonThoat
            // 
            this.buttonThoat.Caption = "Thoát";
            this.buttonThoat.Enabled = false;
            this.buttonThoat.Id = 3;
            this.buttonThoat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonThoat.ImageOptions.Image")));
            this.buttonThoat.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("buttonThoat.ImageOptions.LargeImage")));
            this.buttonThoat.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThoat.ItemAppearance.Normal.Options.UseFont = true;
            this.buttonThoat.Name = "buttonThoat";
            this.buttonThoat.Size = new System.Drawing.Size(85, 30);
            this.buttonThoat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // buttonReload
            // 
            this.buttonReload.Caption = "Reload";
            this.buttonReload.Id = 4;
            this.buttonReload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonReload.ImageOptions.Image")));
            this.buttonReload.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("buttonReload.ImageOptions.LargeImage")));
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick_1);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1668, 45);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 703);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1668, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 45);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 658);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1668, 45);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 658);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(816, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(244, 32);
            this.labelControl1.TabIndex = 37;
            this.labelControl1.Text = "Thông Tin Giao Dịch";
            // 
            // spinEditTien
            // 
            this.spinEditTien.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_GOIRUTBindingSource, "SOTIEN", true));
            this.spinEditTien.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditTien.Location = new System.Drawing.Point(820, 67);
            this.spinEditTien.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.spinEditTien.Name = "spinEditTien";
            this.spinEditTien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditTien.Size = new System.Drawing.Size(240, 28);
            this.spinEditTien.TabIndex = 36;
            this.spinEditTien.EditValueChanged += new System.EventHandler(this.spinEditTien_EditValueChanged);
            // 
            // dateEditNgayGD
            // 
            this.dateEditNgayGD.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_GOIRUTBindingSource, "NGAYGD", true));
            this.dateEditNgayGD.EditValue = null;
            this.dateEditNgayGD.Enabled = false;
            this.dateEditNgayGD.Location = new System.Drawing.Point(1408, 120);
            this.dateEditNgayGD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateEditNgayGD.Name = "dateEditNgayGD";
            this.dateEditNgayGD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditNgayGD.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditNgayGD.Size = new System.Drawing.Size(240, 28);
            this.dateEditNgayGD.TabIndex = 35;
            // 
            // textBoxMNV
            // 
            this.textBoxMNV.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gD_GOIRUTBindingSource, "MANV", true));
            this.textBoxMNV.Enabled = false;
            this.textBoxMNV.Location = new System.Drawing.Point(822, 121);
            this.textBoxMNV.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxMNV.Name = "textBoxMNV";
            this.textBoxMNV.Size = new System.Drawing.Size(238, 27);
            this.textBoxMNV.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(116, 118);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 24);
            this.label3.TabIndex = 33;
            this.label3.Text = "Mã giao dịch";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1188, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 24);
            this.label2.TabIndex = 31;
            this.label2.Text = "Ngày giao dịch";
            // 
            // comboBoxLOAIGD
            // 
            this.comboBoxLOAIGD.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gD_GOIRUTBindingSource, "LOAIGD", true));
            this.comboBoxLOAIGD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLOAIGD.FormattingEnabled = true;
            this.comboBoxLOAIGD.Items.AddRange(new object[] {
            "GT",
            "RT"});
            this.comboBoxLOAIGD.Location = new System.Drawing.Point(1410, 66);
            this.comboBoxLOAIGD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxLOAIGD.Name = "comboBoxLOAIGD";
            this.comboBoxLOAIGD.Size = new System.Drawing.Size(238, 27);
            this.comboBoxLOAIGD.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1188, 69);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 24);
            this.label6.TabIndex = 24;
            this.label6.Text = "Loại Giao dịch";
            // 
            // textBoxMGD
            // 
            this.textBoxMGD.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gD_GOIRUTBindingSource, "MAGD", true));
            this.textBoxMGD.Enabled = false;
            this.textBoxMGD.Location = new System.Drawing.Point(310, 118);
            this.textBoxMGD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxMGD.Name = "textBoxMGD";
            this.textBoxMGD.Size = new System.Drawing.Size(238, 27);
            this.textBoxMGD.TabIndex = 22;
            this.textBoxMGD.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(660, 121);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 24);
            this.label5.TabIndex = 21;
            this.label5.Text = "Mã nhân viên";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(753, 250);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 19);
            this.label9.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(660, 69);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "Số tiền";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(116, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số tài khoản";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox3.Controls.Add(this.comboBoxCN);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 334);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(1668, 58);
            this.groupBox3.TabIndex = 39;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Chuyển chi nhánh";
            // 
            // comboBoxCN
            // 
            this.comboBoxCN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCN.Enabled = false;
            this.comboBoxCN.FormattingEnabled = true;
            this.comboBoxCN.Location = new System.Drawing.Point(820, 23);
            this.comboBoxCN.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxCN.Name = "comboBoxCN";
            this.comboBoxCN.Size = new System.Drawing.Size(306, 27);
            this.comboBoxCN.TabIndex = 38;
            this.comboBoxCN.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(656, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 24);
            this.label7.TabIndex = 37;
            this.label7.Text = "Chi Nhánh  :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Controls.Add(this.gridControlGoiRut);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 346);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1668, 357);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách giao dịch";
            // 
            // gridControlGoiRut
            // 
            this.gridControlGoiRut.CausesValidation = false;
            this.gridControlGoiRut.DataSource = this.gD_GOIRUTBindingSource;
            this.gridControlGoiRut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlGoiRut.Location = new System.Drawing.Point(3, 23);
            this.gridControlGoiRut.MainView = this.gridView1;
            this.gridControlGoiRut.Name = "gridControlGoiRut";
            this.gridControlGoiRut.Size = new System.Drawing.Size(1662, 331);
            this.gridControlGoiRut.TabIndex = 0;
            this.gridControlGoiRut.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControlGoiRut.Click += new System.EventHandler(this.gridControlGoiRut_Click);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAGD,
            this.colNGAYGD,
            this.colLOAIGD,
            this.colSOTK,
            this.colSOTIEN,
            this.colMANV});
            this.gridView1.DetailHeight = 349;
            this.gridView1.FixedLineWidth = 1;
            this.gridView1.GridControl = this.gridControlGoiRut;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            // 
            // colMAGD
            // 
            this.colMAGD.FieldName = "MAGD";
            this.colMAGD.MinWidth = 30;
            this.colMAGD.Name = "colMAGD";
            this.colMAGD.Visible = true;
            this.colMAGD.VisibleIndex = 0;
            this.colMAGD.Width = 112;
            // 
            // colNGAYGD
            // 
            this.colNGAYGD.FieldName = "NGAYGD";
            this.colNGAYGD.MinWidth = 30;
            this.colNGAYGD.Name = "colNGAYGD";
            this.colNGAYGD.Visible = true;
            this.colNGAYGD.VisibleIndex = 1;
            this.colNGAYGD.Width = 112;
            // 
            // colLOAIGD
            // 
            this.colLOAIGD.FieldName = "LOAIGD";
            this.colLOAIGD.MinWidth = 30;
            this.colLOAIGD.Name = "colLOAIGD";
            this.colLOAIGD.Visible = true;
            this.colLOAIGD.VisibleIndex = 2;
            this.colLOAIGD.Width = 112;
            // 
            // colSOTK
            // 
            this.colSOTK.FieldName = "SOTK";
            this.colSOTK.MinWidth = 30;
            this.colSOTK.Name = "colSOTK";
            this.colSOTK.Visible = true;
            this.colSOTK.VisibleIndex = 3;
            this.colSOTK.Width = 112;
            // 
            // colSOTIEN
            // 
            this.colSOTIEN.FieldName = "SOTIEN";
            this.colSOTIEN.MinWidth = 30;
            this.colSOTIEN.Name = "colSOTIEN";
            this.colSOTIEN.Visible = true;
            this.colSOTIEN.VisibleIndex = 4;
            this.colSOTIEN.Width = 112;
            // 
            // colMANV
            // 
            this.colMANV.FieldName = "MANV";
            this.colMANV.MinWidth = 30;
            this.colMANV.Name = "colMANV";
            this.colMANV.Visible = true;
            this.colMANV.VisibleIndex = 5;
            this.colMANV.Width = 112;
            // 
            // gD_GOIRUTTableAdapter
            // 
            this.gD_GOIRUTTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ChiNhanhTableAdapter = null;
            this.tableAdapterManager.GD_CHUYENTIENTableAdapter = null;
            this.tableAdapterManager.GD_GOIRUTTableAdapter = this.gD_GOIRUTTableAdapter;
            this.tableAdapterManager.KhachHangTableAdapter = null;
            this.tableAdapterManager.NhanVienTableAdapter = null;
            this.tableAdapterManager.TaiKhoanTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = NganHang.NGANHANGDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bar4
            // 
            this.bar4.BarName = "Status bar";
            this.bar4.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar4.OptionsBar.AllowQuickCustomization = false;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.Text = "Status bar";
            // 
            // sOTKSearchLookUpEditView
            // 
            this.sOTKSearchLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.sOTKSearchLookUpEditView.Name = "sOTKSearchLookUpEditView";
            this.sOTKSearchLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.sOTKSearchLookUpEditView.OptionsView.ShowGroupPanel = false;
            // 
            // textBoxSTK
            // 
            this.textBoxSTK.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gD_GOIRUTBindingSource, "SOTK", true));
            this.textBoxSTK.Location = new System.Drawing.Point(310, 72);
            this.textBoxSTK.MenuManager = this.barManager1;
            this.textBoxSTK.Name = "textBoxSTK";
            this.textBoxSTK.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textBoxSTK.Properties.PopupView = this.sOTKSearchLookUpEditView;
            this.textBoxSTK.Size = new System.Drawing.Size(238, 28);
            this.textBoxSTK.TabIndex = 38;
            // 
            // FormGuiRut
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1668, 703);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelGuiRut);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormGuiRut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Gửi Rút";
            this.Load += new System.EventHandler(this.FormGuiRut_Load);
            this.panelGuiRut.ResumeLayout(false);
            this.panelGuiRut.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gD_GOIRUTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGANHANGDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNgayGD.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNgayGD.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoiRut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTKSearchLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSTK.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelGuiRut;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxLOAIGD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxMGD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMNV;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private NGANHANGDataSet nGANHANGDataSet;
        private System.Windows.Forms.BindingSource gD_GOIRUTBindingSource;
        private NGANHANGDataSetTableAdapters.GD_GOIRUTTableAdapter gD_GOIRUTTableAdapter;
        private NGANHANGDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.DateEdit dateEditNgayGD;
        private DevExpress.XtraEditors.SpinEdit spinEditTien;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxCN;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraGrid.GridControl gridControlGoiRut;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colMAGD;
        private DevExpress.XtraGrid.Columns.GridColumn colNGAYGD;
        private DevExpress.XtraGrid.Columns.GridColumn colLOAIGD;
        private DevExpress.XtraGrid.Columns.GridColumn colSOTK;
        private DevExpress.XtraGrid.Columns.GridColumn colSOTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn colMANV;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarButtonItem buttonThem;
        private DevExpress.XtraBars.BarButtonItem buttonXoa;
        private DevExpress.XtraBars.BarButtonItem buttonGD;
        private DevExpress.XtraBars.BarButtonItem buttonThoat;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarButtonItem buttonReload;
        private DevExpress.XtraEditors.SearchLookUpEdit textBoxSTK;
        private DevExpress.XtraGrid.Views.Grid.GridView sOTKSearchLookUpEditView;
    }
}