﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Collections;

namespace NganHang
{
    public partial class KhachHang : DevExpress.XtraEditors.XtraForm
    {

        Stack<SqlCommand> sqlsCache;

        String cn = "";
        String sukien = "";
        String oldcmnd = "";

        public KhachHang()
        {
            InitializeComponent();
        }

        private void KhachHang_Load(object sender, EventArgs e)
        {
            try {
                // TODO: This line of code loads data into the 'nGANHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
                Program.bds_dspm.Filter = "TENCN <> 'Khách Hàng'";
                Program.BindingDataToComBo(comboBoxChiNhanh, Program.bds_dspm);
                // TODO: This line of code loads data into the 'nGANHANGDataSet.KhachHang' table. You can move, or remove it, as needed.

                this.khachHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.khachHangTableAdapter.Fill(this.nGANHANGDataSet.KhachHang);
                if (Program.mChinhanh == 2)
                {
                    this.comboBoxChiNhanh.Visible = false;
                }
                else
                {
                    if (Program.mGroup.Trim() == "NGANHANG")
                    {
                        this.comboBoxChiNhanh.Enabled = true;
                    }
                    cn = Program.getMaChiNhanhHienTai();


                    sqlsCache = new Stack<SqlCommand>();
                   
                }
            } catch(Exception) { }
            
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void khachHangGridControl_Click(object sender, EventArgs e)
        {

        }

        private void labelControl9_Click(object sender, EventArgs e)
        {

        }

        private void barButtonThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sukien = "THEM";

            khachHangBindingSource.AddNew();
            textEditCN.Text = cn;
            comboBoxPhai.SelectedIndex = 1;
            comboBoxPhai.SelectedItem = "NỮ";
           
            khachHangGridControl.Enabled = false;
            groupBoxThongTinNv.Enabled = true;
            barButtonItemDelete.Enabled = barButtonItemSua.Enabled = barButtonItemPhucHoi.Enabled = barButtonItemThemKH.Enabled = false;
            barButtonItemSave.Enabled = barButtonItemThoatKh.Enabled = true;
            this.comboBoxChiNhanh.Enabled = false;
        }

        private void barButtonItemThoatKh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            khachHangBindingSource.CancelEdit();
            oldcmnd = "";
            
            khachHangGridControl.Enabled = true;
            groupBoxThongTinNv.Enabled = false;
            barButtonItemDelete.Enabled = barButtonItemSua.Enabled = barButtonItemThemKH.Enabled= barButtonItemPhucHoi.Enabled = true;
            barButtonItemSave.Enabled = barButtonItemThoatKh.Enabled = false;
            this.comboBoxChiNhanh.Enabled = false;
            if (Program.mGroup.Trim() == "NGANHANG")
            {
                this.comboBoxChiNhanh.Enabled = true;
            }
        }

        private void barButtonLuu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            String ho = "";
            String ten = "";
            String diachi = "";
            String phai = "";
            String sodt = "";
            String ngaycap = "";
            String newcmnd = "";
            if (textEditCMND.Text.Trim() == "")
            {
                MessageBox.Show("CMND Không Được Trống");
                textEditCMND.Focus();
                return;
            }
            if (textEditHo.Text.Trim() == "")
            {
                MessageBox.Show("Họ Không Được Trống");
                textEditHo.Focus();
                return;
            }
            if (textEditTen.Text.Trim() == "")
            {
                MessageBox.Show("Tên Không Được Trống");
                textEditTen.Focus();
                return;
            }
            if (textEditSDT.Text.Trim() == "")
            {
                MessageBox.Show("Số điện thoại Không Được Trống");
                textEditSDT.Focus();
                return;
            }
            if (textEditDiaChi.Text.Trim() == "")
            {
                MessageBox.Show("Địa chỉ Không Được Trống");
                textEditDiaChi.Focus();
                return;
            }
            if (textEditCN.Text.Trim() == "")
            {
                MessageBox.Show("Chi nhánh Không Được Trống");
                textEditCN.Focus();
                return;
            }
            if (dateeEditNgayCap.Text.Trim() == "")
            {
                MessageBox.Show("Ngày Không Được Trống");
                dateeEditNgayCap.Focus();
                return;
            }
            if (sukien == "THEM")
            {
                if(checkCmndKhachHang(textEditCMND.Text.Trim()) == 1)
                {
                    MessageBox.Show("CMND của khách hàng này đã tồn tại");
                    textEditCMND.Focus();
                    return;
                }
            }
            if(sukien == "SUA")
            {
                if(oldcmnd.Trim()!= textEditCMND.Text.Trim())
                {
                    if (checkCmndKhachHang(textEditCMND.Text.Trim()) == 1)
                    {
                        MessageBox.Show("CMND của khách hàng này đã tồn tại");
                        textEditCMND.Focus();
                        return;
                    }
                }
            }
            try
            {
                if(sukien == "SUA")
                {
                    ho = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["HO"].ToString();
                    ten = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["TEN"].ToString();
                    diachi = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["DIACHI"].ToString();
                    phai = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["PHAI"].ToString();
                    sodt = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["SODT"].ToString();
                    ngaycap = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["NGAYCAP"].ToString();
                }
                   
                khachHangBindingSource.EndEdit();
                khachHangBindingSource.ResetCurrentItem();
                this.khachHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.khachHangTableAdapter.Update(this.nGANHANGDataSet.KhachHang);

                newcmnd = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["CMND"].ToString();
                //cache
                if (sukien == "SUA")
                {
                    SqlCommand sql = new SqlCommand("UPDATE KhachHang SET CMND = @CMND,HO = @HO,TEN = @TEN ,DIACHI = @DIACHI,PHAI = @PHAI,NGAYCAP = @NGAYCAP,SODT =@SODT ,MACN = @MACN WHERE CMND = @NEWCMND");
                    sql.Parameters.AddWithValue("@CMND", oldcmnd);
                    sql.Parameters.AddWithValue("@HO", ho);
                    sql.Parameters.AddWithValue("@TEN", ten);
                    sql.Parameters.AddWithValue("@DIACHI", diachi);
                    sql.Parameters.AddWithValue("@PHAI", phai);
                    sql.Parameters.AddWithValue("@NGAYCAP", DateTime.Parse(ngaycap));
                    sql.Parameters.AddWithValue("@SODT", sodt);
                    sql.Parameters.AddWithValue("@MACN", cn);
                    sql.Parameters.AddWithValue("@NEWCMND", newcmnd);
                    sqlsCache.Push(sql);
                }
                if (sukien == "THEM")
                {
                    String str = "if(not exists(select TOP 1 TK.SOTK from NGANHANG.DBO.TaiKhoan as TK where TK.CMND = @CMND) AND not exists(select TOP 1 TK.SOTK from LINK.NGANHANG.DBO.TaiKhoan as TK where TK.CMND = @CMND)) begin DELETE FROM KhachHang WHERE CMND = @CMND end else begin RAISERROR('KHÁCH HÀNG NÀY ĐÃ TỒN TẠI TÀI KHOẢN ',16,1) end";
                    SqlCommand sql = new SqlCommand(str);
                    sql.Parameters.AddWithValue("@CMND", newcmnd);
                    sqlsCache.Push(sql);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Lỗi ghi Khách Hàng "+ex.Message,"",MessageBoxButtons.OK);
                reload();
            }
            finally
            {
                khachHangGridControl.Enabled = true;
                groupBoxThongTinNv.Enabled = false;
                barButtonItemDelete.Enabled = barButtonItemSua.Enabled = barButtonItemThemKH.Enabled = barButtonItemPhucHoi.Enabled = true;
                barButtonItemSave.Enabled = barButtonItemThoatKh.Enabled = false;
                if (Program.mGroup.Trim() == "NGANHANG")
                {
                    this.comboBoxChiNhanh.Enabled = true;
                }
            }
            return;
        }

        private void barButtonSua_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sukien = "SUA";
            oldcmnd = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["CMND"].ToString();

            khachHangGridControl.Enabled = false;
            groupBoxThongTinNv.Enabled = true;
            barButtonItemDelete.Enabled = barButtonItemThemKH.Enabled = barButtonItemPhucHoi.Enabled = barButtonItemSua.Enabled = false;
            barButtonItemSave.Enabled = barButtonItemThoatKh.Enabled = true;
        }

        private void barButtonItemDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String cmnd = "";
            String ho = "";
            String ten = "";
            String diachi = "";
            String phai = "";
            String sodt = "";
            String ngaycap = "";
            if(MessageBox.Show("Bạn có chắc muốn xóa ?","Xác Nhận",MessageBoxButtons.OKCancel ) == DialogResult.OK)
            {
                try
                {
                    cmnd = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["CMND"].ToString();
                    if (checkTonTaiTaiKhoanKH(cmnd))
                    {
                        XtraMessageBox.Show("Khách hàng này tồn tại tài khoản không được xóa", "", MessageBoxButtons.OK);
                        return;
                    }
                    //cache
                    ho = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["HO"].ToString();
                    ten = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["TEN"].ToString();
                    diachi = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["DIACHI"].ToString();
                    phai = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["PHAI"].ToString();
                    sodt = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["SODT"].ToString();
                    ngaycap = ((DataRowView)khachHangBindingSource[khachHangBindingSource.Position])["NGAYCAP"].ToString();

                    khachHangBindingSource.RemoveCurrent();
                    this.khachHangTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.khachHangTableAdapter.Update(this.nGANHANGDataSet.KhachHang);

                    //cache
                    SqlCommand sql = new SqlCommand("INSERT INTO KhachHang(CMND,HO,TEN,DIACHI,PHAI,NGAYCAP,SODT,MACN) VALUES (@CMND,@HO,@TEN,@DIACHI,@PHAI,@NGAYCAP,@SODT,@MACN)");
                    sql.Parameters.AddWithValue("@CMND", cmnd);
                    sql.Parameters.AddWithValue("@HO", ho);
                    sql.Parameters.AddWithValue("@TEN", ten);
                    sql.Parameters.AddWithValue("@DIACHI", diachi);
                    sql.Parameters.AddWithValue("@PHAI", phai);
                    sql.Parameters.AddWithValue("@NGAYCAP", DateTime.Parse(ngaycap));
                    sql.Parameters.AddWithValue("@SODT", sodt);
                    sql.Parameters.AddWithValue("@MACN", cn);
                    sqlsCache.Push(sql);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Lỗi Xóa khách hàng "+ex.Message,"",MessageBoxButtons.OK);
                }
                finally
                {
                    reload();
                    khachHangBindingSource.Position = khachHangBindingSource.Find("CMND",cmnd);
                }
                return;
            }
            else
            {
                return;
            }
        }

        private void comboBoxChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Program.mChinhanh != 2)
            {
                try
                {
                    Program.ComboChuyenChiNhanh(comboBoxChiNhanh);
                    if (Program.KetNoi() == 0)
                    {
                        XtraMessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
                    }
                    else
                    {
                        reload();
                        cn = Program.getMaChiNhanhHienTai();
                        sqlsCache = new Stack<SqlCommand>();
                    }
                }
                catch (Exception) { }
            }
        }

        private void barButtonItemPhucHoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (sqlsCache.Count > 0)
            {
                if(MessageBox.Show("Bạn có chắc muốn phục hồi ?", "Xác Nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    SqlCommand sql = sqlsCache.Pop();
                    try
                    {
                        Program.KetNoi();
                        sql.Connection = Program.conn;
                        sql.ExecuteNonQuery();
                        reload();
                        MessageBox.Show("Phục Hồi Thành Công !", "", MessageBoxButtons.OK);
                        Program.conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi phục Hồi !" + ex.Message);
                        reload();
                    }
                }
                else
                {
                    return;
                }
            }
        }
        private int checkCmndKhachHang(String cmnd)
        {
            int result = 0;
            try
            {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@CMND", cmnd) };
                Program.myReader = Program.CallSpDataReader("SP_CHECK_CMND_KH", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch(Exception) { }
            return result;
        }
        private Boolean checkTonTaiTaiKhoanKH(String cmnd)
        {
            Boolean result = false;
            try
            {
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@CMND", cmnd) };
                Program.myReader = Program.CallSpDataReader("SP_Check_TonTai_TaiKhoan_CMND", list);
                if (Program.myReader.Read())
                {
                    result = true;
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch (Exception) { }
            return result;
        }
        private void reload()
        {
            try
            {
                this.khachHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.khachHangTableAdapter.Fill(this.nGANHANGDataSet.KhachHang);
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
    }
}