﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
namespace NganHang
{
    public partial class RegisterNvForm : DevExpress.XtraEditors.XtraForm
    {
        Stack<SqlCommand> sqlsCache;

        int vitri;
        String cn = "";
        String sukien = "";
        String oldMNV = "";
        public RegisterNvForm()
        {
            InitializeComponent();
        }

        private void RegisterNvForm_Load(object sender, EventArgs e)
        {
            try
            {
                comboBoxCNNV.DataSource = Program.view_macn;
                comboBoxCNNV.DisplayMember = "MACN";
                comboBoxCNNV.ValueMember = "MACN";

                Program.bds_dspm.Filter = "TENCN <> 'Khách Hàng'";
                Program.BindingDataToComBo(comboBoxChiNhanh, Program.bds_dspm);
                this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                this.nhanVienTableAdapter.Fill(this.nGANHANGDataSet.NhanVien);

                this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
                this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);

                this.gD_CHUYENTIENTableAdapter.Connection.ConnectionString = Program.connstr;
                this.gD_CHUYENTIENTableAdapter.Fill(this.nGANHANGDataSet.GD_CHUYENTIEN);

                if (Program.mGroup.Trim() == "NGANHANG")
                {
                    this.comboBoxChiNhanh.Enabled = true;
                }

                cn = Program.getMaChiNhanhHienTai();

                sqlsCache = new Stack<SqlCommand>();

                gridViewNhanVien.Columns["TrangThaiXoa"].FilterInfo = new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[TrangThaiXoa] LIKE 0");

            }
            catch(Exception )
            {  }
            
        }

      
        private void comboBoxChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Program.ComboChuyenChiNhanh(comboBoxChiNhanh);
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
                }
                else
                {
                    this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.nhanVienTableAdapter.Fill(this.nGANHANGDataSet.NhanVien);


                    this.gD_GOIRUTTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.gD_GOIRUTTableAdapter.Fill(this.nGANHANGDataSet.GD_GOIRUT);

                    this.gD_CHUYENTIENTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.gD_CHUYENTIENTableAdapter.Fill(this.nGANHANGDataSet.GD_CHUYENTIEN);
                    cn = Program.getMaChiNhanhHienTai();
                    sqlsCache = new Stack<SqlCommand>();
                }
            }
            catch (Exception) { }
        }

        private void barButtonThem(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                sukien = "THEM";
                nhanVienBindingSource.AddNew();
                comboBoxCNNV.Text = cn;
                comboBoxPhai.SelectedIndex = 1;
                comboBoxPhai.SelectedItem = "NỮ";
                checkBoxTranThaiXoa.Checked = false;

                btnLuu.Enabled = true;
                btnThoat.Enabled = true;
                btnThem.Enabled = btnSua.Enabled = btnXoa.Enabled = btnPhucHoi.Enabled = false;
                panelNhanVien.Enabled = true;
                gridControlNV.Enabled = false;
                this.comboBoxChiNhanh.Enabled = barButtonItem1 .Enabled= checkBoxTranThaiXoa.Enabled = false;

            }
            catch (Exception) { }
        }

        private void barButtonXoa(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String manv = "";
            if (gD_CHUYENTIENBindingSource.Count > 0)
            {
                MessageBox.Show("Nhân viên này có giao dịch chuyển tiền!");
                return;
            }
            if (gD_GOIRUTBindingSource.Count > 0)
            {
                MessageBox.Show("Nhân viên này có giao dịch gửi rút!");
                return;
            }
            if (MessageBox.Show("Bạn có chắc muốn xóa ?", "Xác Nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {           
                try
                {
                    manv = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["MANV"].ToString();
                    if(Program.username == manv)
                    {
                        MessageBox.Show("Không được xóa nhân viên đang đăng nhập!");
                        return;
                    }
                    String sqlTTT = "UPDATE NhanVien SET TrangThaiXoa = 1 WHERE MANV = @MANV";
                    
                    Program.KetNoi();
                    SqlCommand sqlc = new SqlCommand(sqlTTT, Program.conn);
                    sqlc.Parameters.AddWithValue("@MANV", manv);
                    sqlc.CommandType = CommandType.Text;

                    sqlc.ExecuteNonQuery();              
                    Program.conn.Close();

                    string LOGGIN = getLGNAME(manv);
                    if(LOGGIN != null)
                    {
                        Program.KetNoi();
                        SqlCommand sqlc1 = new SqlCommand("Xoa_Login", Program.conn);
                        sqlc1.CommandType = CommandType.StoredProcedure;
                        sqlc1.Parameters.AddWithValue("@LGNAME", LOGGIN);
                        sqlc1.Parameters.AddWithValue("@USRNAME", manv);
                        sqlc1.ExecuteNonQuery();
                        Program.conn.Close();
                    }
                    

                    //cache
                    SqlCommand sql = new SqlCommand("UPDATE NhanVien SET TrangThaiXoa = 0 WHERE MANV = @MANV");
                    sql.Parameters.AddWithValue("@MANV", manv);
                    sqlsCache.Push(sql);
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Loi xoa nhan vien!" + ex.Message);
                }
                finally
                {
                    btnLuu.Enabled = false;
                    btnThoat.Enabled = false;
                    btnThem.Enabled = btnSua.Enabled = btnXoa.Enabled = btnPhucHoi.Enabled = true;
                    panelNhanVien.Enabled = false;
                    gridControlNV.Enabled = true;
                    reload();
                    gridViewNhanVien.Columns["TrangThaiXoa"].FilterInfo = new DevExpress.XtraGrid.Columns.ColumnFilterInfo("[TrangThaiXoa] LIKE 0");
                }
            }
            else { return; }
        }

        private void barButtonSua(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                sukien = "SUA";
                oldMNV = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["MANV"].ToString();
                vitri = nhanVienBindingSource.Position;
                btnLuu.Enabled = true;
                btnThoat.Enabled = true;
                btnThem.Enabled = btnSua.Enabled = btnXoa.Enabled = btnPhucHoi.Enabled = false;
                panelNhanVien.Enabled = true;
                comboBoxCNNV.Enabled = false;
                gridControlNV.Enabled = false;
                this.comboBoxChiNhanh.Enabled = false;
                barButtonItem1.Enabled = false;
            }
            catch (Exception) { }
        }



        private void barButtonGhi(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            String ho = "";
            String ten = "";
            String diachi = "";
            String phai = "";
            String sodt = "";
            String trangthaixoa = "";
            String newMANV = "";
            if (textBoxMNV.Text.Trim() == "")
            {
                MessageBox.Show("Ma nhan vien khong de trong!");
                textBoxMNV.Focus();
                return;
            }
            if (textBoxHo.Text.Trim() == "")
            {
                MessageBox.Show("Ho nhan vien khong de trong!");
                textBoxHo.Focus();
                return;
            }
            if (textBoxTen.Text.Trim() == "")
            {
                MessageBox.Show("Ten nhan vien khong de trong!");
                textBoxTen.Focus();
                return;
            }
            if (textBoxDiaChi.Text.Trim() == "")
            {
                MessageBox.Show("Dia chi nhan vien khong de trong!");
                textBoxDiaChi.Focus();
                return;
            }
            if (textBoxSDT.Text.Trim() == "")
            {
                MessageBox.Show("SDT nhan vien khong de trong!");
                textBoxSDT.Focus();
                return;
            }

            if (sukien == "THEM")
            {
                if (checkMANV(textBoxMNV.Text.Trim()) == 1)
                {
                    MessageBox.Show("Mã nhân viên này đã tồn tại");
                    textBoxMNV.Focus();
                    return;
                }
            }
            if (sukien == "SUA")
            {
                if (oldMNV.Trim() != textBoxMNV.Text.Trim())
                {
                    if (checkMANV(textBoxMNV.Text.Trim()) == 1)
                    {
                        MessageBox.Show("Mã nhân viên này đã tồn tại");
                        textBoxMNV.Focus();
                        return;
                    }
                }
            }
            try
            {
                if (sukien != "THEM")
                {
                    ho = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["HO"].ToString();
                    ten = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["TEN"].ToString();
                    diachi = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["DIACHI"].ToString();
                    phai = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["PHAI"].ToString();
                    trangthaixoa = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["TrangThaiXoa"].ToString();
                    sodt = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["SODT"].ToString();
                }
      
                if (sukien == "CHUYEN")
                {
                    if(comboBoxCNNV.SelectedValue.ToString().Trim() == cn.Trim() )
                    {
                        MessageBox.Show(" Bạn không được chuyển qua chi nhánh hiện tại!");
                        
                    }
                    else
                    {
                        Program.KetNoi();
                        if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
                        SqlCommand sql = Program.conn.CreateCommand();
                        sql.CommandType = CommandType.StoredProcedure;
                        sql.CommandText = "CHUYEN_CN_NHANVIEN";
                        sql.Parameters.AddWithValue("@MANV", oldMNV);
                        sql.Parameters.AddWithValue("@HO", ho);
                        sql.Parameters.AddWithValue("@TEN", ten);
                        sql.Parameters.AddWithValue("@DIACHI", diachi);
                        sql.Parameters.AddWithValue("@PHAI", phai);
                        sql.Parameters.AddWithValue("@SODT", sodt);
                        sql.Parameters.AddWithValue("@MACN", comboBoxCNNV.SelectedValue);
                        sql.ExecuteNonQuery();
                        sqlsCache.Push(sql);

                        string LOGGIN = getLGNAME(oldMNV);
                        if (LOGGIN != null)
                        {
                            Program.KetNoi();
                            SqlCommand sqlc1 = new SqlCommand("Xoa_Login", Program.conn);
                            sqlc1.CommandType = CommandType.StoredProcedure;
                            sqlc1.Parameters.AddWithValue("@LGNAME", LOGGIN);
                            sqlc1.Parameters.AddWithValue("@USRNAME", oldMNV);
                            sqlc1.ExecuteNonQuery();
                            Program.conn.Close();
                        }



                    }

                }
                else{

                    nhanVienBindingSource.EndEdit();
                    nhanVienBindingSource.ResetCurrentItem();
                    this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.nhanVienTableAdapter.Update(this.nGANHANGDataSet.NhanVien);

                    newMANV = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["MANV"].ToString();

                    //cache
                    if (sukien == "SUA")
                    {

                        SqlCommand sql = new SqlCommand("UPDATE NhanVien SET MANV = @MANV,HO = @HO,TEN = @TEN ,DIACHI = @DIACHI,PHAI = @PHAI,TrangThaiXoa = @TrangThaiXoa,SODT =@SODT ,MACN = @MACN WHERE MANV = @NEWMANV");
                        sql.Parameters.AddWithValue("@MANV", oldMNV);
                        sql.Parameters.AddWithValue("@HO", ho);
                        sql.Parameters.AddWithValue("@TEN", ten);
                        sql.Parameters.AddWithValue("@DIACHI", diachi);
                        sql.Parameters.AddWithValue("@PHAI", phai);
                        sql.Parameters.AddWithValue("@TrangThaiXoa", trangthaixoa);
                        sql.Parameters.AddWithValue("@SODT", sodt);
                        sql.Parameters.AddWithValue("@MACN", cn);
                        sql.Parameters.AddWithValue("@NEWMANV", newMANV);
                        sqlsCache.Push(sql);
                    }
                    if (sukien == "THEM")
                    {
                        SqlCommand sql = new SqlCommand("DELETE FROM NhanVien WHERE MANV = @MANV");
                        sql.Parameters.AddWithValue("@MANV", newMANV);
                        sqlsCache.Push(sql);

                    }
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loi ghi nhan vien" + ex.Message, "", MessageBoxButtons.OK);
                reload();

            }
            finally
            {
                gridControlNV.Enabled = true;
                panelNhanVien.Enabled = false;
                btnXoa.Enabled = btnSua.Enabled = btnThem.Enabled = btnPhucHoi.Enabled = barButtonItem1.Enabled = true;
                btnLuu.Enabled = btnThoat.Enabled = false;
                if (sukien == "CHUYEN")
                {
                    textBoxHo.Enabled = comboBoxPhai.Enabled = textBoxTen.Enabled = textBoxDiaChi.Enabled = textBoxMNV.Enabled = textBoxSDT.Enabled = checkBoxTranThaiXoa.Enabled = true;
                }
                if (Program.mGroup.Trim() == "NGANHANG")
                {
                    this.comboBoxChiNhanh.Enabled = true;
                }
                reload();
            }
            return;
        }

        private void barButtonPhucHoi(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (sqlsCache.Count > 0)
            {
                if (MessageBox.Show("Bạn có chắc muốn phục hồi ?", "Xác Nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    SqlCommand sql = sqlsCache.Pop();
                    try
                    {
                        Program.KetNoi();
                        sql.Connection = Program.conn;
                        sql.ExecuteNonQuery();
                        reload();
                        MessageBox.Show("Phục Hồi Thành Công !", "", MessageBoxButtons.OK);
                        Program.conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi phục Hồi !" + ex.Message);
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void barButtonItemThoat(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            nhanVienBindingSource.CancelEdit();
            btnLuu.Enabled = false;
            btnThoat.Enabled = false;
            btnThem.Enabled = btnSua.Enabled = btnXoa.Enabled = btnPhucHoi.Enabled = true;
            panelNhanVien.Enabled = false;
            gridControlNV.Enabled = true;
            barButtonItem1.Enabled = true;
            //this.comboBoxChiNhanh.Enabled = false;
            if(sukien == "CHUYEN")
            {
                textBoxHo.Enabled = comboBoxPhai.Enabled = textBoxTen.Enabled = textBoxDiaChi.Enabled = textBoxMNV.Enabled = textBoxSDT.Enabled = checkBoxTranThaiXoa.Enabled = true;
            }
            if (Program.mGroup.Trim() == "NGANHANG")
            {
                this.comboBoxChiNhanh.Enabled = true;
            }
        }

        private void gridControlNV_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxPhai_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private String getLGNAME(string manv)
        {
            String result = null;
            try
            {
                Program.KetNoi();

                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@MANV", manv) };
                Program.myReader = Program.CallSpDataReader("SP_GETLOGNME", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetString(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loi xoa nhan vien" + ex.Message, "", MessageBoxButtons.OK);

            }
            return result;
        }
        private int checkMANV(String manv)
        {
            int result = 0;
            try
            {
                
                Program.KetNoi();
                List<SqlParameter> list = new List<SqlParameter>() { new SqlParameter("@MANV", manv) };
                Program.myReader = Program.CallSpDataReader("SP_CHECKMANV", list);
                if (Program.myReader.Read())
                {
                    result = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
                Program.conn.Close();
                
            }
            catch (Exception) { }
            return result;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }



        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                sukien = "CHUYEN";
                oldMNV = ((DataRowView)nhanVienBindingSource[nhanVienBindingSource.Position])["MANV"].ToString();
                vitri = nhanVienBindingSource.Position;
                btnLuu.Enabled = true;
                btnThoat.Enabled = true;
                btnThem.Enabled = btnSua.Enabled = btnXoa.Enabled = btnPhucHoi.Enabled = false;
                panelNhanVien.Enabled = true;
                comboBoxCNNV.Enabled = true;
                textBoxHo.Enabled = comboBoxPhai.Enabled = textBoxTen.Enabled = textBoxDiaChi.Enabled = textBoxMNV.Enabled= textBoxSDT.Enabled= checkBoxTranThaiXoa.Enabled = false;
                gridControlNV.Enabled  = false;
                this.comboBoxChiNhanh.Enabled = false;

            }
            catch (Exception) { }
        }
        private void reload()
        {
            try
            {
                this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                this.nhanVienTableAdapter.Fill(this.nGANHANGDataSet.NhanVien);
            }
            catch(Exception)
            {

            }
        }
        private void panelNhanVien_Paint(object sender, PaintEventArgs e)
        {

        }
    }

}