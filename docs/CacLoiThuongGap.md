# Loi Xoa Subcription
```sql
DECLARE @publication AS sysname;
DECLARE @subscriber AS sysname;
DECLARE @subscriptionDB AS sysname;
SET @publication = N'NGANHANG_CN1';
SET @subscriber = 'DESKTOP-V1KFST6\THIENVSERVER1';
SET @subscriptionDB = N'NGANHANG';

USE NGANHANG
EXEC sp_dropmergesubscription 
  @publication = @publication, 
  @subscriber = @subscriber, 
  @subscriber_db = @subscriptionDB;
GO
```