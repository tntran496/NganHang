# Danh Sách Phân Mảnh
```sql
CREATE VIEW [dbo].[V_DS_PHANMANH]
AS
SELECT TENCN=PUBS.description, TENSERVER= subscriber_server
   FROM dbo.sysmergepublications PUBS,  dbo.sysmergesubscriptions SUBS
   WHERE PUBS.pubid= SUBS.PUBID  AND PUBS.publisher <> SUBS.subscriber_server
```
# Danh Sách Nhan Vien Chua Co Tai Khoan
```sql
CREATE VIEW [dbo].[V_LAYMANHANVIEN]
AS
SELECT NV.MANV FROM NhanVien NV
EXCEPT
SELECT US.name FROM dbo.sysusers US
```
# Dang Nhap Cho user trong he thong
```sql
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[SP_DANGNHAP]
@TENLOGIN NVARCHAR (50),
@PASSWORD NVARCHAR (50)
AS
DECLARE @TENUSER NVARCHAR(50)
SELECT @TENLOGIN=loginname FROM sys.syslogins where loginname = @TENLOGIN and pwdcompare(@PASSWORD, password) = 1

SELECT @TENUSER=NAME FROM sys.sysusers WHERE sid = SUSER_SID(@TENLOGIN)
 --djkasbdjkbasjk
 SELECT USERNAME = @TENUSER, 
  HOTEN = (SELECT HO+ ' '+ TEN FROM NHANVIEN  WHERE MANV = @TENUSER ),
   TENNHOM= NAME
   FROM sys.sysusers 
   WHERE UID = (SELECT GROUPUID 
                 FROM SYS.SYSMEMBERS 
                   WHERE MEMBERUID= (SELECT UID FROM sys.sysusers 
                                      WHERE NAME=@TENUSER))
```
# Tao Tai Khoan Cho Nhan Vien He Thong
```sql
USE [NGANHANG]
GO
/****** Object:  StoredProcedure [dbo].[SP_TAOLOGIN]    Script Date: 11/1/2020 2:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_TAOLOGIN]
  @LGNAME VARCHAR(50),
  @PASS VARCHAR(50),
  @USERNAME VARCHAR(50),
  @ROLE VARCHAR(50)
AS
  DECLARE @RET INT
  EXEC @RET= SP_ADDLOGIN @LGNAME, @PASS,'NGANHANG'
  IF (@RET =1)  -- LOGIN NAME BI TRUNG
   
  BEGIN
  RAISERROR (N'Login name bị trùng', 16,1)

     RETURN 
	END

  EXEC @RET= SP_GRANTDBACCESS @LGNAME, @USERNAME --tao user
  IF (@RET =1)  -- USER  NAME BI TRUNG
  BEGIN
       EXEC SP_DROPLOGIN @LGNAME
	   RAISERROR (N' name bị trùng', 16,2)
       RETURN 
  END

  EXEC sp_addrolemember @ROLE, @USERNAME
  IF @ROLE= 'NGANHANG' OR @ROLE = 'CHINHANH'
  BEGIN
    EXEC sp_addsrvrolemember @LGNAME, 'securityadmin'
	EXEC sp_addsrvrolemember @LGNAME, 'processadmin'	
	END
	SELECT KQ = 0
RETURN  -- THANH CONG
```
# GOI TIEN
```sql
USE [NGANHANG]
GO
/****** Object:  StoredProcedure [dbo].[SP_GOIRUT]    Script Date: 11/19/2020 6:26:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[SP_GOIRUT]
@TAIKHOAN NCHAR(9), @SOTIEN money, @LOAIGD NCHAR(2), @MANV nchar(10)

AS
BEGIN

SET XACT_ABORT ON
BEGIN TRAN

BEGIN TRY
	DECLARE @sd money;
	SET @sd = ( SELECT SODU FROM TaiKhoan )
	IF(exists(SELECT TK.SOTK FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @TAIKHOAN))
		BEGIN
			IF(@LOAIGD = 'GT')
				BEGIN
					UPDATE NGANHANG.dbo.TaiKhoan SET SODU = SODU + @SOTIEN WHERE SOTK= @TAIKHOAN
		  
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV) VALUES (@TAIKHOAN, 'GT', @SOTIEN, @MANV )
					COMMIT
					RETURN
				END
			IF(@LOAIGD = 'RT')
				BEGIN
					IF(@sd < @SOTIEN)
						BEGIN
							RAISERROR('So du khong du', 16, 1)
							ROLLBACK
							RETURN
						END
				
					UPDATE NGANHANG.dbo.TaiKhoan SET SODU = SODU - @SOTIEN WHERE SOTK= @TAIKHOAN
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV)VALUES (@TAIKHOAN, 'RT', @SOTIEN, @MANV )
					COMMIT
					RETURN
				END
		END
	ELSE
		BEGIN
			RAISERROR('Tai khoan khong ton tai', 16, 1) 
		END
	IF(exists(SELECT TK.SOTK FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @TAIKHOAN))
		BEGIN
			IF(@LOAIGD = 'GT')
				BEGIN
					UPDATE NGANHANG.dbo.TaiKhoan SET SODU = SODU + @SOTIEN WHERE SOTK= @TAIKHOAN
		  
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV) VALUES (@TAIKHOAN, 'GT', @SOTIEN, @MANV )
					COMMIT
					RETURN
				END
			IF(@LOAIGD = 'RT')
				BEGIN
					IF(@sd < @SOTIEN)
						BEGIN
							RAISERROR('So du khong du', 16, 1)
							ROLLBACK
							RETURN
						END
				
					UPDATE NGANHANG.dbo.TaiKhoan SET SODU = SODU - @SOTIEN WHERE SOTK= @TAIKHOAN
					INSERT INTO NGANHANG.dbo.GD_GOIRUT(SOTK, LOAIGD, SOTIEN, MANV)VALUES (@TAIKHOAN, 'RT', @SOTIEN, @MANV )
					COMMIT
					RETURN
				END
	END
ELSE
		BEGIN
			RAISERROR('Tai khoan khong ton tai', 16, 1) 
		END
END TRY

BEGIN CATCH
   
   DECLARE @ErrorMessage VARCHAR(2000)
   SELECT @ErrorMessage = 'Lỗi: ' + ERROR_MESSAGE()
   RAISERROR(@ErrorMessage, 16, 1)
   ROLLBACK
END CATCH
END
```
# Chuyen Tien
```sql
USE [NGANHANG]
GO
/****** Object:  StoredProcedure [dbo].[SP_ChuyenTien_TaiKhoan]    Script Date: 11/19/2020 6:28:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_ChuyenTien_TaiKhoan]
	-- Add the parameters for the stored procedure here
@SOTK_CHUYEN NCHAR(9),
@SOTIEN MONEY,
@SOTK_NHAN NCHAR(9),
@MANV NCHAR(10)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		BEGIN TRY
			DECLARE @ErrorMessage varchar(2000)
			DECLARE @TienTkChuyen money

			IF(EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE NGANHANG.dbo.TaiKhoan  SET SODU = SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
			IF(EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE LINK.NGANHANG.dbo.TaiKhoan SET SODU =SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
			IF(EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE NGANHANG.dbo.TaiKhoan SET SODU =SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
			IF(EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN) AND
					EXISTS(SELECT * FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_NHAN))
			BEGIN
				SELECT @TienTkChuyen = TK.SODU FROM LINK.NGANHANG.dbo.TaiKhoan AS TK WHERE TK.SOTK = @SOTK_CHUYEN
				IF(@TienTkChuyen<@SOTIEN)
					BEGIN
						SELECT @ErrorMessage = N'SỐ TIỀN TRONG TÀI KHOẢN CHUYỂN KHÔNG ĐỦ'
						RAISERROR(@ErrorMessage, 16,1)
						ROLLBACK
						RETURN
					END
				UPDATE LINK.NGANHANG.dbo.TaiKhoan  SET SODU =@TienTkChuyen-@SOTIEN  WHERE SOTK = @SOTK_CHUYEN 
				UPDATE LINK.NGANHANG.dbo.TaiKhoan SET SODU =SODU+@SOTIEN  WHERE SOTK = @SOTK_NHAN
				INSERT INTO NGANHANG.dbo.GD_CHUYENTIEN(SOTK_CHUYEN,SOTIEN,SOTK_NHAN,MANV) VALUES (@SOTK_CHUYEN,@SOTIEN,@SOTK_NHAN,@MANV)
				COMMIT
				RETURN
			END
		END TRY
		BEGIN CATCH
			SELECT @ErrorMessage = N'LỖI ' +ERROR_MESSAGE()
			RAISERROR(@ErrorMessage, 16,1)
			ROLLBACK
		END CATCH
END
```
# Kiem Tra CMND Khach Hang
```sql
USE [NGANHANG]
GO
/****** Object:  StoredProcedure [dbo].[SP_CHECK_CMND_KH]    Script Date: 11/19/2020 6:29:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_CHECK_CMND_KH] 
	-- Add the parameters for the stored procedure here
@CMND nchar(10)
AS
BEGIN
	IF(exists(SELECT KH.CMND FROM NGANHANG.dbo.KhachHang AS KH WHERE KH.CMND = @CMND))
		BEGIN
			SELECT 1;
			RETURN;
		END
		
	IF(exists(SELECT KH.CMND FROM LINK.NGANHANG.dbo.KhachHang AS KH WHERE KH.CMND = @CMND))
		BEGIN
			SELECT 1;
			RETURN;
		END
	SELECT 0;
END
```